const { removeCharOfString, insertText } = require("../helper/helper")
const { lang } = require("./lang")

let defaultLang = lang['en']

const setT = (key, status, ...props) => {
    let text = defaultLang[key][status]

    var regex = new RegExp('\\?\\?', 'g')
    let i = 0

    while (text?.search(regex) !== -1) {
        if (!props[i] && props[i] !== 0) {
            break
        }
        const index = text?.search(regex)
        text = removeCharOfString(text, index, index + 1)
        text = insertText(text, index, props[i]?.toString() || '')
        i++
    }

    return text
}

const setLocale = (locale) => {
    defaultLang = lang[locale]

    return { setT }
}

// export default setLocale
module.exports = {
    setLocale, setT
}


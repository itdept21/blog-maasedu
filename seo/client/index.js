import { Calculation } from "../checker/calculation"
import { setLocale } from "../lang"

const SEOContentAnalyzer = (param, locale = 'en') => {
    return Calculation(
        param,
        new DOMParser().parseFromString(param.content.toLowerCase(), 'text/html'),
    )
}

// export default SEOContentAnalyzer
module.exports = {
    SEOContentAnalyzer
}
const Tag = require("en-pos").Tag;
const pluralize = require('pluralize');

const splitWords = (text) => {
    text = text.replace(/(^\s*)|(\s*$)/gi, '') //exclude  start and end white-space
    text = text.replace(/\n/g, ' ')
    text = text.replace(/[^\w\s]/gi, ' ') // bo ky tu dac biet
    text = text.replace(/[ ]{2,}/gi, ' ') //2 or more space to 1
    return text.split(' ').filter((s) => !!s.trim())
}

const splitSentences = (text) => {
    const regex = /(?<![A-Z].)[.?!;:\n]\s+/
    let sentences = text.split(regex);
    sentences.forEach((sentence, index) => {
        sentences[index] = sentence.replace(/(\.|\?|\!)+$/g, '') // Remove trailing punctuation if present
    })
    return sentences;
}

const countWords = (text) => {
    return splitWords(text).length
}

const countSentences = (text) => {
    return splitSentences(text).length
}

const getDensity = (
    text,
    keyword
) => {
    const s = splitWords(text)
    if (s.length <= 0 || !keyword) return [0, 0]

    // let numOfKeyword = text.match(new RegExp(pluralize.plural(keyword), 'g'))?.length || 0

    // numOfKeyword = numOfKeyword + text.match(new RegExp(pluralize.singular(keyword), 'g'))?.length || 0

    // numOfKeyword = numOfKeyword - text.match(new RegExp(keyword, 'g'))?.length || 0

    const _countOccurrences = countOccurrences(text, [keyword]) || {};

    const numOfKeyword = Object.values(_countOccurrences).reduce((acc, currentValue) => acc + currentValue, 0) || 0

    return [Math.round((numOfKeyword * 100) / s.length), numOfKeyword]
}

function countOccurrences(content, words) {
    const occurrences = {};

    words.forEach((word) => {
        const plural = pluralize.plural(word);
        const singular = pluralize.singular(word);

        const regex = new RegExp(`\\b(${plural}|${singular})\\b`, 'gi');
        const matches = content.match(regex);

        if (matches) {
            occurrences[word] = matches.length;
        } else {
            occurrences[word] = 0;
        }
    });

    return occurrences;
}

const countChar = (str) => {
    return str.trim().length
}

const wordExists = (
    text,
    keyword,
    firstNumChar
) => {
    text = text.toLowerCase()
    const arrKeyword = splitWords(keyword.toLowerCase())
    console.log("wordExists", arrKeyword)
    if (firstNumChar) {
        let i = firstNumChar - 1
        while (text[i] && text[i]?.trim()) {
            i++
        }
        return (
            arrKeyword.length > 0 &&
            arrKeyword.every((k) => splitWords(text.slice(0, i)).includes(k))
        )
    }
    return (
        arrKeyword.length > 0 &&
        arrKeyword.every((k) => splitWords(text).includes(k))
    )
}

// Hàm chuyển đổi từ Singular sang Plural và ngược lại
function convertToPluralOrSingular(word, isPlural = true) {
    const irregulars = {
        // Các trường hợp đặc biệt
        child: 'children',
        person: 'people',
        man: 'men',
        woman: 'women',
        tooth: 'teeth',
        foot: 'feet',
        mouse: 'mice',
        goose: 'geese',
        ox: 'oxen',
        cactus: 'cacti',
        focus: 'foci',
        stimulus: 'stimuli',
        fungus: 'fungi',
        appendix: 'appendices',
        criterion: 'criteria',
        phenomenon: 'phenomena'
    };

    const plurals = [
        [/s$/, ''], // Đã có s ở cuối
        [/([bcdfghjklmnpqrstvwxyz])y$/, '$1ies'], // Từ kết thúc bằng 'y'
        [/$/, 's'] // Các trường hợp khác
    ];

    const singulars = [
        [/children$/, 'child'],
        [/people$/, 'person'],
        [/men$/, 'man'],
        [/women$/, 'woman'],
        [/teeth$/, 'tooth'],
        [/feet$/, 'foot'],
        [/mice$/, 'mouse'],
        [/geese$/, 'goose'],
        [/oxen$/, 'ox'],
        [/cacti$/, 'cactus'],
        [/foci$/, 'focus'],
        [/stimuli$/, 'stimulus'],
        [/fungi$/, 'fungus'],
        [/appendices$/, 'appendix'],
        [/criteria$/, 'criterion'],
        [/phenomena$/, 'phenomenon'],
        [/(s)$/, ''] // Các trường hợp khác
    ];

    const rules = isPlural ? plurals : singulars;

    // Xử lý các trường hợp đặc biệt
    if (irregulars[word]) {
        return irregulars[word];
    }

    // Áp dụng quy tắc chung
    for (const [pattern, replacement] of rules) {
        // @ts-ignore
        if (pattern.test(word)) {
            return word.replace(pattern, replacement);
        }
    }

    // Áp dụng quy tắc chung cho từ hoặc cụm từ
    const applyRules = (input) => {
        for (const [pattern, replacement] of rules) {
            // @ts-ignore
            if (pattern.test(input)) {
                return input.replace(pattern, replacement);
            }
        }
        return input;
    };

    // Xử lý trường hợp từ là cụm từ (được xác định bằng dấu cách)
    if (/\s/.test(word)) {
        const words = word.split(/\s+/);
        const convertedWords = words.map((w) => applyRules(w));
        return convertedWords.join(' ');
    }

    // Xử lý trường hợp từ đơn
    return applyRules(word);
}

const insertText = (
    text,
    index,
    addText
) => {
    return text.slice(0, index) + addText + text.slice(index)
}

const removeCharOfString = (
    text,
    bottomIndex,
    topIndex
) => {
    return text.slice(0, bottomIndex) + text.slice(topIndex + 1)
}

const countSyllables = (word) => {
    let count = 0;
    let vowels = "aeiouy"
    word = word.toLowerCase();
    if (vowels.includes(word.charAt(0))) {
        count += 1;
    }
    for (let index = 0; index < word.length; index++) {
        if (vowels.includes(word.charAt(index)) && !vowels.includes(word.charAt(index - 1))) {
            count += 1;
        }
    }
    if (word.slice(-1) === "e") {
        count -= 1;
    }
    if (count == 0) {
        count += 1;
    }

    return count;
}
const fleshReadingScore = (text) => {
    const base = 206.835
    const firstParam = 1.015
    const secondParam = 84.6
    const totalWords = countWords(text)
    const totalSentences = countSentences(text)
    const totalSyllables = countSyllables(text)


    if (totalSentences !== 0 && totalWords !== 0 && totalSyllables !== 0) {
        return Math.round(
            base -
            firstParam * (totalWords / totalSentences) -
            secondParam * (totalSyllables / totalWords)
        )
    } else {
        return 0
    }
}

const longSectionExists = (elements) => {
    let longSectionExists = false
    let sectionLength = 0

    elements.forEach((element) => {
        if (element.tagName.startsWith('H')) {
            sectionLength = 0
        } else {
            const elementText = element.textContent || ''
            sectionLength += countWords(elementText)
        }

        if (sectionLength > 300) {
            longSectionExists = true
        }
    })
    return longSectionExists;
}

const usingPassiveVoice = (text) => {
    // console.log(text)
    const passiveTensePatterns = [
        ['VBZ', 'VBN'],
        ['VBP', 'VBN'],
        ['VBZ', 'VBG', 'VBN'],
        ['VBP', 'VBG', 'VBN'],
        ['VBD', 'VBN'],
        ['VBD', 'VBG', 'VBN'],
        ['VBZ', 'VBN', 'VBN'],
        ['VBP', 'VBN', 'VBN'],
        ['VBD', 'VBN', 'VBN'],
        ['MD', 'VB', 'VBN'],
        ['MD', 'VB', 'VBN', 'VBN'],
        ['VBZ', 'RB', 'VBN'],
        ['VBP', 'RB', 'VBN'],
        ['VBZ', 'RB', 'VBG', 'VBN'],
        ['VBP', 'RB', 'VBG', 'VBN'],
        ['VBD', 'RB', 'VBN'],
        ['VBD', 'RB', 'VBG', 'VBN'],
        ['VBZ', 'RB', 'VBN', 'VBN'],
        ['VBP', 'RB', 'VBN', 'VBN'],
        ['VBD', 'RB', 'VBN', 'VBN'],
        ['MD', 'RB', 'VB', 'VBN'],
        ['MD', 'RB', 'VB', 'VBN', 'VBN'],
        ['TO', 'VB', 'VBN']
    ]

    let passiveVoice = false
    let words = splitWords(text)
    var tags = new Tag(words)
        .initial()
        .smooth()
        .tags;

    // console.log(tags)
    let indexOfPattern
    passiveTensePatterns.forEach((pattern) => {
        for (let indexOfTags = 0; indexOfTags < tags.length - pattern.length + 1; indexOfTags++) {
            indexOfPattern = 0
            while (tags[indexOfTags + indexOfPattern] == pattern[indexOfPattern] && indexOfPattern < pattern.length) {
                // Discarding the most probable false positives
                if (
                    (pattern[indexOfPattern] == 'VBP' || pattern[indexOfPattern] == 'VB')
                    &&
                    (words[indexOfTags + indexOfPattern] == 'have' || words[indexOfTags + indexOfPattern] == 'has')
                ) {
                    // console.log('inside if')
                    break
                }
                // console.log('indexOfPattern : ' + indexOfPattern)
                if (indexOfPattern == pattern.length - 1) {
                    // console.log('indexOfPattern == pattern.length')
                    passiveVoice = true
                }
                indexOfPattern++
            }
            if (passiveVoice) {
                break
            }
        }
    })
    return passiveVoice
}

function createSlugFromTitle(alias) {
    let str = alias;
    if (str && str.length > 0) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        str = str.replace(/\s+/g, "-");
    }
    return str;
}

module.exports = {
    splitWords,
    splitSentences,
    countWords,
    countSentences,
    getDensity,
    countChar,
    wordExists,
    insertText,
    removeCharOfString,
    countSyllables,
    fleshReadingScore,
    longSectionExists,
    usingPassiveVoice,
    createSlugFromTitle,
    convertToPluralOrSingular,
};
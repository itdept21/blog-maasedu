const focusKeyPhraseContentScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'not_used',
        score: 0,
    }
];

const focusKeyPhraseIntroductionScoreBasicSEO = [
    {
        status: 'used',
        score: 9,
    },
    {
        status: 'not_used',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const focusKeyPhraseDescriptionScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const focusKeyPhraseSlugScoreBasicSEO = [
    {
        status: 'used',
        score: 5,
    },
    {
        status: 'not_used',
        score: 1,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const focusKeyPhraseScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'long',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const basicSeoDescriptionScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'long',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const basicSeoContentScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'short',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const internalLinksScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'not_exists',
        score: 0,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const externalLinksScoreBasicSEO = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'not_exists',
        score: 0,
    },
    {
        status: 'bad',
        score: 0,
    }
];




module.exports = {
    focusKeyPhraseContentScoreBasicSEO,
    focusKeyPhraseIntroductionScoreBasicSEO,
    focusKeyPhraseDescriptionScoreBasicSEO,
    focusKeyPhraseSlugScoreBasicSEO,
    focusKeyPhraseScoreBasicSEO,
    basicSeoDescriptionScoreBasicSEO,
    basicSeoContentScoreBasicSEO,
    internalLinksScoreBasicSEO,
    externalLinksScoreBasicSEO,

}
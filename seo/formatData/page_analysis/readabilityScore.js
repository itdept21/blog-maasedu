const imageOrVideosInContentScoreReadability = [
    {
        status: 'perfect',
        score: 5,
    },
    {
        status: 'not_exists',
        score: 2,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const paragraphsLengthScoreReadability = [
    {
        status: 'perfect',
        score: 5,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'short',
        score: 2,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const sentencesLengthScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'long',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const passiveVoiceScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const transitionWordsScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'long',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const consecutiveSentencesScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const subheadingDistributionScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const fleschReadingEaseScoreReadability = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

module.exports = {
    imageOrVideosInContentScoreReadability,
    paragraphsLengthScoreReadability,
    sentencesLengthScoreReadability,
    passiveVoiceScoreReadability,
    transitionWordsScoreReadability,
    consecutiveSentencesScoreReadability,
    subheadingDistributionScoreReadability,
    fleschReadingEaseScoreReadability,

}
const {
    focusKeyPhraseContentScoreBasicSEO,
    focusKeyPhraseIntroductionScoreBasicSEO,
    focusKeyPhraseDescriptionScoreBasicSEO,
    focusKeyPhraseSlugScoreBasicSEO,
    focusKeyPhraseScoreBasicSEO,
    basicSeoDescriptionScoreBasicSEO,
    basicSeoContentScoreBasicSEO,
    internalLinksScoreBasicSEO,
    externalLinksScoreBasicSEO,
} = require("./basicSEOScore");

const {
    imageOrVideosInContentScoreReadability,
    paragraphsLengthScoreReadability,
    sentencesLengthScoreReadability,
    passiveVoiceScoreReadability,
    transitionWordsScoreReadability,
    consecutiveSentencesScoreReadability,
    subheadingDistributionScoreReadability,
    fleschReadingEaseScoreReadability,
} = require("./readabilityScore");

const {
    focusKeyPhraseSeoTitleScoreTitle,
    focusKeyPhraseBeginningSeoTitleScoreTitle,
    seoTileLengthScoreTitle
} = require("./titleScore");

const formatPageAnalysis = (payload, data) => {
    let basicSEO = [
        {
            key: 'basicSeoDescription',
            title: "Meta description length",
            score: basicSeoDescriptionScoreBasicSEO.find((e) =>
                e.status == data?.[1]?.messages?.[0]?.status
            )?.score || 0,
            status: data?.[1]?.messages?.[0]?.status || "",
            text: data?.[1]?.messages?.[0]?.text || "",
        },
        {
            key: 'basicSeoContent',
            title: "Content length",
            score: basicSeoContentScoreBasicSEO.find((e) =>
                e.status == data?.[0]?.messages?.[0]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[0]?.status || "",
            text: data?.[0]?.messages?.[0]?.text || "",
        },
        {
            key: 'internalLinks',
            title: "Internal links",
            score: internalLinksScoreBasicSEO.find((e) =>
                e.status == data?.[0]?.messages?.[15]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[15]?.status || "",
            text: data?.[0]?.messages?.[15]?.text || "",
        },
        {
            key: 'externalLinks',
            title: "External links",
            score: externalLinksScoreBasicSEO.find((e) =>
                e.status == data?.[0]?.messages?.[16]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[16]?.status || "",
            text: data?.[0]?.messages?.[16]?.text || "",
        }
    ];

    if (payload.keyword.trim() !== "") {
        basicSEO = [
            {
                key: 'focusKeyPhraseContent',
                title: "Focus keyphrase in content",
                // score: data?.[2]?.messages?.[1]?.score || 0,
                score: focusKeyPhraseContentScoreBasicSEO.find((e) =>
                    e.status == data?.[3]?.messages?.[0]?.status
                )?.score || 0,
                status: data?.[2]?.messages?.[1]?.status || "",
                text: data?.[2]?.messages?.[1]?.text || "",
            },
            {
                key: 'focusKeyPhraseIntroduction',
                title: "Focus keyphrase in introduction",
                score: focusKeyPhraseIntroductionScoreBasicSEO.find((e) =>
                    e.status == data?.[0]?.messages?.[3]?.status
                )?.score || 0,
                status: data?.[0]?.messages?.[3]?.status || "",
                text: data?.[0]?.messages?.[3]?.text || "",
            },
            {
                key: 'focusKeyPhraseDescription',
                title: "Focus keyphrase in meta description",
                score: focusKeyPhraseDescriptionScoreBasicSEO.find((e) =>
                    e.status == data?.[1]?.messages?.[1]?.status
                )?.score || 0,
                status: data?.[1]?.messages?.[1]?.status || "",
                text: data?.[1]?.messages?.[1]?.text || "",
            },
            {
                key: 'focusKeyPhraseSlug',
                title: "Focus keyphrase in URL",
                score: focusKeyPhraseSlugScoreBasicSEO.find((e) =>
                    e.status == data?.[3]?.messages?.[1]?.status
                )?.score || 0,
                status: data?.[3]?.messages?.[1]?.status || "",
                text: data?.[3]?.messages?.[1]?.text || "",
            },
            {
                key: 'focusKeyPhrase',
                title: "Focus keyphrase length",
                score: focusKeyPhraseScoreBasicSEO.find((e) =>
                    e.status == data?.[3]?.messages?.[0]?.status
                )?.score || 0,
                status: data?.[3]?.messages?.[0]?.status || "",
                text: data?.[3]?.messages?.[0]?.text || "",
            },
            ...basicSEO
        ]
    };

    let _seoTitle = [
        {
            key: 'seoTileLength',
            title: "SEO Title length",
            score: seoTileLengthScoreTitle.find((e) =>
                e.status == data?.[2]?.messages?.[0]?.status
            )?.score || 0,
            status: data?.[2]?.messages?.[0]?.status || "",
            text: data?.[2]?.messages?.[0]?.text || "",
        }
    ]
    if (payload.keyword.trim() !== "") {
        _seoTitle = [
            {
                key: 'focusKeyPhraseSeoTitle',
                title: "Focus keyphrase in SEO title",
                score: focusKeyPhraseSeoTitleScoreTitle.find((e) =>
                    e.status == data?.[2]?.messages?.[1]?.status
                )?.score || 0,
                status: data?.[2]?.messages?.[1]?.status || "",
                text: data?.[2]?.messages?.[1]?.text || "",
            },
            {
                key: 'focusKeyPhraseBeginningSeoTitle',
                title: "Focus keyphrase at the beginning of SEO Title",
                score: focusKeyPhraseBeginningSeoTitleScoreTitle.find((e) =>
                    e.status == data?.[2]?.messages?.[2]?.status
                )?.score || 0,
                status: data?.[2]?.messages?.[2]?.status || "",
                text: data?.[2]?.messages?.[2]?.text || "",
            },
            ..._seoTitle
        ]
    }

    const readability = [
        {
            key: 'imageOrVideosInContent',
            title: "Images/videos in content",
            score: imageOrVideosInContentScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[7]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[7]?.status || "",
            text: data?.[0]?.messages?.[7]?.text || "",
        },
        {
            key: 'paragraphsLength',
            title: "Paragraphs length",
            score: paragraphsLengthScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[0]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[0]?.status || "",
            text: data?.[0]?.messages?.[0]?.text || "",
        },
        {
            key: 'sentencesLength',
            title: "Sentences length",
            score: sentencesLengthScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[10]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[10]?.status || "",
            text: data?.[0]?.messages?.[10]?.text || "",
        },
        {
            key: 'passiveVoice',
            title: "Passive voice",
            score: passiveVoiceScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[11]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[11]?.status || "",
            text: data?.[0]?.messages?.[11]?.text || "",
        },
        // ?
        {
            key: 'transitionWords',
            title: "Transition words",
            score: transitionWordsScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[17]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[17]?.status || "",
            text: data?.[0]?.messages?.[17]?.text || "",
        },
        // ?
        {
            key: 'consecutiveSentences',
            title: "Consecutive sentences ",
            score: consecutiveSentencesScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[18]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[18]?.status || "",
            text: data?.[0]?.messages?.[18]?.text || "",
        },
        // ?
        {
            key: 'subheadingDistribution',
            title: "Subheading distribution ",
            score: subheadingDistributionScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[19]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[19]?.status || "",
            text: data?.[0]?.messages?.[19]?.text || "",
        },
        {
            key: 'fleschReadingEase',
            title: "Flesch reading ease",
            score: fleschReadingEaseScoreReadability.find((e) =>
                e.status == data?.[0]?.messages?.[12]?.status
            )?.score || 0,
            status: data?.[0]?.messages?.[12]?.status || "",
            text: data?.[0]?.messages?.[12]?.text || "",
        }
    ]

    const _countErrorBasicSEO = basicSEO?.filter((e) => e.status !== 'perfect')?.length || 0;
    const _countErrorTitle = _seoTitle?.filter((e) => e.status !== 'perfect')?.length || 0;
    const _countErrorReadability = readability?.filter((e) => e.status !== 'perfect')?.length || 0;

    const pageAnalysis = {
        basicSEO,
        basicSEOCountError: _countErrorBasicSEO,
        title: _seoTitle,
        titleCountError: _countErrorTitle,
        readability: readability,
        readabilityCountError: _countErrorReadability,
    }

    const scoreAnalysisBasicSEO = basicSEO.reduce((partialSum, item) => partialSum + item.score, 0);
    const scoreBasicSEO = Math.round((scoreAnalysisBasicSEO * 100) / 77) || 0;

    const scoreAnalysisTitle = _seoTitle.reduce((partialSum, item) => partialSum + item.score, 0);
    const scoreTitle = Math.round((scoreAnalysisTitle * 100) / 27) || 0;

    const scoreAnalysisReadability = readability.reduce((partialSum, item) => partialSum + item.score, 0);
    const scoreReadability = Math.round((scoreAnalysisReadability * 100) / 64) || 0;

    const _score = Math.round(((scoreBasicSEO + scoreTitle + scoreReadability)) / 3) || 0

    const score = {
        scoreAnalysisBasicSEO: scoreAnalysisBasicSEO,
        scoreAnalysisTitle: scoreAnalysisTitle,
        scoreAnalysisReadability: scoreAnalysisReadability,
        score: _score,
        text: _score + "/" + 100,
    }

    return {
        score,
        analysis: pageAnalysis,
    }
}

module.exports = {
    formatPageAnalysis
}
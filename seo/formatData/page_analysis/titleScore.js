const focusKeyPhraseSeoTitleScoreTitle = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const focusKeyPhraseBeginningSeoTitleScoreTitle = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const seoTileLengthScoreTitle = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];



module.exports = {
    focusKeyPhraseSeoTitleScoreTitle,
    focusKeyPhraseBeginningSeoTitleScoreTitle,
    seoTileLengthScoreTitle,

}
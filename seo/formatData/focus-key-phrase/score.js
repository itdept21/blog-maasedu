const keyphraseInTitleScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseInDescriptionScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseLengthScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'good',
        score: 6,
    },
    {
        status: 'long',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseInURLScore = [
    {
        status: 'perfect',
        score: 5,
    },
    {
        status: 'not_used',
        score: 1,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseInIntroductionScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'not_used',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseInSubHeadingsScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'not_used',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keyphraseInImageAltScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'not_used',
        score: 3,
    },
    {
        status: 'bad',
        score: 0,
    }
];

const keywordDensityScore = [
    {
        status: 'perfect',
        score: 9,
    },
    {
        status: 'bad',
        score: 0,
    }
];



module.exports = {
    keyphraseInTitleScore,
    keyphraseInDescriptionScore,
    keyphraseLengthScore,
    keyphraseInURLScore,
    keyphraseInIntroductionScore,
    keyphraseInSubHeadingsScore,
    keyphraseInImageAltScore,
    keywordDensityScore,
}
const {
    keyphraseInTitleScore,
    keyphraseInDescriptionScore,
    keyphraseLengthScore,
    keyphraseInURLScore,
    keyphraseInIntroductionScore,
    keyphraseInSubHeadingsScore,
    keyphraseInImageAltScore,
    keywordDensityScore
} = require("./score")

const formatKeyPhrases = (payload, data, prefix) => {
    let analysis = [];

    if (payload.keyword.trim() == "") {
        analysis = [];
    } else {
        analysis = [
            {
                key: prefix + 'keyphraseInTitle',
                title: "Focus keyphrase in SEO title",
                // score: data?.[2]?.messages?.[1]?.score || 0,
                score: keyphraseInTitleScore.find((e) => e.status == data?.[2]?.messages?.[1]?.status)?.score || 0,
                status: data?.[2]?.messages?.[1]?.status || "",
                text: data?.[2]?.messages?.[1]?.text || "",
            },
            {
                key: prefix + 'keyphraseInDescription',
                title: "Focus keyphrase in meta description",
                score: keyphraseInDescriptionScore.find((e) => e.status == data?.[1]?.messages?.[1]?.status)?.score || 0,
                status: data?.[1]?.messages?.[1]?.status || "",
                text: data?.[1]?.messages?.[1]?.text || "",
            },
            {
                key: prefix + 'keyphraseLength',
                title: "Focus keyphrase length",
                score: keyphraseLengthScore.find((e) =>
                    e.status == data?.[3]?.messages?.[0]?.status
                )?.score || 0,
                status: data?.[3]?.messages?.[0]?.status || "",
                text: data?.[3]?.messages?.[0]?.text || "",
            },
            {
                key: prefix + 'keyphraseInURL',
                title: "Focus keyphrase in URL",
                score: keyphraseInURLScore.find((e) =>
                    e.status == data?.[3]?.messages?.[1]?.status
                )?.score || 0,
                status: data?.[3]?.messages?.[1]?.status || "",
                text: data?.[3]?.messages?.[1]?.text || "",
            },
            {
                key: prefix + 'keyphraseInIntroduction',
                title: "Focus keyphrase in introduction",
                score: keyphraseInIntroductionScore.find((e) =>
                    e.status == data?.[0]?.messages?.[3]?.status
                )?.score || 0,
                status: data?.[0]?.messages?.[3]?.status || "",
                text: data?.[0]?.messages?.[3]?.text || "",
            },
            {
                key: prefix + 'keyphraseInSubHeadings',
                title: "Focus keyphrase in Subheadings",
                score: keyphraseInSubHeadingsScore.find((e) =>
                    e.status == data?.[0]?.messages?.[14]?.status
                )?.score || 0,
                status: data?.[0]?.messages?.[14]?.status || "",
                text: data?.[0]?.messages?.[14]?.text || "",
            },
            {
                key: prefix + 'keyphraseInImageAlt',
                title: "Focus keyphrase in image alt attributes",
                score: keyphraseInImageAltScore.find((e) =>
                    e.status == data?.[0]?.messages?.[8]?.status
                )?.score || 0,
                status: data?.[0]?.messages?.[8]?.status || "",
                text: data?.[0]?.messages?.[8]?.text || "",
            },
            {
                key: prefix + 'keywordDensity',
                title: "Focus keyphrase density",
                score: keywordDensityScore.find((e) =>
                    e.status == data?.[0]?.messages?.[5]?.status
                )?.score || 0,
                status: data?.[0]?.messages?.[5]?.status || "",
                text: data?.[0]?.messages?.[5]?.text || "",
            }
        ];
    }

    const scoreAnalysis = analysis.reduce((partialSum, item) => partialSum + item.score, 0);
    const score = {
        score: Math.round((scoreAnalysis * 100) / 68),
        scoreAnalysis: scoreAnalysis,
        text: Math.round((scoreAnalysis * 100) / 68) + "/100",
    }

    return {
        score,
        analysis
    }
}

module.exports = {
    formatKeyPhrases
}
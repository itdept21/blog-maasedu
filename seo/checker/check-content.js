const { countWords, wordExists, longSectionExists, getDensity, splitSentences, countSentences, usingPassiveVoice, fleshReadingScore, convertToPluralOrSingular } = require("../helper/helper");
const { SectionChecker } = require("./SectionChecker");
const { AtomicChecker } = require("./SectionChecker");
const { setT } = require("../lang");
const { FE_HOST } = require("../../constants/host.constants");
const pluralize = require('pluralize');

// Cause and effect , Clarification, Contrast,
// Example, Emphasis, Enumeration, Time,
// Similarity, Summarize/conclude	
const transitionWords = [
  "therefore", "as a result", "so", 'consequently', // Cause and effect
  "that is to say", "in other words", "to clarify", // Clarification
  "But", "however", "on the other hand", // Contrast
  "for example", "or instance", // Example
  "above all", "most importantly", "certainly", // Emphasis
  "and", "moreover", "in addition", // Firstly/secondly // Enumeration
  "meanwhile", "during", "subsequently", "after that", //Time
  "likewise", "similarly", " in the same vein", // Similarity
  "in conclusion", "to sum up", "in short", // Summarize/conclude	
]

class CheckContent extends SectionChecker {
  domContent;
  listH;

  constructor(domContent, keyword) {
    const contentText =
      domContent.body?.innerText || domContent.body?.textContent || ''
    super('Content score', contentText, keyword)
    this.domContent = domContent;
    this.listH = []

    this.checkContentMinimumWords() // 1
    this.checkH1Exists() // 2
    this.checkH1ContainsKeyword() // 3
    this.checkFirstParagraphContainsKeyword() // 4
    this.checkSectionsLength() // 5
    this.checkDensity() // 6
    this.checkLinkExists() // 7
    this.checkImageExists() // 8
    this.checkImageAltContainsKeword() // 9
    this.checkImageTitleContainsKeword() // 10
    this.checkSentencesLength() // 11
    this.checkPassiveVoice() // 12
    this.checkFleshReadability() // 13
    this.checkH2H3Exists() // 14
    this.checkH2H3ContainsKeyword() // 15
    this.countInternalLinks() // 16
    this.countExternalLinks() // 17
    this.checkTransitionWords() // 17
    this.consecutiveSentences() // 17
    this.checkSubheadingDistribution() // 18
  }

  checkContentMinimumWords() {
    const contentLength = countWords(this.text)
    const perfectMinimum = 300
    const goodMinimum = 150

    const message = new AtomicChecker(
      'CONTENT_MINIMUM_WORDS',
      setT('CONTENT_MINIMUM_WORDS', 'bad', perfectMinimum)
    )

    if (contentLength > 0) {
      if (contentLength >= perfectMinimum) {
        message.score = 21
        message.text = setT('CONTENT_MINIMUM_WORDS', 'perfect', contentLength)
        message.status = 'perfect'
      } else if (contentLength >= goodMinimum) {
        message.score = 16
        message.status = 'good'
        message.text = setT('CONTENT_MINIMUM_WORDS', 'good', contentLength)
      } else {
        message.score = 1,
          message.status = 'short'
        message.text = setT('CONTENT_MINIMUM_WORDS', 'short', contentLength)
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkH1Exists() {
    const listOfH1 = this.domContent.getElementsByTagName('h1')

    const message = new AtomicChecker(
      'H1_EXIST',
      setT('H1_EXIST', 'not_exists')
    )

    if (listOfH1.length > 0) {
      for (const h1 of listOfH1) {
        if (!!h1.textContent?.trim()) {
          message.score = 5
          message.text = setT('H1_EXIST', 'exists')
          message.status = 'perfect'
          break
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
  checkH1ContainsKeyword() {
    const listOfH1 = this.domContent.getElementsByTagName('h1')

    const message = new AtomicChecker(
      'H1_USE_KEYWORD',
      setT('H1_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (listOfH1.length > 0) {
      for (const h1 of listOfH1) {
        if (wordExists(h1.textContent || '', this.keyword)) {
          message.score = 14
          message.text = setT('H1_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        } else if (h1.textContent.includes(this.keyword)) {
          message.score = 14
          message.text = setT('H1_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        } else if (
          h1.textContent.includes(pluralize.plural(this.keyword)) ||
          h1.textContent.includes(pluralize.singular(this.keyword))
        ) {
          console.log("checkH1ContainsKeyword convertToPluralOrSingular", pluralize.plural(this.keyword), pluralize.singular(this.keyword))
          message.score = 14
          message.text = setT('H1_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkFirstParagraphContainsKeyword() {
    const firstParagraph = this.domContent.querySelector('p')

    const message = new AtomicChecker(
      'FIRST_PARAGRAPH_CONTAINS_KEYWORD',
      setT('FIRST_PARAGRAPH_CONTAINS_KEYWORD', 'not_used', this.keyword)
    )

    if (
      firstParagraph &&
      wordExists(firstParagraph.textContent || '', this.keyword)
    ) {
      message.score = 5
      message.text = setT(
        'FIRST_PARAGRAPH_CONTAINS_KEYWORD',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    } else if (firstParagraph.textContent.includes(this.keyword)) {
      message.score = 5
      message.text = setT(
        'FIRST_PARAGRAPH_CONTAINS_KEYWORD',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    } else if (
      firstParagraph.textContent.includes(pluralize.plural(this.keyword)) ||
      firstParagraph.textContent.includes(pluralize.singular(this.keyword))
    ) {
      console.log("checkFirstParagraphContainsKeyword convertToPluralOrSingular", pluralize.plural(this.keyword), pluralize.singular(this.keyword))
      message.score = 5
      message.text = setT(
        'FIRST_PARAGRAPH_CONTAINS_KEYWORD',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkSectionsLength() {
    const message = new AtomicChecker(
      'SECTIONS_LENGTH',
      setT('SECTIONS_LENGTH', 'bad')
    )

    const elements = Array.from(this.domContent.body.children)
    if (!longSectionExists(elements)) {
      message.score = 5
      message.text = setT(
        'SECTIONS_LENGTH',
        'perfect',
      )
      message.status = 'perfect'
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkDensity() {
    const message = new AtomicChecker(
      'DENSITY',
      setT('DENSITY', 'bad', this.keyword)
    )

    const [densityResult, keywordFound] = getDensity(this.text, this.keyword)

    if (densityResult > 0) {
      message.score = 15
      message.text = setT(
        'DENSITY',
        'perfect',
        densityResult,
        this.keyword,
        keywordFound
      )
      message.status = 'perfect'
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkLinkExists() {
    const aTags = this.domContent.getElementsByTagName('a')
    const message = new AtomicChecker(
      'LINK_EXISTS',
      setT('LINK_EXISTS', 'not_exists')
    )

    if (aTags.length > 0) {
      message.text = setT('LINK_EXISTS', 'exists', aTags.length)
      message.status = 'perfect'
      message.score = 5
    }
    this.score += message.score

    this.messages.push(message.getResult())
  }

  checkImageExists() {
    const imageTags = this.domContent.getElementsByTagName('img')
    const message = new AtomicChecker(
      'IMG_EXISTS',
      setT('IMG_EXISTS', 'not_exists')
    )

    if (imageTags.length > 0) {
      message.text = setT('IMG_EXISTS', 'exists')
      message.status = 'perfect'
      message.score = 5
    }
    this.score += message.score

    this.messages.push(message.getResult())
  }

  checkImageAltContainsKeword() {
    const listOfImg = this.domContent.getElementsByTagName('img')

    const message = new AtomicChecker(
      'IMG_ALT_USE_KEYWORD',
      setT('IMG_ALT_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (listOfImg.length > 0) {
      for (const img of listOfImg) {
        if (img.alt && wordExists(img.alt, this.keyword)) {
          message.score = 5
          message.text = setT('IMG_ALT_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        } else if (img.alt && img.alt.includes(this.keyword)) {
          message.score = 5
          message.text = setT('IMG_ALT_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        } else if (img.alt &&
          img.alt.includes(pluralize.plural(this.keyword)) ||
          img.alt.includes(pluralize.singular(this.keyword))
        ) {
          console.log("checkImageAltContainsKeword convertToPluralOrSingular", pluralize.plural(this.keyword), pluralize.singular(this.keyword))
          message.score = 5
          message.text = setT('IMG_ALT_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
  checkImageTitleContainsKeword() {
    const listOfImg = this.domContent.getElementsByTagName('img')

    const message = new AtomicChecker(
      'IMG_TITLE_USE_KEYWORD',
      setT('IMG_TITLE_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (listOfImg.length > 0) {
      for (const img of listOfImg) {
        if (img.title && wordExists(img.title, this.keyword)) {
          message.score = 5
          message.text = setT('IMG_TITLE_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        } else if (img.title && img.title.includes(this.keyword)) {
          message.score = 5
          message.text = setT('IMG_TITLE_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        } else if (img.title &&
          img.title.includes(pluralize.plural(this.keyword)) ||
          img.title.includes(pluralize.singular(this.keyword))
        ) {
          console.log("checkImageTitleContainsKeword convertToPluralOrSingular")
          message.score = 5
          message.text = setT('IMG_TITLE_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
  checkSentencesLength() {
    const sentences = splitSentences(this.text)
    const numberOfSentences = countSentences(this.text)
    let longSentences = 0
    let percentage = 0
    if (numberOfSentences > 0) {
      for (const sentence of sentences) {
        if (countWords(sentence) > 20) {
          longSentences += 1;
        }
      }
      percentage = Math.round((longSentences / numberOfSentences) * 100)
    } else {
      percentage = 0
    }
    const message = new AtomicChecker(
      'SENTENCES_LENGTH',
      setT('SENTENCES_LENGTH', 'bad', percentage)
    )
    if (percentage < 10) {
      message.score = 5
      message.text = setT('SENTENCES_LENGTH', 'perfect', percentage)
      message.status = 'perfect'
    } else if (percentage < 20) {
      message.score = 3
      message.status = 'good'
      message.text = setT('SENTENCES_LENGTH', 'good', percentage)
    } else {
      message.score = 1
      message.status = 'long'
      message.text = setT('SENTENCES_LENGTH', 'long', percentage)
    }
    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkPassiveVoice() {
    const perfectMinimum = 10
    const goodMinimum = 20

    const message = new AtomicChecker(
      'PASSIVE_VOICE',
      setT('PASSIVE_VOICE', 'bad', goodMinimum)
    )

    const sentences = splitSentences(this.text)
    const numberOfSentences = countSentences(this.text)

    let numberOfPassiveSentences = 0
    let result = false;

    sentences.forEach((sentence) => {
      result = usingPassiveVoice(sentence)
      // console.log(result)
      if (result) {
        numberOfPassiveSentences += 1
      }
    })

    const passivePercentage = Math.round(numberOfPassiveSentences * 100 / numberOfSentences)

    if (passivePercentage < 10) {
      message.score = 5
      message.text = setT('PASSIVE_VOICE', 'perfect', passivePercentage)
      message.status = 'perfect'
    } else if (passivePercentage < 20) {
      message.score = 3
      message.text = setT('PASSIVE_VOICE', 'good', passivePercentage)
      message.status = 'good'
    } else {
      message.score = 0
      message.text = setT('PASSIVE_VOICE', 'bad', passivePercentage)
      message.status = 'bad'
    }
    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkFleshReadability() {
    let score = fleshReadingScore(this.text)

    const message = new AtomicChecker(
      'FLESH_READING',
      setT('FLESH_READING', 'difficult', score)
    )

    if (score > 60) {
      message.score = 5
      message.text = setT('FLESH_READING', 'perfect', score)
      message.status = 'perfect'
    } else if (score > 40) {
      message.score = 3
      message.text = setT('FLESH_READING', 'good', score)
      message.status = 'good'
    }
    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkH2H3Exists() {
    const listOfH2 = this.domContent.getElementsByTagName('h2')

    const message = new AtomicChecker(
      'H2_H3_EXIST',
      setT('H2_H3_EXIST', 'not_exists')
    )

    if (listOfH2.length > 0) {
      for (const h2 of listOfH2) {
        if (!!h2.textContent?.trim()) {
          message.score = 5
          message.text = setT('H2_H3_EXIST', 'exists')
          message.status = 'perfect'
          break
        }
      }
    } else {
      const listOfH3 = this.domContent.getElementsByTagName('h3')

      if (listOfH3.length > 0) {
        for (const h3 of listOfH3) {
          if (!!h3.textContent?.trim()) {
            message.score = 5
            message.text = setT('H2_H3_EXIST', 'exists')
            message.status = 'perfect'
            break
          }
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  checkH2H3ContainsKeyword() {
    const listOfH2 = this.domContent.getElementsByTagName('h2')

    const message = new AtomicChecker(
      'H2_H3_USE_KEYWORD',
      setT('H2_H3_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (listOfH2.length > 0) {
      for (const h2 of listOfH2) {
        if (wordExists(h2.textContent || '', this.keyword)) {
          message.score = 14
          message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        } else if (h2.textContent.includes(this.keyword)) {
          message.score = 14
          message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        } else if (h2.textContent &&
          h2.textContent.includes(pluralize.plural(this.keyword)) ||
          h2.textContent.includes(pluralize.singular(this.keyword))
        ) {
          console.log("checkH2H3ContainsKeyword convertToPluralOrSingular")
          message.score = 14
          message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
          message.status = 'perfect'
          break
        }
      }
    } else {
      const listOfH3 = this.domContent.getElementsByTagName('h3')

      if (listOfH3.length > 0) {
        for (const h3 of listOfH3) {
          if (wordExists(h3.textContent || '', this.keyword)) {
            message.score = 14
            message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
            message.status = 'perfect'
            break
          }
          if (h3.textContent.includes(this.keyword)) {
            message.score = 14
            message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
            message.status = 'perfect'
            break
          } else if (h3.textContent &&
            h3.textContent.includes(pluralize.plural(this.keyword)) ||
            h3.textContent.includes(pluralize.singular(this.keyword))
          ) {
            console.log("checkH2H3ContainsKeyword convertToPluralOrSingular")
            message.score = 14
            message.text = setT('H2_H3_USE_KEYWORD', 'used', this.keyword)
            message.status = 'perfect'
            break
          }
        }
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  countInternalLinks() {
    const links = this.domContent.querySelectorAll('a')
    const message = new AtomicChecker(
      'INTERNAL_LINK',
      setT('INTERNAL_LINK', 'not_exists')
    )

    let internalCount = 0;
    links.forEach(link => {
      const href = link.getAttribute('href');
      if (href && href.startsWith(`${FE_HOST}`)) {
        internalCount++;
      }
    });

    if (internalCount > 0) {
      message.text = setT('INTERNAL_LINK', 'exists', internalCount)
      message.status = 'perfect'
      message.score = 5
    }
    this.score += message.score

    this.messages.push(message.getResult())
  }

  countExternalLinks() {
    const links = this.domContent.querySelectorAll('a')
    const message = new AtomicChecker(
      'EXTERNAL_LINK',
      setT('EXTERNAL_LINK', 'not_exists')
    )

    let externalCount = 0;
    links.forEach(link => {
      const href = link.getAttribute('href');
      if (href && !href.startsWith(`${FE_HOST}`)) {
        externalCount++;
      }
    });

    if (externalCount > 0) {
      message.text = setT('EXTERNAL_LINK', 'exists', externalCount)
      message.status = 'perfect'
      message.score = 5
    }
    this.score += message.score

    this.messages.push(message.getResult())
  }

  checkTransitionWords() {
    const sentences = splitSentences(this.text)
    const numberOfSentences = countSentences(this.text)
    // Đếm số lượng câu chứa từ chuyển đổi
    let sentencesWithTransitionCount = 0;
    let percentage = 0;

    if (numberOfSentences > 0) {
      for (const sentence of sentences) {
        if (transitionWords.some(word => sentence.toLowerCase().includes(word))) {
          sentencesWithTransitionCount++;
        }
      }
      // Tính phần trăm
      percentage = Math.round((sentencesWithTransitionCount / sentences.length) * 100) || 0;
    } else {
      percentage = 0
    }

    const message = new AtomicChecker(
      'TRANSITION_WORDS',
      setT('TRANSITION_WORDS', 'bad', 0)
    );

    if (percentage > 0 && percentage < 20) {
      message.score = 3
      message.text = setT('TRANSITION_WORDS', 'long', percentage)
      message.status = 'long'
    } else if (percentage >= 20 && percentage < 30) {
      message.score = 6
      message.status = 'good'
      message.text = setT('TRANSITION_WORDS', 'good', percentage)
    } else if (percentage >= 30) {
      message.score = 9
      message.status = 'perfect'
      message.text = setT('TRANSITION_WORDS', 'perfect', percentage)
    }
    this.score += message.score
    this.messages.push(message.getResult())
  }

  getStartWord(sentence) {
    const words = sentence.trim().split(/\s+/);
    return words.length > 0 ? words[0].toLowerCase() : '';
  }

  consecutiveSentences() {
    const message = new AtomicChecker(
      'CONSECUTIVE_SENTENCES',
      setT('CONSECUTIVE_SENTENCES', 'bad')
    )

    let maxConsecutiveStarts = 0;
    const sentences = splitSentences(this.text)
    let consecutiveStarts = 1;
    let previousStartWord = this.getStartWord(sentences[0]);

    for (let i = 1; i < sentences.length; i++) {
      const currentStartWord = this.getStartWord(sentences[i]);
      if (currentStartWord === previousStartWord) {
        consecutiveStarts++;
      } else {
        consecutiveStarts = 1;
        previousStartWord = currentStartWord;
      }
      if (maxConsecutiveStarts < consecutiveStarts) {
        maxConsecutiveStarts = consecutiveStarts
      }
    }

    if (maxConsecutiveStarts > 3) {
      message.score = 6
      message.status = 'good'
      message.text = setT('CONSECUTIVE_SENTENCES', 'good', 3)
    } else if (maxConsecutiveStarts < 3) {
      message.score = 9
      message.status = 'perfect'
      message.text = setT('CONSECUTIVE_SENTENCES', 'perfect')
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  getListTagH() {
    const listTitle = []
    const listOfH1 = this.domContent.getElementsByTagName('h1')
    if (listOfH1.length > 0) {
      for (const h1 of listOfH1) {
        if (!!h1.textContent?.trim()) {
          listTitle.push(h1.textContent)
        }
      }
    }

    const listOfH2 = this.domContent.getElementsByTagName('h2')
    if (listOfH2.length > 0) {
      for (const item of listOfH2) {
        if (!!item.textContent?.trim()) {
          listTitle.push(item.textContent)
        }
      }
    }

    const listOfH3 = this.domContent.getElementsByTagName('h3')
    if (listOfH3.length > 0) {
      for (const item of listOfH3) {
        if (!!item.textContent?.trim()) {
          listTitle.push(item.textContent)
        }
      }
    }

    const listOfH4 = this.domContent.getElementsByTagName('h4')
    if (listOfH4.length > 0) {
      for (const item of listOfH4) {
        if (!!item.textContent?.trim()) {
          listTitle.push(item.textContent)
        }
      }
    }
    return listTitle
  }

  checkSubheadingDistribution() {
    const message = new AtomicChecker(
      'SUBHEADING_DISTRIBUTION',
      setT('SUBHEADING_DISTRIBUTION', 'bad', 0)
    );

    let checkCountWord = 0;
    let checkCountSection = 0;

    const listTitle = this.getListTagH()
    const listAllTag = this.domContent.getElementsByTagName('*')
    if (listAllTag.length > 0) {
      for (const item of listAllTag) {
        if (!item.innerHTML.includes(`<li>`) && !item.innerHTML.includes(`<tr>`)) {
          if (!listTitle.includes(item.textContent)) {
            checkCountWord += countWords(item.textContent);
          } else {
            checkCountWord = 0
          }
        }

        if (checkCountWord > 300) {
          checkCountSection += 1;
          checkCountWord = 0
        }
      }
    }

    if (checkCountSection > 0) {
      message.score = 6
      message.status = 'good'
      message.text = setT('SUBHEADING_DISTRIBUTION', 'good', checkCountSection)
    } else if (checkCountSection == 0) {
      message.score = 9
      message.status = 'perfect'
      message.text = setT('SUBHEADING_DISTRIBUTION', 'perfect')
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

}



module.exports = {
  CheckContent
}
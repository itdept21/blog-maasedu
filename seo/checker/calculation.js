const { CheckContent } = require("./check-content");
const { CheckDescMeta } = require("./check-desc-meta");
const { CheckOther } = require("./check-other");
const { CheckTitle } = require("./check-title");

const Calculation = (
  { keyword, title, descriptionMeta, slug },
  domDocument,
) => {
  const contentScore = new CheckContent(domDocument, keyword);
  const descMetaScore = new CheckDescMeta(descriptionMeta, keyword)
  const titleScore = new CheckTitle(title, keyword)
  const otherScore = new CheckOther(title, slug, keyword)

  const seoScore =
    contentScore.countScore(75) +
    titleScore.countScore(20) +
    descMetaScore.countScore(5) +
    otherScore.countScore(0)

  return {
    seoScore,
    sectionScore: [
      contentScore.getResult(),
      descMetaScore.getResult(),
      titleScore.getResult(),
      otherScore.getResult()
    ]
  }
}

// export default Calculation
module.exports = {
  Calculation
}
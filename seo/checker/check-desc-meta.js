const { countChar, wordExists, convertToPluralOrSingular } = require("../helper/helper");
const { SectionChecker } = require("./SectionChecker");
const { AtomicChecker } = require("./SectionChecker");
const { setT } = require("../lang");
const pluralize = require('pluralize');

class CheckDescMeta extends SectionChecker {
  constructor(text, keyword, t) {
    super('Meta description score', text, keyword)
    this.checkLength()
    this.containsKeyword()
  }

  checkLength() {
    const textLength = countChar(this.text)
    const perfectMinimum = 100
    const perfectMaximum = 160
    const diff = perfectMaximum - textLength

    const message = new AtomicChecker(
      'DESC_META_LENGTH',
      setT('DESC_META_LENGTH', 'short', diff, textLength, perfectMaximum)
    )

    if (textLength > 0) {
      if (textLength > perfectMaximum) {
        message.score = 10
        message.text = setT(
          'DESC_META_LENGTH',
          'long',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'good'
      } else if (textLength < perfectMinimum) {
        message.score = 10
        message.text = setT(
          'DESC_META_LENGTH',
          'short',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'good'
      } else {
        message.score = 85
        message.text = setT(
          'DESC_META_LENGTH',
          'perfect',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'perfect'
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
  containsKeyword() {
    const message = new AtomicChecker(
      'DESC_META_USE_KEYWORD',
      setT('DESC_META_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (wordExists(this.text, this.keyword)) {
      message.score = 15
      message.text = setT('DESC_META_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    } else if (this.text.includes(this.keyword)) {
      message.score = 15
      message.text = setT('DESC_META_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    } else if (
      this.text.includes(pluralize.plural(this.keyword)) ||
      this.text.includes(pluralize.singular(this.keyword))
    ) {
      console.log("containsKeyword convertToPluralOrSingular")
      message.score = 15
      message.text = setT('DESC_META_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
}

module.exports = {
  CheckDescMeta
}

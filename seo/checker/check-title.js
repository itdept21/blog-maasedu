const { countChar, wordExists, convertToPluralOrSingular } = require("../helper/helper")
const { SectionChecker, AtomicChecker } = require("./SectionChecker")
const { setT } = require("../lang");
const pluralize = require('pluralize');

class CheckTitle extends SectionChecker {
  constructor(text, keyword, t) {
    super('Page title score', text, keyword)
    this.checkLength()
    this.containsKeyword()
    this.keywordContainsOnBegining()
  }

  checkLength() {
    const textLength = countChar(this.text)
    const perfectMinimum = 30
    const perfectMaximum = 60
    const diff = perfectMaximum - textLength

    const message = new AtomicChecker(
      'TITLE_LENGTH',
      setT('TITLE_LENGTH', 'short', diff, textLength, perfectMaximum)
    )

    if (textLength > 0) {
      if (textLength > perfectMaximum) {
        message.score = 10
        message.text = setT(
          'TITLE_LENGTH',
          'long',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'good'
      } else if (textLength < perfectMinimum) {
        message.score = 10
        message.text = setT(
          'TITLE_LENGTH',
          'short',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'good'
      } else {
        message.score = 25
        message.text = setT(
          'TITLE_LENGTH',
          'perfect',
          diff,
          textLength,
          perfectMaximum
        )
        message.status = 'perfect'
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
  containsKeyword() {
    const message = new AtomicChecker(
      'TITLE_USE_KEYWORD',
      setT('TITLE_USE_KEYWORD', 'not_used', this.keyword)
    )

    if (wordExists(this.text, this.keyword)) {
      message.score = 70
      message.text = setT('TITLE_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    } else if (this.text.includes(this.keyword)) {
      message.score = 70
      message.text = setT('TITLE_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    } else if (
      this.text.includes(pluralize.plural(this.keyword)) ||
      this.text.includes(pluralize.singular(this.keyword))
    ) {
      console.log("containsKeyword title convertToPluralOrSingular")
      message.score = 70
      message.text = setT('TITLE_USE_KEYWORD', 'used', this.keyword)
      message.status = 'perfect'
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }

  keywordContainsOnBegining() {
    const message = new AtomicChecker(
      'TITLE_USE_KEYWORD_ON_BEGINNING',
      setT('TITLE_USE_KEYWORD_ON_BEGINNING', 'bad')
    )

    if (wordExists(this.text, this.keyword, 30)) {
      message.score = 5
      message.text = setT(
        'TITLE_USE_KEYWORD_ON_BEGINNING',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    } else if (this.text.startsWith(this.keyword)) {
      message.score = 5
      message.text = setT(
        'TITLE_USE_KEYWORD_ON_BEGINNING',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    } else if (
      this.text.startsWith(pluralize.plural(this.keyword)) ||
      this.text.startsWith(pluralize.singular(this.keyword))
    ) {
      message.score = 5
      message.text = setT(
        'TITLE_USE_KEYWORD_ON_BEGINNING',
        'used',
        this.keyword
      )
      message.status = 'perfect'
    } else {
      message.status = 'good'
      message.text = setT(
        'TITLE_USE_KEYWORD_ON_BEGINNING',
        'not_used',
        this.keyword
      )
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }
}

module.exports = {
  CheckTitle
}

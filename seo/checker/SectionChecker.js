const { setT } = require("../lang");

class SectionChecker {
  name = "";
  score = 0;
  messages = []
  text = "";
  keyword = "";

  constructor(name, text, keyword) {
    this.name = name
    this.text = text.toLowerCase()
    this.keyword = keyword.toLowerCase()
    this.score = 0
    this.messages = []
  }

  getResult() {
    return {
      name: this.name,
      score: this.score,
      messages: this.messages
    }
  }

  countScore(percentage) {
    return Math.round((this.score * percentage) / 100)
  }
}

class AtomicChecker {
  score
  code
  text
  status

  constructor(code, text = 'Bad text') {
    this.score = 0
    this.code = code
    this.text = text
    this.status = 'bad'
  }

  getResult() {
    return {
      code: this.code,
      score: this.score,
      status: this.status,
      text: this.text
    }
  }
}


module.exports = {
  SectionChecker, AtomicChecker
}

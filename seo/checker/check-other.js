const { countWords, wordExists, longSectionExists, getDensity, splitSentences, countSentences, usingPassiveVoice, fleshReadingScore, createSlugFromTitle, countChar, convertToPluralOrSingular } = require("../helper/helper");
const { SectionChecker } = require("./SectionChecker");
const { AtomicChecker } = require("./SectionChecker");
const { setT } = require("../lang");
const pluralize = require('pluralize');

class CheckOther extends SectionChecker {
  keywordSlug;

  constructor(text, slug, keyword) {
    super('Content score', text, keyword)
    this.keywordSlug = createSlugFromTitle(keyword);
    this.text = slug;

    this.checkFocusKeyPhraseLength()
    this.containsKeyword()

  }

  checkFocusKeyPhraseLength() {
    const textLength = countChar(this.keyword)

    const message = new AtomicChecker(
      'FOCUS_KEY_PHRASE_LENGTH',
      setT('FOCUS_KEY_PHRASE_LENGTH', 'bad', textLength)
    )
    if (textLength > 1 && textLength <= 15) {
      message.score = 5
      message.text = setT('FOCUS_KEY_PHRASE_LENGTH', 'perfect', textLength)
      message.status = 'perfect'
    } else if (textLength > 6 && textLength <= 16) {
      message.score = 3
      message.status = 'good'
      message.text = setT('FOCUS_KEY_PHRASE_LENGTH', 'good', textLength)
    } else if (textLength > 16) {
      message.score = 3
      message.status = 'long'
      message.text = setT('FOCUS_KEY_PHRASE_LENGTH', 'long', textLength)
    }
    this.score += message.score
    this.messages.push(message.getResult())
  }

  containsKeyword() {
    const message = new AtomicChecker(
      'URL_USE_KEYWORD',
      setT('URL_USE_KEYWORD', 'bad', this.keyword)
    )
    if (this.keyword.length > 0) {
      if (wordExists(this.text, this.keywordSlug)) {
        message.score = 70
        message.text = setT('URL_USE_KEYWORD', 'used', this.keyword)
        message.status = 'perfect'
      } else if (this.text.includes(this.keywordSlug)) {
        message.score = 70
        message.text = setT('URL_USE_KEYWORD', 'used', this.keyword)
        message.status = 'perfect'
      } else if (
        this.text.includes(createSlugFromTitle((pluralize.plural(this.keyword)))) ||
        this.text.includes(createSlugFromTitle((pluralize.singular(this.keyword))))
      ) {
        console.log("CheckOther convertToPluralOrSingular")
        message.score = 15
        message.text = setT('DESC_META_USE_KEYWORD', 'used', this.keyword)
        message.status = 'perfect'
      }
      else {
        message.score = 35
        message.text = setT('URL_USE_KEYWORD', 'not_used', this.keyword)
        message.status = 'not_used'
      }
    }

    this.score += message.score
    this.messages.push(message.getResult())
  }


}

module.exports = {
  CheckOther
}

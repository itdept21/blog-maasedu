const jsdom = require("jsdom");
const { Calculation } = require("./checker/calculation");
const { formatKeyPhrases } = require("./formatData/focus-key-phrase/focus-key-phrase");
const { formatPageAnalysis } = require("./formatData/page_analysis/page_analysis");
const { createSlugFromTitle } = require("./helper/helper");
const { JSDOM } = jsdom;

const SEOContentAnalyzer = (param) => {
    try {
        return Calculation(
            param,
            new JSDOM(param.content.toLowerCase()).window.document,
        )
    } catch (error) {
        return {};
    }

}

const SEOFocusKeyPhrase = (data) => {
    const calculationFocusKeyPhrase = SEOContentAnalyzer(data);
    const sectionScore = calculationFocusKeyPhrase.sectionScore;
    const result = formatKeyPhrases(data, sectionScore, '');

    return {
        score: result.score,
        analysis: result.analysis,
    };
};

const SEOAdditionalFocusKeyPhrases = (key, data) => {
    let result = [];
    for (let i = 0; i < key.length; i++) {
        const _payload = {
            keyword: key[i].trim(),
            ...data,
        }

        const calculationFocusKeyPhrase = SEOContentAnalyzer(_payload);
        const sectionScore = calculationFocusKeyPhrase.sectionScore;
        const dataResult = formatKeyPhrases(_payload, sectionScore, `additional-phrases-${i}-`);

        result.push({
            title: key[i],
            score: dataResult.score,
            key: createSlugFromTitle(key[i]) + `-${i}`,
            seoAnalyzer: dataResult.analysis,
        });
    }

    return result;
};

const SEOPageAnalysis = (data) => {
    const calculationFocusKeyPhrase = SEOContentAnalyzer(data);
    const sectionScore = calculationFocusKeyPhrase.sectionScore;
    const result = formatPageAnalysis(data, sectionScore);

    return {
        score: result.score,
        analysis: result.analysis,
    };
};

module.exports = {
    SEOContentAnalyzer,
    SEOFocusKeyPhrase,
    SEOAdditionalFocusKeyPhrases,
    SEOPageAnalysis,
}
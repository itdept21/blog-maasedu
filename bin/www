var app = require("../app");
var debug = require("debug")("maas-blog:server");
var http = require("http");
require("dotenv").config();
const mongoose = require('mongoose');
const fs = require('fs');
const { mongoDbUri } = require("../config/cf_mongodb");
const { SingletonRedis } = require("../redis/redis.module");

var port = normalizePort(process.env.PORT || "5015");
app.set("port", port);
var cors = require('cors');
app.use(cors());
var server = http.createServer(app);

const {
    IMAGES_STORAGE_DIR = "./public/uploads/images/original_images",
    IMAGES_STORAGE_LARGE = "./public/uploads/images/large_images",
    IMAGES_STORAGE_MEDIUM = "./public/uploads/images/medium_images",
    IMAGES_STORAGE_SMALL = "./public/uploads/images/small_images",
    IMAGES_STORAGE_USER = "./public/user_images",
} = process.env;

function createDir() {
    console.log('CREATING DIRECTORY FOR FILES UPLOAD ....')
    const dirs = ['./public/uploads', IMAGES_STORAGE_DIR, IMAGES_STORAGE_LARGE, IMAGES_STORAGE_MEDIUM, IMAGES_STORAGE_SMALL,
        IMAGES_STORAGE_USER,
    ];

    dirs.forEach(dir => {
        if (!fs.existsSync(dir))
            fs.mkdirSync(dir)
    })
}

mongoose.connect(
    mongoDbUri,
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: true
    },
    async (err) => {
        if (err) throw Error("MONGOOSE CONNECT FAILED");
        createDir();
        console.log("connect mongodb succeeded");
        server = http.createServer(app);

        /**
         * Listen on provided port, on all network interfaces.
         */
        server.listen(port, process.env.HOST || "localhost");
        server.on("error", onError);
        server.on("listening", onListening);
    }
);

const client = SingletonRedis.getClient()
client.connect().then(() => { console.log("Connect Redis") })

client.on(
    'error', (err) => {
        console.log("Redis Errr", err)
    }
)

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }

    var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
    debug("Listening on " + bind);
    console.log("bind", bind)
}
const POPULATE_HOME = [
    {
        path: "images",
        select: "_id name src alt title caption",
    },
    {
        path: "child",
        populate: {
            path: "images",
            select: "_id name src alt title caption",
        },
    },
    {
        path: "serviceCategory",
    },
    {
        path: "feedbacks",
    },
    {
        path: "packages",
    },
    {
        path: "faqs",
    },
];

const POPULATE_SERVICES = [
    {
        path: "images",
        select: "_id name src alt title caption",
    },
    {
        path: "child",
        populate: {
            path: "images",
            select: "_id name src alt title caption",
        },
    },
    {
        path: "customerFeedbacks",
        populate: [
            {
                path: "images",
                select: "_id name src alt title caption",
            },
            {
                path: "child",
                populate: {
                    path: "images",
                    select: "_id name src alt title caption",
                },
            },
            {
                path: "resultImg",
                select: "_id name src alt title caption",
            },
            {
                path: "avatar",
                select: "_id name src alt title caption",
            },
        ]
    },
    {
        path: "packages",
        populate: [
            {
                path: "thumbnail",
                select: "_id name src alt title caption",
            },
            {
                path: "category",
            }
        ],
    },
    {
        path: "faqs",
        populate: {
            path: "thumbnail",
            select: "_id name src alt title caption",
        },
    },
];

const POPULATE_FAQ = [
    {
        path: "images",
        select: "_id name src alt title caption",
    },
    {
        path: "faq",
        populate: [
        ],
        select: "_id name slug description updatedDay lang status publishedAt ",
    },
];


module.exports = {
    POPULATE_HOME,
    POPULATE_SERVICES,
    POPULATE_FAQ,
};
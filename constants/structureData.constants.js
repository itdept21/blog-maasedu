const STRUCTURE_DATA_TYPE = {
    HOME: "home",
    ABOUT: "about",
    POLICY: "policy",
    OUR_SERVICE: "our-services",
    BLOG: "blog",
    SEARCH: "search",
    ARTICLE: "article",
}

const TARGET_AUDIENCE = [
    "Human Resource Management",
    "Marketing",
    "Business",
    "Health & Nursing"
]

const SERVICES_TYPE = [
    "AcademicWriting",
    "EssayWritingService",
    "DissertationWritingService"
];

//inLanguage
const IN_LANGUAGE = "en-US";

module.exports = {
    STRUCTURE_DATA_TYPE,
    TARGET_AUDIENCE,
    SERVICES_TYPE,
    IN_LANGUAGE,
}
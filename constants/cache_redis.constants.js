const time_cache = 60 * 2;

const time_cache_userprofile = 60 * 60 * 24 * 365;

module.exports = {
    time_cache,
    time_cache_userprofile,
}
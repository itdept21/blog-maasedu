// const process_email = 'order@passableessay.com'
const process_email = 'itdept@maasedu.com'

const dev_email = ''

const host_admin = 'https://server-passableessay.maasedtech.com'
const host_ui = ''

module.exports = {
    process_email,
    host_admin,
    host_ui,
    dev_email
}
const TYPE_OF_SERVICES = [
    { id: "main_services", name: 'Main Services' },
    { id: "add_on_services", name: 'Additional Services' },
    { id: "package", name: 'Package' },
]

const TYPE = [
    { id: "essay", name: 'Essay' },
    { id: "dissertation", name: 'Dissertation' },
    //Research Paper 
    { id: "research_paper", name: 'Research Paper' },
    // Research Project (PhD, DBA)
    { id: "research_project", name: 'Research Project (PhD, DBA)' },
    // Personal Statement
    { id: "personal_statement", name: 'Personal Statement' },
    // Paraphrase
    { id: "paraphrase", name: 'Paraphrase' },
    // Online Exam
    { id: "online_exam", name: 'Online Exam' },
    // Online Exam (PhD, DBA)
    { id: "online_exam_phd_dba", name: 'Online Exam (PhD, DBA)' },
    // Bài luận Tiếng Việt
    { id: "vietnamese_essay", name: 'Bài luận Tiếng Việt' },

    // Slide
    { id: "slide", name: 'Slide' },
    // Poster
    { id: "poster", name: 'Poster' },
    // Chart & Graph
    { id: "chart_graph", name: 'Chart & Graph' },
    // Essay Summary
    { id: "essay_summary", name: 'Essay Summary' },
    // Dissertation Summary
    { id: "dissertation_summary", name: 'Dissertation Summary' },
    // Ethics Form
    { id: "ethics_form", name: 'Ethics Form' },
]

const BASE_PRICE = [
    // Per 1000 word 
    { id: "per_1000_word", name: 'Per 1000 word' },
    // Per length of time 
    { id: "per_length_of_time", name: 'Per length of time' },
    // Per Slide
    { id: "per_slide", name: 'Per Slide' },
    // Per Poster
    { id: "per_poster", name: 'Per Poster' },
    // Per Chart & Graph
    { id: "per_chart_graph", name: 'Per Chart & Graph' },
    // Per Time
    { id: "per_time", name: 'Per Time' },
]

const LIST_STATUS_EN = [
    { id: 0, name: 'Private' },
    {
        id: 1, name: 'Public, noindex'
    },
    { id: 2, name: 'Public, index' },
]

module.exports = {
    TYPE_OF_SERVICES,
    TYPE,
    BASE_PRICE,
    LIST_STATUS_EN,
};
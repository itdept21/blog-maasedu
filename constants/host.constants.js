const FE_HOST = 'https://blog.maasedu.com';
const BE_HOST = 'https://blog-maasedu.maasedtech.com';
const IMAGES_HOST = 'https://cms-maasedu.maasedtech.com';

module.exports = {
    BE_HOST, FE_HOST, IMAGES_HOST
}
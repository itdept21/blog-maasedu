const STATUS_CODE = {
	OK: 200,
	CREATED: 201,
	ACCEPTED: 202,
	NON_AUTH_INFO: 203,
	REDIRECT: 301,
	FOUND: 302,
	BAD_REQUEST: 400,
	UNAUTHORIZED: 401,
	FORBIDDEN: 403,
	NOT_FOUND: 404,
	UNSUPPORTED_MEDIA_TYPE: 415,
	SERVER_ERROR: 500,
};

const MESSAGES = {
	GET_SUCCEED: "get_succeed",
	CREATED_SUCCEED: "created_model_succeed",
	CREATED_SUCCEED_MANY: "created_many_model_succeed",
	DELETE_SUCCEED: "delete_succeed",
	UPDATE_SUCCEED: "update_succeed",
	CANNOT_GET_DATA: "cannot_get_data",
	CANNOT_CREATE: "cannot_create_new_model",
	CANNOT_DELETE: "cannot_deleted",
	INSERT_SUCCEED: "insert_succeed",
	PERMISSION_DENIED: "permission_denied",
	UNAUTHORIZED: "unauthorized",
	INVALID_TOKEN: "invalid_token",
	CANNOT_UPLOAD_IMAGE: "cannot_upload_image",
	INVALID_QUERY: "invalid_query",
	UNAUTHORIZED_ADMIN: "unauthorized admin request",
	DATA_NOT_FOUND: "data_not_found",
	FIELD_MANDATORY: "field_mandatory",

	// create tracking success
	TRACKING_VIEW_SERVICE_SUCCEED: "tracking_view_service_succeed",
	TRACKING_VIEW_CATEGORY_SUCCEED: "tracking_view_category_succeed",

};

const MANDATORY = (field = '') => {
	return `${MESSAGES.FIELD_MANDATORY}: ${field}`;
}

const LANGUAGES = { vi: "vi", en: "en" };
const LANGUAGES_REVERSE = { vi: "en", en: "vi" };
const LANGUAGES_REVERSE_STRING = { vi: "-en", en: "-vi" };
const LANGUAGE_ENUM = [LANGUAGES.vi, LANGUAGES.en];

function RESPONSE_DATA({ error = false, data = null, message = "" }) {
	// if (!error && !data) return { message: "some_thing_went_wrong_response_data" };

	if (error && data) return { message: "some_thing_went_wrong_response_data" };

	return { error, data, message };
}


const POST_DEFAULT = {
	viName: "(Chưa có tiêu đề)",
	viSlug: "chua-co-tieu-de",
	enName: "(non titled)",
	enSlug: "non-titled",
};

/**
 *	CREATE ARRAY OF PAGINATION LINKS
 * @param {*} total
 * @param {*} limit
 * @param {*} url
 * @returns LINKS ARRAY
 */
const PAGINATION = (total = 0, limit = 0, url = "", oldQuery = "") => {
	// const URL = url.match(/\?/) ? url : url + "?";
	const pagination = [];
	// console.log({ total, limit, url, oldQuery });
	for (i = 1; i <= myMathRound(total / limit); i++) {
		pagination.push(url + oldQuery.replace(/&page=\d*|page=\d*/, "") + "&page=" + i);
	}

	return pagination;
};
const ROLE_NAMES = ["Quản trị viên", "SEOer", "Editor"];

function GET_SEO(seo = {}) {
	const result = {};
	result.title = seo?.titles?.[0] || "";
	result.description = seo?.descriptions?.[0] || "";
	result.keywords = seo?.contents?.[0] || "";
	result.image = seo?.images?.[0]?.src || "/assets/img/Main-banner.png";
	return result;
}

/**
 * Add suitable field langSlug and langName for root Object
 * @param {*} dataUpdate
 * @param {*} lang
 * @param {*} name
 * @param {*} slug
 * @returns new data update with viSlug,viName or enSlug,enName
 */
function UPDATE_NAME_SLUG_BY_LANG({ dataUpdate = {}, lang = 'vi', name = '', slug = '' }) {
	const update =
		lang == "vi"
			? { viSlug: slug, viName: name }
			: { enSlug: slug, enName: name };
	return { ...dataUpdate, ...update };
}

function FORMAT_ID_FN(id = '') {
	return "......" + id.slice(id.length - 6);
}

function FORMAT_CURRENCY_FN(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

/**
 * @param promise A promise to resolve
 * @nthTry Number of tries before rejecting
 * @desc Retries a promise n no. of times before rejecting.
 * @returns resolved promise
 */
async function retryPromise(promise, nthTry) {
	try {
		// try to resolve the promise
		const data = await promise;
		// if resolved simply return the result back to the caller
		if (data.error) {
			if (nthTry === 1) {
				return data;
			}
			return retryPromise(promise, nthTry - 1);
		}
		return data;
	} catch (e) {
		// if the promise fails and we are down to 1 try we reject
		if (nthTry === 1) {
			return Promise.reject(e);
		}
		// if the promise fails and the current try is not equal to 1
		// we call this function again from itself but this time
		// we reduce the no. of tries by one
		// so that eventually we reach to "1 try left" where we know we have to stop and reject
		console.log('retrying', nthTry, 'time');
		// we return whatever is the result of calling the same function
		return retryPromise(promise, nthTry - 1);
	}
};

const LIST_STATUS_PRODUCT = [
	{ id: -2, name: 'Trash' },
	{ id: -1, name: 'Private, noindex' },
	{ id: 0, name: 'Redirect, noindex' },
	{
		id: 1, name: 'Public, noindex'
	},
	{ id: 2, name: 'Public, index' },
]

const LIST_STATUS_POST = [
	{ id: -2, name: 'Trash' },
	{ id: -1, name: 'Private, noindex' },
	{ id: 0, name: 'Redirect, noindex' },
	{
		id: 1, name: 'Public, noindex'
	},
	{ id: 2, name: 'Public, index' },
]

const LIST_STATUS_PRODUCT_CATE_EN = [
	{ id: 0, name: 'Private' },
	{
		id: 1, name: 'Public, noindex'
	},
	{ id: 2, name: 'Public, index' },
]

const LIST_STATUS_POST_CATE_EN = [
	{ id: 0, name: 'Private' },
	{
		id: 1, name: 'Public, noindex'
	},
	{ id: 2, name: 'Public, index' },
]

const LIST_STATUS = [
	{ id: 0, name: 'Private' },
	{
		id: 1, name: 'Public, noindex'
	},
	{ id: 2, name: 'Public, index' },
]

const myMathRound = (num) => num + (num - parseInt(num) > 0 ? 1 : 0);

const DOC_TYPES = ["doc", "pdf", "txt"];

const FILE_TYPES = ["doc", "pdf", "txt"];

const formatData = (data) => {
	return {
		_id: data?._id || "",
		lang: data?.lang || "en",
		name: data?.name || "",
		title: data?.titles?.[0] || "",
		content: data?.contents?.[0] || "",
		description: data?.descriptions?.[0] || "",
		image: data?.images?.[0] || {},
		buttonName: data?.buttonName || "",
		buttonLink: data?.buttonLink || "",
		linkUrl: data?.url?.[0] || "",
		packages: data?.packages || [],
		faq: data?.faq || [],
		blog: data?.blog || [],
		blogParent: data?.blogParent || [],
		info: data?.info || [],
		customerFeedbacks: data?.customerFeedbacks || [],
		child: data?.child?.map((e) => {
			return {
				_id: e._id,
				lang: e.lang,
				name: e.name,
				title: e?.titles?.[0] || "",
				content: e?.contents?.[0] || "",
				description: e?.descriptions?.[0] || "",
				image: e?.images?.[0] || {},
				url: e?.url?.[0] || "",
				buttonName: e?.buttonName || "",
				buttonLink: e?.buttonLink || "",
			};
		}) || [],
		child1: data?.child1?.map((e) => {
			return {
				_id: e._id,
				lang: e.lang,
				name: e.name,
				title: e?.titles?.[0] || "",
				content: e?.contents?.[0] || "",
				description: e?.descriptions?.[0] || "",
				image: e?.images?.[0] || {},
				url: e?.url?.[0] || "",
			};
		}) || [],
	}
}

const formatDataSEO = (data) => {
	return {
		seo_title: data?.titles[0] || '',
		seo_descriptions: data?.descriptions[0] || '',
		seo_h1: data?.seo_h1 || '',
		seo_keyword: data?.contents[0] || '',
		seo_schema: data?.seo_schema || '',
		seo_canonical: data?.seo_canonical || '',
		seo_lang: data?.seo_lang || data?.lang || 'en',
		seo_image: data?.images[0]?.src ? {
			src: data?.images[0]?.src || '',
			name: data?.images[0]?.name || '',
			alt: data?.images[0]?.alt || '',
			caption: data?.images[0]?.caption || '',
		} : {},
	}
};

module.exports = {
	LANGUAGE_ENUM,
	STATUS_CODE,
	MESSAGES,
	RESPONSE_DATA,
	POST_DEFAULT,
	LANGUAGES,
	PAGINATION,
	myMathRound,
	GET_SEO,
	ROLE_NAMES,
	LANGUAGES_REVERSE,
	LANGUAGES_REVERSE_STRING,
	UPDATE_NAME_SLUG_BY_LANG,
	FORMAT_ID_FN,
	FORMAT_CURRENCY_FN,
	MANDATORY,
	retryPromise,
	LIST_STATUS_PRODUCT, LIST_STATUS_POST,
	LIST_STATUS_PRODUCT_CATE_EN,
	LIST_STATUS_POST_CATE_EN,
	LIST_STATUS,
	DOC_TYPES,
	formatData,
	formatDataSEO
};


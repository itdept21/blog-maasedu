const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const { DOC_TYPES } = require("../constants/base.constants");

const docSchema = new Schema({
	name: { type: String, require: true },

	src: { type: String, required: true },

	type: { type: String, enum: DOC_TYPES },

	createdAt: { type: Date, require: true, default: new Date() },

	modifiedAt: { type: Date, require: true, default: new Date() },
});

const DOCS_MODEL = mongoose.model("doc", docSchema);

module.exports = DOCS_MODEL;

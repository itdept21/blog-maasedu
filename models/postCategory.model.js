const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postCategory = new Schema({
    vi: { type: mongoose.SchemaTypes.ObjectId, ref: "post_category_info", default: null },
    viName: { type: String, trim: true },
    viStatus: { type: Number, default: 0, required: true },
    viSlug: { type: String, trim: true },

    en: { type: mongoose.SchemaTypes.ObjectId, ref: "post_category_info", default: null },
    enName: { type: String, trim: true },
    enStatus: { type: Number, default: 0, required: true },
    enSlug: { type: String, trim: true },

    thumbnail: { type: mongoose.Types.ObjectId, ref: "image" },

    parent: { type: mongoose.Types.ObjectId, ref: "post_category", default: null },

    createdAt: { type: Date, default: new Date() },
    modifiedAt: { type: Date, default: new Date() },
});

postCategory.index({ viName: "text", viSlug: "text", enName: "text", enSlug: "text" });

module.exports = mongoose.model("post_category", postCategory);

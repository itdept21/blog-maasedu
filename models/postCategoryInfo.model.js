const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postCategoryInfo = new Schema({
    name: String,

    status: { type: Number, default: 0, required: true },

    description: String,

    lang: { type: String, enum: ["vi", "en"] },

    slug: { type: String, require: false, trim: true },

    /* SEO meta */
    seo_title: { type: String, trim: true },
    seo_description: { type: String, trim: true },
    seo_keywords: { type: String, trim: true },
    seo_schema: { type: String, trim: true },
    seo_canonical: { type: String, required: false },
    seo_redirect: { type: String, required: false },
    seo_lang: { type: String, required: false },

    content: { type: String, require: false, trim: true },

    // refs
    postCategory: { type: mongoose.Types.ObjectId, ref: "post_category" },

    /* analytics */
    views: { type: Number, require: false, default: 0 },

    // default
    createdAt: { type: Date, default: new Date() },
    modifiedAt: { type: Date, default: new Date() },
});

postCategoryInfo.index({
    name: "text",
    description: "text",
    slug: "text",
    content: "text",
});

const PRODUCT_CATEGORY_INFO_MODEL = mongoose.model(
    "post_category_info",
    postCategoryInfo
);

module.exports = PRODUCT_CATEGORY_INFO_MODEL;

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserOTPSchema = new Schema({
    user: { type: mongoose.Types.ObjectId, ref: 'users', default: null },
    code: String, // 6 ký tự
    expireAt: {
        type: Date,
        default: Date.now,
        index: { expires: '5m' },
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
    modifiedAt: {
        type: Date,
        default: new Date(),
    },
});

UserOTPSchema.index({ "expireAt": 1 }, { expireAfterSeconds: 0 });

const UserOTP_MODEL = mongoose.model('UserOTP', UserOTPSchema);

module.exports = UserOTP_MODEL;
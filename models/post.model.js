const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema({
	vi: { type: mongoose.SchemaTypes.ObjectId, ref: "article" },
	viName: { type: String, trim: true },
	viSlug: { type: String, trim: true },

	en: { type: mongoose.SchemaTypes.ObjectId, ref: "article" },
	enName: { type: String, trim: true },
	enSlug: { type: String, trim: true },

	createdAt: { type: Date, default: new Date() },
	modifiedAt: { type: Date, default: new Date() },

	category: { type: mongoose.SchemaTypes.ObjectId, ref: "post_category" },
});

postSchema.index({ viName: "text", viSlug: "text", enName: "text", enSlug: "text" });

const POST_MODEL = mongoose.model("post", postSchema);

module.exports = POST_MODEL;

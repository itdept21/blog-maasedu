const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pageComponentInfoSchema = new Schema({
	name: { type: String, trim: true, required: true },
	page: { type: String, trim: true, required: true },

	title: { type: String, trim: true },
	content: { type: String, trim: true },
	description: { type: String, trim: true },

	url: { type: String, trim: true },

	parent: { type: mongoose.Types.ObjectId, ref: "page_component_info", default: null },

	child: [{ type: mongoose.Types.ObjectId, ref: "page_component_info", default: null }],

	images: [{ type: mongoose.Types.ObjectId, ref: "image" }],

	avatar: { type: mongoose.Types.ObjectId, ref: "image" },

	lang: { type: String, trim: true, enum: ["vi", "en"], default: "en", required: true },

	major: { type: String, trim: true },

	resultScore: { type: String, trim: true },

	resultImg: { type: mongoose.Types.ObjectId, ref: "image" },

	ratingValue: { type: String, trim: true },

	// thông tin chung
	// 1 biến linh hoạt.
	common: [{ type: Schema.Types.Mixed, trim: true }],

	// thứ tự hiển thị
	displayOrder: { type: Number, default: 0 },

	/* SEO meta */
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_lang: { type: String, required: false },

	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_redirect: { type: String, required: false },
	seo_h1: { type: String, required: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },
});
pageComponentInfoSchema.index({ title: "text", content: "text", description: "text" });

const PAGE_COMPONENT_INFO_MODEL = mongoose.model("page_component_info", pageComponentInfoSchema);

module.exports = PAGE_COMPONENT_INFO_MODEL;

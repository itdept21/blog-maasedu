const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pageComponentSchema = new Schema({
	name: { type: String, trim: true, required: true },
	page: { type: String, trim: true, required: true },

	titles: [{ type: String, trim: true }],
	contents: [{ type: String, trim: true }],
	descriptions: [{ type: String, trim: true }],

	info: [{ type: mongoose.Types.ObjectId, ref: "page_component_info", default: null }],

	dataCounters: [{ type: mongoose.Types.ObjectId, ref: "page_component_info", default: null }],

	customerFeedbacks: [{ type: mongoose.Types.ObjectId, ref: "page_component_info", default: null }],

	url: [{ type: String, trim: true }],

	parent: { type: mongoose.Types.ObjectId, ref: "page_component", default: null },

	child: [{ type: mongoose.Types.ObjectId, ref: "page_component", default: null }],

	child1: [{ type: mongoose.Types.ObjectId, ref: "page_component", default: null }],

	images: [{ type: mongoose.Types.ObjectId, ref: "image" }],

	lang: { type: String, trim: true, enum: ["vi", "en"], default: "en", required: true },

	// button name
	buttonName: { type: String, trim: true },

	// button link
	buttonLink: { type: String, trim: true },

	// lưu package cho our services
	packages: [{ type: mongoose.Types.ObjectId, ref: "service", default: null }],

	faq: [{ type: mongoose.Types.ObjectId, ref: "faq", default: null }],

	blog: [{ type: mongoose.Types.ObjectId, ref: "article", default: null }],
	blogParent: [{ type: mongoose.Types.ObjectId, ref: "post_category", default: null }],

	upComingEvents: [{ type: mongoose.Types.ObjectId, ref: "event", default: null }],
	pastEvents: [{ type: mongoose.Types.ObjectId, ref: "event", default: null }],

	staff: [{ type: mongoose.Types.ObjectId, ref: "staff", default: null }],

	// file
	docs: [{ type: mongoose.Types.ObjectId, ref: "doc", default: null }],

	/* SEO meta */
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_lang: { type: String, required: false },

	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_redirect: { type: String, required: false },
	seo_h1: { type: String, required: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },
});

const PAGE_COMPONENT_MODEL = mongoose.model("page_component", pageComponentSchema);

module.exports = PAGE_COMPONENT_MODEL;

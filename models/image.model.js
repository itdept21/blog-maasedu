const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const image = new Schema({
	name: { type: String, require: true },
	/* status: {type: Number, default: 1}, */
	src: { type: String, required: true },

	alt: { type: String, required: false },
	title: { type: String, required: false },
	caption: { type: String, required: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },
});

image.index({ alt: "text", name: "text" });

const IMAGE_MODEL = mongoose.model("image", image);

module.exports = IMAGE_MODEL;

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const article = new Schema({
	lang: { type: String, enum: ["vi", "en"] },

	name: { type: String, require: false, trim: true },

	description: { type: String, require: false, trim: true },

	content: { type: String, require: false, trim: true },

	thumbnail: { type: mongoose.Types.ObjectId, ref: "image", require: false },

	category: { type: mongoose.SchemaTypes.ObjectId, ref: "post_category" },

	slug: { type: String, require: false, index: { unique: true } },

	/*** STATUS
	 * 2 - public, index
	 * 1 - public, noindex
	 * 0 - private
	 * -1 - trash
	 ***/
	status: { type: Number, require: false },

	/* analytics */
	views: { type: Number, require: false, default: 0 },
	shared: { type: Number, require: false, default: 0 },
	postAt: { type: Date, require: false },

	post: { type: mongoose.Types.ObjectId, ref: "post" },

	hashTags: [{ type: String }],

	// author: { type: String, require: false, trim: true },
	author: { type: mongoose.SchemaTypes.ObjectId, ref: "page_component_info" },
	toc: { type: String },
	menuList: [{ type: Schema.Types.Mixed, require: false }],

	related: [{ type: mongoose.SchemaTypes.ObjectId, ref: "article", default: [] }],

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },

	focus_keyphrase: { type: String, trim: true }, // giong wp

	/* SEO meta */
	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_redirect: { type: String, required: false },
	seo_lang: { type: String, required: false },

});

article.index({ title: "text", slug: "text", description: "text", hashTags: "text", name: "text", });
// article.index({ name: "text", slug: "text", description: "text", hashTags: "text" });

module.exports = mongoose.model("article", article);

// @ts-nocheck
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const admin = new Schema({
	/* username - unique index */
	facebookID: { type: String },

	username: { type: String, require: true, minlength: 6 },

	/* password is hashed by bcrypt */
	password: { type: String, minlength: 6, require: true },

	name: { type: String, require: true },

	createdAt: { type: Date, require: true, default: new Date() },

	modifiedAt: { type: Date, require: true, default: new Date() },
	email: String,

	/* role
	 * 0 - editor
	 * 1 - SEOer
	 * 2 - admin
	 */
	role: { type: Number, require: true, default: 1 },

	// 1 nam . 0 nữ
	gender: { type: Number, default: 1 },

	phone: { type: String, default: "" },
});

admin.methods.setPassword = function (password) {
	this.password = bcrypt.hashSync(password, 10);
};

admin.methods.comparePassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};

const ADMIN_MODEL = mongoose.model("admins", admin);

module.exports = ADMIN_MODEL;

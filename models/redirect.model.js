const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = new Schema({
    url: { type: String },
    oldUrl: { type: String },
    statusCode: { type: Number, default: 301 },

    lang: { type: String, enum: ["vi", "en"] },

    createdAt: { type: Date, require: false, default: new Date() },
    createdBy: { type: String, require: false, default: null },
    modifiedAt: { type: Date, require: false, default: new Date() },
    modifiedBy: { type: String, require: false, default: null },
});

model.index({ url: "text", oldUrl: "text" });

module.exports = mongoose.model("redirects", model);

// @ts-nocheck
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const User = new Schema({
	/* username - unique index */
	facebookID: { type: String, default: null },
	googleID: { type: String, default: null },
	email: { type: String, default: null },

	username: { type: String, minlength: 6 },

	/* password is hashed by bcrypt */
	password: { type: String, minlength: 6, require: true },

	fullName: { type: String, require: true },

	/* Anh dai dien */
	avatar: { type: mongoose.Types.ObjectId, ref: "image" },

	createdAt: { type: Date, require: true, default: new Date() },

	modifiedAt: { type: Date, require: true, default: new Date() },
});

User.methods.setPassword = function (password) {
	this.password = bcrypt.hashSync(password, 10);
};

User.methods.comparePassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};

User.index({
	email: "text",
	username: "text",
	fullName: "text",
});

const USER_MODEL = mongoose.model("user", User);

module.exports = USER_MODEL;

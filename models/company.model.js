const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = new Schema({
    page: { type: String, trim: true, required: true },
    // maasedu.vn
    url: { type: String, trim: true, required: true },
    //"MAAS EDTECH – The best Academic Writing Services for Students from all over the world",
    name: { type: String, trim: true, required: true },
    legalName: { type: String, trim: true },
    additionalType: [{ type: String, trim: true }],
    description: { type: String, trim: true },
    mainEntityOfPage: { type: String, trim: true },
    sameAs: [{ type: String, trim: true }],
    currenciesAccepted: { type: String, trim: true },
    dayOfWeek: [{ type: String, trim: true }],
    openTime: { type: String, trim: true },
    closeTime: { type: String, trim: true },
    paymentAccepted: [{ type: String, trim: true }],
    priceRange: { type: String, trim: true },
    logo: { type: Schema.Types.Mixed, require: false },
    image: { type: Schema.Types.Mixed, require: false },
    officeLocation: { type: Schema.Types.Mixed, require: false },
    geo: { type: Schema.Types.Mixed, require: false },
    hasMap: { type: String, trim: true },
    telephone: { type: String, trim: true },
    foundingDate: { type: String, trim: true },
    foundingLocation: { type: Schema.Types.Mixed, require: false },
    knowsLanguage: [{ type: String, trim: true }],
    numberOfEmployees: { type: String, trim: true },
    taxID: { type: String, trim: true },
    contactPoint: { type: Schema.Types.Mixed, require: false },

    datePublished: { type: String, trim: true },
    dateModified: { type: String, trim: true },

    areaServed: [{ type: Schema.Types.String, require: false }],

    targetAudience: [{ type: Schema.Types.String, require: false }],

});

module.exports = mongoose.model("company", model);

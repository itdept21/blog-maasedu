// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const ArticleService = require("../services/article.services");
const PostCategoryService = require("../services/postCategory.services");
const ComponentInfoService = require("../services/pageComponentInfo.services");
const service = require("../services/post.services");
const { STATUS_CODE, MESSAGES, RESPONSE_DATA, PAGINATION, LANGUAGES_REVERSE, FORMAT_ID_FN, LIST_STATUS_POST } = require("../constants/base.constants");

const { DATE_FORMAT_FN } = require("../helpers/dateTimeFormat");
const createToc = require("../helpers/createToc");
const CURRENCY_FORMAT_FN = require("../helpers/currencyFormat");
const { SEOContentAnalyzer, SEOFocusKeyPhrase, SEOPageAnalysis, SEOAdditionalFocusKeyPhrases } = require("../seo");
const keyword_extractor = require("keyword-extractor");
const { wordExists, createEdgeNGramsRegex } = require("../helpers/common");
const jsdom = require("jsdom");
const { FE_HOST } = require("../constants/host.constants");
const { JSDOM } = jsdom;
const axios = require('axios');

class PostController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createEmptyPost = async (req, res, next) => {
		const signalResponse = await this.service.createID();
		res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.CREATED).json(signalResponse);
	};

	renderList = async (req, res, next) => {
		const { page = 1, search = "", ...filter } = req.query;

		Object.entries(filter).forEach(([key, value]) => {
			if (!filter[key]) {
				delete filter[key];
			}
		});

		let status = null
		if (filter.status) {
			status = parseInt(filter.status)
			const signalGetListStatus = await ArticleService.getList({
				matchConditions: { status },
			});
			const listArticleIds = signalGetListStatus.data.map(item => item._id);
			if (listArticleIds.length > 0) {
				filter['en'] = { $in: listArticleIds }
			} else {
				filter['en'] = { $in: [null] }
			}
			delete filter['status']
		};

		const url = req._parsedOriginalUrl.pathname;
		const oldQuery = req._parsedOriginalUrl.search || "?";

		if (page < 0) return res.sendStatus(400);
		const limit = 10;
		const skip = (page - 1) * limit;

		const postConditions = search ? { $text: { $search: search } } : {};

		const postPopulates = [{ path: 'category', populate: { path: 'vi en' } }, 'vi', 'en'];
		const signalGetList = await this.service.getList({
			skip, limit,
			matchConditions: { ...postConditions, ...filter },
			populates: postPopulates,
			sort: { createdAt: -1, modifiedAt: -1 }
		});
		const signalGetCount = await this.service.count({ ...postConditions, ...filter });

		const total = signalGetCount.data;
		const pagination = PAGINATION(total, limit, url, oldQuery);

		if (signalGetList.error) next();
		const posts = signalGetList.data;

		const categories = await PostCategoryService.getListCategory();

		// list status
		const listStatus = LIST_STATUS_POST

		return res.render("admin", {
			inc: "inc/admin/posts",
			title: "Danh sách bài viết",
			posts,
			pagination,
			page,
			total,
			search,
			listCategories: categories,
			listStatus, status,
			filter,
			FORMAT_ID_FN,
			DATE_FORMAT_FN,
		});
	};

	renderEdit = async (req, res, next) => {
		const { id } = req.params;
		const lang = req.query.lang || "en";
		const switchLangs = {
			vi: `/admin/post/${id}?lang=vi`,
			en: `/admin/post/${id}?lang=en`,
		};

		const signalGetPost = await this.service.get({ _id: id.trim() });

		const post = signalGetPost?.data || { _id: id }

		const signalGetArticle = await ArticleService.get({ post: id, lang }, ["thumbnail"]);

		const article = signalGetArticle?.data ? {
			...signalGetArticle?.data,
			hashTags: signalGetArticle?.data?.hashTags?.join(", ") || ' '
		} : {};

		const categories = await PostCategoryService.getListCategory();

		const signalGetRelated = await ArticleService.getList({
			matchConditions: {
				status: { $gte: 1 },
			},
			sort: { displayOrder: 1, modifiedAt: -1, createdAt: -1 }
		});
		const listRelated = signalGetRelated.data || [];

		// 
		const signalGetCustomerFeedbacks = await ComponentInfoService.getList({
			matchConditions: {
				page: { $in: ["blog"] },
				name: { $in: ['author'] },
				lang,
				parent: null,
				child: {
					$ne: []
				}
			},
			populates: [
				"images",
				"child",
				{
					path: "child",
					populate: {
						path: "images",
					},
				},
			],
		});

		let seoAnalyzer = [];
		let seoAnalyzerScore = 0;

		let seoAnalyzerAdditions = [];

		let pageAnalysis = {};
		let pageAnalysisScore = 0;

		let listInternalLink = []

		if (article.seo_title && article.seo_description && article.content && article.slug) {
			// const _seo_keywords = article.seo_keywords;
			const _seo_keywords = article.focus_keyphrase?.split(",") || [article.focus_keyphrase];
			const _keyPhrase = _seo_keywords?.[0] || article.focus_keyphrase;

			// focus keyPhrase
			const SEOContentAnalyzer = SEOFocusKeyPhrase({
				keyword: _keyPhrase,
				title: article.seo_title,
				descriptionMeta: article.seo_description,
				content: article.content,
				slug: article.slug,
			});

			seoAnalyzer = SEOContentAnalyzer.analysis;
			seoAnalyzerScore = SEOContentAnalyzer.score;


			// additional keyPhrase
			if (_seo_keywords.length > 1) {
				const _keyPhraseChild = _seo_keywords.filter(item => item !== _keyPhrase);

				const SEOContentAdditionsAnalyzer = SEOAdditionalFocusKeyPhrases(
					[..._keyPhraseChild],
					{

						title: article.seo_title,
						descriptionMeta: article.seo_description,
						content: article.content,
						slug: article.slug,
					});

				seoAnalyzerAdditions = SEOContentAdditionsAnalyzer;
			}

			// page Analysis
			const SEOPageAnalyzer = SEOPageAnalysis({
				keyword: _keyPhrase,
				title: article.seo_title,
				descriptionMeta: article.seo_description,
				content: article.content,
				slug: article.slug,
			});

			pageAnalysis = SEOPageAnalyzer.analysis;
			pageAnalysisScore = SEOPageAnalyzer.score;

		};

		const listKeyPhrases = [];
		// // listInternalLink
		let checkCount = 0;

		return res.render("admin", {
			inc: "inc/admin/post",
			title: "Chỉnh sửa bài viết",
			post,
			article,
			listRelated,
			lang,
			switchLangs,
			categories,
			listAuthor: signalGetCustomerFeedbacks?.data || [],

			listKeyPhrases,
			listInternalLink,

			seoAnalyzer: seoAnalyzer,
			seoAnalyzerScore,

			seoAnalyzerAdditions,

			pageAnalysis: pageAnalysis,
			pageAnalysisScore: pageAnalysisScore,

			DATE_FORMAT_FN,
			CURRENCY_FORMAT_FN,
			SEOContentAnalyzer,
		});
	};

	checkFocusKeyPhrase = async (req, res, next) => {
		const dataUpdate = req.body;

		console.log("checkFocusKeyPhrase dataUpdate",);

		let seoAnalyzer = [];
		let seoAnalyzerScore = 0;
		let seoAnalyzerAdditions = [];
		let pageAnalysis = {};
		let pageAnalysisScore = 0;

		let listInternalLink = [];

		if (dataUpdate.title && dataUpdate.descriptionMeta && dataUpdate.content && dataUpdate.slug) {

			const _keyword = dataUpdate?.keyword?.split(",") || [dataUpdate.keyword];
			const _keyPhrase = _keyword?.[0] || dataUpdate.keyword;

			// focus keyPhrase
			const SEOContentAnalyzer = SEOFocusKeyPhrase({
				keyword: _keyPhrase,
				title: dataUpdate.title,
				descriptionMeta: dataUpdate.descriptionMeta,
				content: dataUpdate.content,
				slug: dataUpdate.slug,
			});

			seoAnalyzer = SEOContentAnalyzer.analysis;
			seoAnalyzerScore = SEOContentAnalyzer.score;

			// additional keyPhrase
			if (_keyword.length > 1) {
				const _keyPhraseChild = _keyword.filter(item => item !== _keyPhrase);

				const SEOContentAdditionsAnalyzer = SEOAdditionalFocusKeyPhrases(
					[..._keyPhraseChild],
					{

						title: dataUpdate.title,
						descriptionMeta: dataUpdate.descriptionMeta,
						content: dataUpdate.content,
						slug: dataUpdate.slug,
					});

				seoAnalyzerAdditions = SEOContentAdditionsAnalyzer;
			}

			// page Analysis
			const SEOPageAnalyzer = SEOPageAnalysis({
				keyword: _keyPhrase,
				title: dataUpdate.title,
				descriptionMeta: dataUpdate.descriptionMeta,
				content: dataUpdate.content,
				slug: dataUpdate.slug,
			});

			pageAnalysis = SEOPageAnalyzer.analysis;
			pageAnalysisScore = SEOPageAnalyzer.score;
		};

		const extraction_result =
			keyword_extractor.extract(dataUpdate.content, {
				language: "english",
				remove_digits: true,
				return_changed_case: true,
				remove_duplicates: true,
				return_chained_words: true

			});
		const keyWord = extraction_result.reduce((arr, item) => {
			if (arr.length < 10) {
				if (wordExists(dataUpdate.title, item))
					arr.push({
						key: item,
						title: item
					})
			}

			return arr;
		}, []) || [];

		let listKeyPhrases = keyWord;

		const responseJson = RESPONSE_DATA(
			{
				data: {
					keyword: dataUpdate.keyword,

					listKeyPhrases,
					listInternalLink,

					seoAnalyzer: seoAnalyzer,
					seoAnalyzerScore,
					seoAnalyzerAdditions,

					pageAnalysis: pageAnalysis,
					pageAnalysisScore: pageAnalysisScore,
				},
				message: MESSAGES.CREATED_SUCCEED
			});
		return res.status(STATUS_CODE.OK).json(responseJson);
	};

	getListAdditionalKeyPhrase = async (req, res, next) => {
		const dataUpdate = req.body;
		// console.log("getListAdditionalKeyPhrase", dataUpdate)

		const extraction_result =
			keyword_extractor.extract(dataUpdate.content, {
				language: "english",
				remove_digits: true,
				return_changed_case: true,
				remove_duplicates: true,
				return_chained_words: true

			});
		// console.log("listKeyPhrases extraction_result", extraction_result)

		const keyWord = extraction_result.reduce((arr, item) => {
			if (arr.length < 10) {
				if (dataUpdate.title.includes(item)) {
					arr.push({
						key: item,
						title: item
					})
				}
			}

			return arr;
		}, []) || [];

		let listKeyPhrases = keyWord;
		// console.log("listKeyPhrases 1", listKeyPhrases)

		if (listKeyPhrases.length > -1) {
			const signalAxiosDataKeyword = await axios.get(
				`https://www.google.com/search?q=${dataUpdate.title}`,
			);
			const dataAxios = signalAxiosDataKeyword.data || null
			if (dataAxios !== null) {
				const domDocumentAxios = new JSDOM(dataAxios.toLowerCase()).window.document
				const elementsWithClassTagName = domDocumentAxios.getElementsByTagName('h3');
				// Kiểm tra xem có phần tử nào có class là 'myClass' hay không
				if (elementsWithClassTagName.length > 0) {
					for (let i = 0; i < 5; i++) {
						let _key = elementsWithClassTagName[i]?.textContent?.split("|")?.[0] || elementsWithClassTagName[i]?.textContent
						_key = _key?.split("-")?.[0] || _key
						listKeyPhrases = [
							{
								key: _key,
								title: _key
							},
							...listKeyPhrases
						]
					}
				} else {
					console.log('Không tìm thấy phần tử với class "myClass"');
				}

				// Truy cập thẻ div bằng class và lấy nội dung
				const elementsWithClass = domDocumentAxios.getElementsByTagName('div');
				// Kiểm tra xem có phần tử nào có class là 'myClass' hay không
				if (elementsWithClass.length > 0) {
					for (const div of elementsWithClass) {
						if (div.innerHTML.startsWith(`<div class="lt3tzc">`)) {
							const _key = div.textContent;
							listKeyPhrases = [
								{
									key: _key,
									title: _key
								},
								...listKeyPhrases
							]
						}
						if (div.innerHTML.startsWith(`<span><div class="bneawe s3v9rd ap7wnd lrvwie">`)) {
							if (!div.textContent.includes('pdf')) {
								const _key = div.textContent;
								listKeyPhrases = [
									{
										key: _key,
										title: _key
									},
									...listKeyPhrases
								]
							}
						}
					}
				} else {
					console.log('Không tìm thấy phần tử với class "myClass"');
				}
			}
		}

		const responseJson = RESPONSE_DATA(
			{
				data: {
					keyword: dataUpdate.keyword,

					listKeyPhrases,
				},
				message: MESSAGES.CREATED_SUCCEED
			});
		return res.status(STATUS_CODE.OK).json(responseJson);
	};

	getListInternalLink = async (req, res, next) => {
		const dataUpdate = req.body;
		console.log("getListInternalLink ");

		let listInternalLink = [];

		// listInternalLink
		// sử dụng h2, h3 để get danh sách bài viết theo từng key
		if (dataUpdate.content) {
			let _listKey = []
			const domDocument = new JSDOM(dataUpdate.content.toLowerCase()).window.document;

			const listOfH2 = domDocument.getElementsByTagName('h2')
			if (listOfH2.length > 0) {
				for (const h2 of listOfH2) {
					const regex1 = /1|2|3|4|5|6|7|8|9|0/gi;
					let contentH2 = h2.textContent.replace(regex1, '').replace(/[^\w\s]/gi, '').trim()
					_listKey.push(contentH2)
				}
			}
			const listOfH3 = domDocument.getElementsByTagName('h3')
			if (listOfH3.length > 0) {
				for (const h3 of listOfH3) {
					const regex1 = /1|2|3|4|5|6|7|8|9|0/gi;
					let contentH3 = h3.textContent.replace(regex1, '').replace(/[^\w\s]/gi, '').trim()
					_listKey.push(contentH3)
				}
			}
			const listOfTagP = domDocument.getElementsByTagName('i')
			if (listOfTagP.length > 0) {
				for (const tagP of listOfTagP) {
					_listKey.push(tagP.textContent.replace(/[^\w\s]/gi, '').trim())
				}
			}
			const listOfTagStrong = domDocument.getElementsByTagName('strong')
			if (listOfTagStrong.length > 0) {
				for (const tagStrong of listOfTagStrong) {
					_listKey.push(tagStrong.textContent.replace(/[^\w\s]/gi, '').trim())
				}
			}

			if (_listKey.length > 0) {
				let postConditions = {};
				postConditions['$or'] = [];
				for (let i = 0; i < _listKey.length; i++) {
					const _keyword = _listKey[i];
					postConditions['$or'].push({ name: { $regex: createEdgeNGramsRegex(_keyword), $options: "gi" } });
				};

				if (dataUpdate?.keyword) {
					postConditions['$or'].push({ focus_keyphrase: { $regex: createEdgeNGramsRegex(dataUpdate?.keyword), $options: "gi" } });
				};

				// get list by condition
				const signalGetListPost = await ArticleService.getList({
					matchConditions: {
						...postConditions,
						lang: 'en',
						status: {
							$gte: 1
						}
					},
					select: 'name slug',
					sort: { postAt: -1 },
					limit: 10,
					skip: 0,
				});

				listInternalLink = signalGetListPost?.data?.map((e) => {
					return {
						name: e.name,
						slug: FE_HOST + '/blog/' + e.slug,
						_id: e._id,
					}
				}) || [];
			}
		}

		const responseJson = RESPONSE_DATA(
			{
				data: {
					keyword: dataUpdate.keyword,
					listInternalLink,
				},
				message: MESSAGES.CREATED_SUCCEED
			});
		return res.status(STATUS_CODE.OK).json(responseJson);
	};

	createOrUpdate = async (req, res, next) => {
		try {
			let id = req.params.id.trim();
			const dataUpdate = req.body;
			const lang = dataUpdate.lang.trim();

			dataUpdate['thumbnail'] = dataUpdate['thumbnail'] ? dataUpdate['thumbnail'] : null;
			dataUpdate['category'] = dataUpdate['category'] ? dataUpdate['category'] : null;
			dataUpdate['related'] = dataUpdate['related'] ? dataUpdate['related'] : null;
			dataUpdate['author'] = dataUpdate['author'] ? dataUpdate['author'] : null;

			if (!dataUpdate?.createdAt) dataUpdate.createdAt = new Date();

			// create TOC
			const dataCreate = createToc(dataUpdate.content);
			dataUpdate.toc = dataCreate.html;
			dataUpdate.content = dataCreate.content;

			const _minLevel = dataCreate.minLevel;
			const _listMenuList = dataCreate.menuList.filter((e) => e.level == _minLevel);
			const __menuList = []
			for (let i = 1; i <= _listMenuList.length; i++) {
				const __listMenuList = _listMenuList[i];
				const __listMenuListPre = _listMenuList[i - 1];
				let ___listMenuList = dataCreate.menuList.filter((e) => e._id > __listMenuListPre._id && e._id < __listMenuList?._id);
				if (___listMenuList.length == 0 && i == _listMenuList.length) {
					___listMenuList = dataCreate.menuList.filter((e) => e._id > __listMenuListPre._id)
				}
				__menuList.push({
					...__listMenuListPre,
					child: ___listMenuList
				});
			}
			dataUpdate.menuList = __menuList

			const signalUpdateArticle = await ArticleService.createOrUpdate({ post: id, lang }, dataUpdate);

			if (signalUpdateArticle.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdateArticle);
			const article = signalUpdateArticle.data;

			const postUpdate =
				lang == "vi"
					? { viSlug: article.slug, viName: article.name }
					: { enSlug: article.slug, enName: article.name };

			postUpdate[lang] = article._id;
			postUpdate['category'] = dataUpdate['category'];

			const signalUpdatePost = await this.service.createOrUpdate({ _id: id }, postUpdate);
			if (signalUpdatePost.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdatePost);
			const post = signalUpdatePost.data;

			if (!post[LANGUAGES_REVERSE[lang]]) {
				const signalCreateOppositeLang = await this.updateOppositeLang({
					lang,
					otherLangInfo: article,
					id: post._id, multiLangField: 'post',
					infoService: ArticleService
				});
				if (signalCreateOppositeLang.error) console.log(signalCreateOppositeLang);
			}

			const responseJson = RESPONSE_DATA({ data: { post, article }, message: MESSAGES.CREATED_SUCCEED });
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			console.log("create or update admin post", error)
			return res.status(400).json(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};

	remove = async (req, res, next) => {
		const { id } = req.params;

		const signalGetPost = await this.service.get({ _id: id });
		if (signalGetPost.error) res.status(STATUS_CODE.BAD_REQUEST).json(signalGetPost);

		const post = signalGetPost.data;

		const removeSignals = await Promise.all([
			this.service.remove(post._id),
			ArticleService.remove(post.vi),
			ArticleService.remove(post.en),
		]);

		const signalError = removeSignals.find((e) => e.error == true);
		if (signalError) return res.status(STATUS_CODE.SERVER_ERROR).json(signalError);

		const data = {
			post: removeSignals[0].data,
			articleVi: removeSignals[1].data,
			articleEn: removeSignals[2].data,
		};

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA({ data, message: MESSAGES.DELETE_SUCCEED }));
	};
}

module.exports = new PostController();

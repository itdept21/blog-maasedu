// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/user.services");

const { ROLE_NAMES } = require("../constants/base.constants");

class UserController extends BaseCrudController {
	constructor() {
		super(service);
	}

	create = async (req, res, next) => {
		const signalCreate = await service.createService(req.body)
		return res.status(signalCreate.error ? 400 : 201).json(signalCreate);
	};

	updatePassword = async (req, res, next) => {
		const { password, userID, oldPassword } = req.body;
		if (!req.user?._id) return res.sendStatus(403);

		if (password != oldPassword) {
			return res.status(400).json({ error: true, message: 'Mật khẩu Không khớp' });
		}

		const signalUpdate = await this.service.updatePassword(userID, password);
		return res.status(signalUpdate.error ? 400 : 200).json(signalUpdate);
	};

	renderList = async (req, res, next) => {
		const signalGetListUsers = await this.service.getList({});
		if (signalGetListUsers.error) res.status(500).json(signalGetListUsers);
		const users = signalGetListUsers.data;
		return res.render("admin", {
			inc: "inc/admin/users",
			title: "Danh sách quản trị viên",
			users,
			ROLE_NAMES,
			currentUserID: req.user._id,
			currentUserRole: req.user.role,
		});
	};

	renderUpdatePassword = async (req, res, next) => {
		const userID = req.params.userID || req.user._id;
		const signalGetUserInfo = await this.service.get({ _id: userID });
		if (signalGetUserInfo.error) res.status(500).json(signalGetUserInfo);
		const user = signalGetUserInfo.data;

		return res.render("admin", {
			inc: "inc/admin/reset_pass",
			title: "Đổi mật khẩu quản trị viên",
			user,
		});
	};
}

module.exports = new UserController();

const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/image.services");

class ImageController extends BaseCrudController {
	constructor() {
		super(service);
	}

	create = async (req, res, next) => {
		console.log("req.files", req.files)
		if (req.uploadError) {
			return res.status(400).json({ error: true, message: "upload_error" });
		}
		return res.json({ error: false, message: "created", data: req.files });
	};

	getList = async (req, res, next) => {
		console.log("ImageController getList")
		const { skip = 0, limit = 10, search } = req.query;

		const conditions = search ? { $text: { $search: search } } : {};
		const metaSort = search ? { score: { $meta: "textScore" } } : {};

		const signalResponse = await this.service.getList({
			matchConditions: conditions,
			skip, limit, search,
			sort: { ...metaSort, createdAt: -1 }
		});

		return res.status(signalResponse.error ? 400 : 200).json(signalResponse);
	};

	renderGallery = async (req, res, next) => {
		console.log("ImageController renderGallery", req.query)
		const { skip = 0, limit = 10, search } = req.query;

		const conditions = search ? { $text: { $search: search } } : {};
		const metaSort = search ? { score: { $meta: "textScore" } } : {};

		const sort = { ...metaSort, createdAt: -1 };
		const listImages = await service.getList({ matchConditions: conditions, skip, limit, sort });

		return res.render("gallery", { images: listImages.data, search });
	};

	renderGalleryCkeditor = async (req, res, next) => {
		const { skip = 0, limit = 20, search, CKEditor = '', CKEditorFuncNum = '', langCode = '' } = req.query;
		const conditions = search ? { $text: { $search: search } } : {};
		const metaSort = search ? { score: { $meta: "textScore" } } : {};

		const sort = { ...metaSort, createdAt: -1 };
		const listImages = await service.getList({ matchConditions: conditions, skip, limit, sort });
		return res.render("ckeditor_browser", { images: listImages.data, search, CKEditor, CKEditorFuncNum, langCode });
	};

	update = async (req, res, next) => {
		const { id, ...dataUpdate } = req.body;
		dataUpdate.modifiedBy = req.user.username;

		const signalResponse = await this.service.update({ _id: id }, dataUpdate);
		return res.status(signalResponse.error ? 400 : 200).json(signalResponse);
	};
}

module.exports = new ImageController();

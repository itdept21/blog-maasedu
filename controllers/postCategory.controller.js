// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const InfoService = require("../services/postCategoryInfo.services");
const ArticleService = require("../services/article.services");
const service = require("../services/postCategory.services");
const {
	STATUS_CODE,
	MESSAGES,
	RESPONSE_DATA,
	LANGUAGES,
	UPDATE_NAME_SLUG_BY_LANG }
	= require("../constants/base.constants");

class resultCategoryController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createOrUpdate = async (req, res, next) => {
		try {

			let id = req.params.id.trim();
			const dataUpdate = req.body;
			const lang = dataUpdate.lang.trim();
			dataUpdate['thumbnail'] = dataUpdate['thumbnail'] ? dataUpdate['thumbnail'] : null;

			const signalUpdateInfo = await InfoService.createOrUpdate({ postCategory: id, lang }, dataUpdate);
			if (signalUpdateInfo.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdateInfo);
			const info = signalUpdateInfo.data;

			const update = UPDATE_NAME_SLUG_BY_LANG({ name: info.name, lang, slug: info.slug, status: info.status })
			update[lang] = info._id;
			update.thumbnail = dataUpdate.thumbnail;
			update.parent = dataUpdate?.parent || null;

			const signalUpdate = await this.service.createOrUpdate({ _id: id }, update);
			if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);
			let postCategory = signalUpdate.data;

			const responseJson = RESPONSE_DATA({ data: { postCategory, info }, message: MESSAGES.CREATED_SUCCEED });
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			return res.status(400).send(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};

	getListArticlesBySlugCate = async (req, res, next) => {
		const { slugCate } = req.params;
		const { skip = 0, limit = 20 } = req.query;

		const signalGetCateInfo = await InfoService.get({
			slug: slugCate, status: { $gte: 1 }
		},
			[{ path: 'postCategory', populate: 'thumbnail' }]);

		if (!signalGetCateInfo.data) return res.status(400).json(signalGetCateInfo)
		let catePostInfo = signalGetCateInfo.data;

		catePostInfo.thumbnail = catePostInfo.postCategory.thumbnail
		const otherLangSlug = catePostInfo.lang == 'en' ? catePostInfo.postCategory?.viSlug : catePostInfo.postCategory?.enSlug

		const signalGetArticles = await ArticleService.getList({
			matchConditions: {
				category: catePostInfo?.postCategory || null,
				status: { $gte: 1 },
				lang: catePostInfo.lang,
			},
			skip,
			limit,
			populates: [
				{
					path: 'thumbnail',
					select: 'name src',
				}
			]
		});

		if (signalGetArticles.error) return res.status(400).json(signalGetArticles);

		const listArticles = signalGetArticles.data || null;
		// Total bài viết có cate = slug
		const signalGetCount = await ArticleService.count({
			category: catePostInfo?.postCategory || null,
			status: { $gte: 1 },
			lang: catePostInfo.lang,
		});
		const total = signalGetCount.data;

		delete catePostInfo.postCategory
		const code = signalGetArticles.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK;

		const seo = {
			seo_title: catePostInfo?.seo_title || catePostInfo.name,
			seo_descriptions: catePostInfo?.seo_description || catePostInfo.description,
			seo_image: catePostInfo?.thumbnail?.src || '/images/noimage.jpg',
			seo_keyword: catePostInfo?.seo_keywords || catePostInfo.name,
			seo_lang: catePostInfo?.seo_lang || 'vi',
		}

		return res.status(code).json({
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				otherLangSlug, catePostInfo, listArticles, seo,
				total, typePage: 'categoriesArticle'
			}
		});
	};

	updatePostCategoryStatus = async (req, res, next) => {
		let { id, status } = req.params;
		// validate status
		status = parseInt(status);
		if ([0, 1, 2].indexOf(parseInt(status)) < 0)
			return res.status(STATUS_CODE.BAD_REQUEST).json(RESPONSE_DATA({ error: true, message: "invalid_status" }));

		const signalUpdate = await InfoService.update({ _id: id }, { status });

		if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);
		await this.service.update({ en: id }, { enStatus: status });

		return res.status(STATUS_CODE.OK).json(signalUpdate);
	}

	removeInfo = async (req, res, next) => {
		const { id, lang } = req.params;
		const signalGetresult = await this.service.get({ _id: id });

		if (signalGetresult.error || !lang || !id || !LANGUAGES[lang] || !signalGetresult.data?.[lang])
			return res.sendStatus(400);

		const dataRemove = {};

		dataRemove[lang] = null;
		dataRemove[lang + "Name"] = null;
		dataRemove[lang + "Slug"] = null;

		const removeSignals = await Promise.all([
			InfoService.remove(signalGetresult.data[lang]),
			this.service.update({ _id: id }, dataRemove),
		]);

		const signalError = removeSignals.find((e) => e.error == true);
		if (signalError) {
			return res.status(500).json(signalError);
		}
		return res.status(STATUS_CODE.OK).json(
			RESPONSE_DATA({
				data: {
					result: removeSignals[0].data,
					Info: removeSignals[1].data,
				},
				message: MESSAGES.DELETE_SUCCEED,
			})
		);
	};

	remove = async (req, res, next) => {
		const { id } = req.params;

		const signalGetresult = await this.service.get({ _id: id });
		if (signalGetresult.error) res.status(STATUS_CODE.BAD_REQUEST).json(signalGetresult);

		const result = signalGetresult.data;

		const removeSignals = await Promise.all([
			this.service.remove(result._id),
			InfoService.remove(result.vi),
			InfoService.remove(result.en),
		]);

		const signalError = removeSignals.find((e) => e.error == true);
		if (signalError) return res.status(STATUS_CODE.SERVER_ERROR).json(signalError);

		const data = {
			result: removeSignals[0].data,
			InfoVi: removeSignals[1].data,
			InfoEn: removeSignals[2].data,
		};

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA({ data, message: MESSAGES.DELETE_SUCCEED }));
	};
}

module.exports = new resultCategoryController();

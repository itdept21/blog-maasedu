// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/company.services");
const { STATUS_CODE, MESSAGES, RESPONSE_DATA, } = require("../constants/base.constants");

class CompanyController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createOrUpdate = async (req, res, next) => {
		try {
			console.log("CompanyController	")
			let {
				page,
				url,
				name,
				legalName,
				additionalType = [],
				description,
				mainEntityOfPage,
				openTime,
				closeTime,
				currenciesAccepted,
				priceRange,
				datePublished,
				dateModified,
				// priceRangeCurrency,
				// priceRangeMinValue,
				// priceRangeMaxValue,

				hasMap,
				telephone,
				foundingDate,
				numberOfEmployees,
				taxID,

				logoUrl,
				logoWidth,
				logoHeight,

				imageUrl,
				imageWidth,
				imageHeight,

				locationName,
				locationStreetAddress,
				locationAddressLocality,
				locationAddressRegion,
				locationAddressCountry,
				locationPostalCode,

				geoLatitude,
				geoLongitude,

				foundingLocationName,
				foundingLocationUrl,
				foundingLocationHasMap,

				contactPointTelephone,
				contactPointContactType,
				contactPointAreaServed,
				contactPointEmail,
				contactPointUrl,

				knowsLanguage = [],
				sameAs = [],
				dayOfWeek = [],
				paymentAccepted = [],
				areaServed = [],
			} = req.body;
			if (!Array.isArray(additionalType)) additionalType = [additionalType];
			if (!Array.isArray(knowsLanguage)) knowsLanguage = [knowsLanguage];
			if (!Array.isArray(sameAs)) sameAs = [sameAs];
			if (!Array.isArray(dayOfWeek)) dayOfWeek = [dayOfWeek];
			if (!Array.isArray(paymentAccepted)) paymentAccepted = [paymentAccepted];
			if (!Array.isArray(areaServed)) areaServed = [areaServed];

			const logo = {
				url: logoUrl,
				width: logoWidth,
				height: logoHeight,
			};

			const image = {
				url: imageUrl,
				width: imageWidth,
				height: imageHeight,
			};

			// const priceRange = {
			// 	currency: priceRangeCurrency,
			// 	minValue: priceRangeMinValue,
			// 	maxValue: priceRangeMaxValue,
			// };

			const officeLocation = {
				name: locationName,
				streetAddress: locationStreetAddress,
				addressLocality: locationAddressLocality,
				addressRegion: locationAddressRegion,
				addressCountry: locationAddressCountry,
				postalCode: locationPostalCode,
			};

			const geo = {
				latitude: geoLatitude,
				longitude: geoLongitude,
			};

			const foundingLocation = {
				name: foundingLocationName,
				url: foundingLocationUrl,
				hasMap: foundingLocationHasMap,
			};

			const contactPoint = {
				telephone: contactPointTelephone,
				contactType: contactPointContactType,
				areaServed: contactPointAreaServed,
				email: contactPointEmail,
				url: contactPointUrl,
			};

			const dataUpdate = {
				page,
				url,
				name,
				legalName,
				description,
				mainEntityOfPage,
				openTime,
				closeTime,
				currenciesAccepted,
				priceRange,
				datePublished,
				dateModified,
				hasMap,
				telephone,
				foundingDate,
				numberOfEmployees,
				taxID,

				logo,

				image,

				officeLocation,

				geo,

				foundingLocation,

				contactPoint,

				additionalType: additionalType.filter((e) => (e ? true : false)),
				knowsLanguage: knowsLanguage.filter((e) => (e ? true : false)),
				sameAs: sameAs.filter((e) => (e ? true : false)),
				dayOfWeek: dayOfWeek.filter((e) => (e ? true : false)),
				paymentAccepted: paymentAccepted.filter((e) => (e ? true : false)),
				areaServed: areaServed.filter((e) => (e ? true : false)),
			};

			const signalUpdate = await this.service.createOrUpdate({ page: "info" }, {
				...dataUpdate,
			});
			if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);
			const result = signalUpdate.data;

			const responseJson = RESPONSE_DATA({ data: result, message: MESSAGES.CREATED_SUCCEED });
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			return res.status(400).json(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};
}

module.exports = new CompanyController();

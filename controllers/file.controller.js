// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/file.services");

class FileController extends BaseCrudController {
	constructor() {
		super(service);
	}

	create = async (req, res, next) => {
		if (req.uploadError) {
			return res.status(400).json({ error: true, message: "upload_error" });
		}
		return res.json({ error: false, message: "created", data: req.files });
	};

	renderGallery = async (req, res, next) => {
		const { skip = 0, limit = 20 } = req.query;
		const sort = { createdAt: -1 };

		// res.send('ok')
		const signalResponse = await service.getList({ skip, limit, sort });
		if (signalResponse.error) return res.send(500).json(signalResponse);

		return res.render("documents_library", { documents: signalResponse.data });
	};

}

module.exports = new FileController();

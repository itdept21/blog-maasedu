// @ts-nocheck
const BaseService = require("../services/base.services");
var mongoose = require('mongoose');
const { STATUS_CODE, UPDATE_NAME_SLUG_BY_LANG,
	LANGUAGES, LANGUAGES_REVERSE, LANGUAGES_REVERSE_STRING } = require("../constants/base.constants");
class BaseCrudController {
	constructor(service = new BaseService()) {
		this.service = service;
	}

	createID = async (req, res, next) => {

		const { ...params } = req.query;
		const signalResponse = await this.service.createID();

		const reponseData = {
			error: signalResponse.error,
			data: {
				...params,
				...signalResponse.data,
			},
			message: signalResponse.message,
		}
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(reponseData);
	};

	create = async (req, res, next) => {
		const data = req.body;
		const signalResponse = await this.service.create(data);
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	get = async (req, res, next) => {
		const { id } = req.params;
		const signalResponse = await this.service.get({ _id: id });
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	getList = async (req, res, next) => {
		const { skip = 0, limit = 10 } = req.query;
		const signalResponse = await this.service.getList({ skip, limit, sort: { createdAt: -1 } });
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	remove = async (req, res, next) => {
		const { id } = req.params;
		const signalResponse = await this.service.remove(id);
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	createOrUpdate = async (req, res, next) => {
		let { id = null } = req.params
		let matchingConditions = req.query;
		if (id) {
			matchingConditions['_id'] = id
		}
		const dataUpdate = req.body;
		const signalResponse = await this.service.createOrUpdate(matchingConditions, dataUpdate);
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	update = async (req, res, next) => {
		const matchingConditions = req.query;
		const dataUpdate = req.body;
		const signalResponse = await this.service.update(matchingConditions, dataUpdate);
		return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
	};

	/**
	 * WARNING: FOR MULTI-LANG MODEL ONLY !!! - Update the opposite language info whether it does not exists
	 * @param {string} lang string represent for the language
	 * @param {*} otherLangInfo info model
	 * @param {*} id  Multi-lang model ObjectID
	 * @param {*} infoService  info service class instance
	 * @returns new model with opposite language info
	 */
	updateOppositeLang = async ({ lang = LANGUAGES.vi, otherLangInfo = {}, id = '', multiLangField = '', infoService = new BaseService() }) => {
		// add -lang to name and slug of opposite info language
		otherLangInfo.name = otherLangInfo.name + LANGUAGES_REVERSE_STRING[lang];
		otherLangInfo.slug = otherLangInfo.slug + LANGUAGES_REVERSE_STRING[lang];
		otherLangInfo.seo_title = (otherLangInfo.seo_title || otherLangInfo.name) + LANGUAGES_REVERSE_STRING[lang];
		otherLangInfo.lang = LANGUAGES_REVERSE[lang];

		// update the opposite language version version

		const queryUpdate = {};
		queryUpdate[multiLangField] = id;
		queryUpdate.lang = LANGUAGES_REVERSE[lang];
		let { _id, ...dataUpdate } = otherLangInfo;
		dataUpdate.status = -1

		const signalUpdateInfo = await infoService.createOrUpdate(queryUpdate, dataUpdate);

		if (signalUpdateInfo.error) return signalUpdateInfo;
		const info = signalUpdateInfo.data;
		const name = signalUpdateInfo.data.name
		const slug = signalUpdateInfo.data.slug

		// update the opposite lang info to the root model
		const update = UPDATE_NAME_SLUG_BY_LANG({ name, slug, lang: LANGUAGES_REVERSE[lang] })
		update[LANGUAGES_REVERSE[lang]] = info._id

		const signalUpdate = await this.service.createOrUpdate({ _id: id }, update);
		return signalUpdate
	}
}

module.exports = BaseCrudController;

// @ts-nocheck
const COMPONENT_MODEL = require("../models/pageComponent.model");
const ARTICLE_MODEL = require("../models/article.model");
const POST_CATEGORY_MODEL = require("../models/postCategory.model");
const { FE_HOST } = require("../constants/host.constants");

require('dotenv').config()

// xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.google.com/schemas/sitemap-image/1.1 http://www.google.com/schemas/sitemap-image/1.1/sitemap-image.xsd"
const siteMapHeader =
    `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;

const siteMapFooter = "</urlset>";

function buildSiteMapItem(url = "", modifiedAt = new Date(), priority = 0.8, changeFrequencies = 'weekly') {
    return `<url>
	<loc>${url}</loc>
	<changefreq>${changeFrequencies}</changefreq>
	<priority>${priority == 1 ? '1.0' : priority}</priority>
	</url>`;
}

class SeoController {
    constructor() { }

    // @ts-ignore
    renderArticlesSiteMap = async (req, res, next) => {
        const articles = await ARTICLE_MODEL.find({
            status: { $gte: 1 },
            lang: 'en'
        })
            .populate([
                { path: 'category', }
            ])
            .sort({ modifiedAt: -1 })
            .select("slug modifiedAt post status")
            .limit(1000)
            .skip(0)
            .lean();
        console.log("articles", articles)
        let result = articles
            .map((e) => {
                const _slug = e.slug.replace(/\&/g, '%26')
                const _date = e.modifiedAt
                const _priority = 0.6;
                const _changeFrequencies = 'daily'

                return buildSiteMapItem(`${FE_HOST}/${e.category.enSlug}/${_slug}`, _date, _priority, _changeFrequencies);
            })
            .join("");

        return result;
    };

    renderPostCategorySiteMap = async (req, res, next) => {
        const postCategories = await POST_CATEGORY_MODEL.find({
            enStatus: { $gte: 1 },
            parent: { $ne: null }
        })
            .sort({ modifiedAt: -1 })
            .limit(1000)
            .skip(0)
            .lean();

        let result = postCategories
            .map((e) => {
                const _slug = e.enSlug.replace(/\&/g, '%26')
                const _date = e.modifiedAt
                const _priority = 0.7;
                const _changeFrequencies = 'daily'
                return buildSiteMapItem(`${FE_HOST}/${_slug}`, _date, _priority, _changeFrequencies);
            })
            .join("");

        return result;
    };

    renderPagesSitemap = async (req, res, next) => {
        let pages = [
            {
                name: 'home',
                slug: "",
                priorities: 1.0,
                change_frequency: 'daily'
            },
        ];

        let i = 0
        let dataPage = []
        for (i = 0; i < pages.length; i++) {
            const _pages = pages[i];
            const signal = await COMPONENT_MODEL.findOne({
                page: { $in: [_pages.name] },
                lang: 'en',
                parent: null,
            })
                .sort({ modifiedAt: 1 })
                .select('page createdAt modifiedAt')
                .lean();

            dataPage.push({
                page: signal.page,
                createdAt: signal.createdAt,
                modifiedAt: signal.modifiedAt,
            })
        }

        const resultPage = dataPage.map((e) => {
            const _pages = pages.find((_) => _.name == e.page) || {};
            const _date = new Date();
            const _slug = _pages.slug;
            const _priorities = _pages.priorities;
            const _change_frequency = _pages.change_frequency;

            return buildSiteMapItem(`${FE_HOST}/${_slug}`, _date, _priorities, _change_frequency);
        }).join("");

        const result = resultPage;
        return result;
    };

    // @ts-ignore
    renderMainSitemap = async (req, res, next) => {
        const mainPage = await this.renderPagesSitemap(req, res, next);
        const postCatePage = await this.renderPostCategorySiteMap(req, res, next);
        const blogPage = await this.renderArticlesSiteMap(req, res, next);

        const result = mainPage + postCatePage + blogPage;
        return res.set("Content-Type", "text/xml; charset=UTF-8").send(siteMapHeader + result + siteMapFooter);

    };
}

module.exports = new SeoController();

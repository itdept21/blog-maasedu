const AdminController = require("./admin.controller");
const GuardController = require("./guard.controller");
const ImageController = require("./image.controller");
const UserController = require("./user.controller");
const IndexController = require("./index.controller");
const DocController = require("./doc.controller");
const FileController = require("./file.controller");
const LayoutController = require("./layout.controller");
const PostCategoryController = require("./postCategory.controller");
const PostController = require("./post.controller");
const ArticleController = require("./article.controller");

module.exports = {
	AdminController,
	GuardController,
	ImageController,
	UserController,
	IndexController,
	DocController,
	FileController,
	LayoutController,
	PostController,
	ArticleController,
	PostCategoryController,
};

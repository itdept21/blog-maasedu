// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/postCategory.services");
const infoService = require("../../services/postCategoryInfo.services");
const { PAGINATION, STATUS_CODE, FORMAT_ID_FN, LIST_STATUS_POST_CATE_EN } = require("../../constants/base.constants");
const mongoose = require("mongoose");

class ViewPostCategoryController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderList = async (req, res, next) => {
        const { page = 1, search = "", ...filter } = req.query;
        const url = req._parsedOriginalUrl.pathname;
        const oldQuery = req._parsedOriginalUrl.search || "?";

        let status = null
        if (filter.status && filter.status != "") {
            status = parseInt(filter.status)
            const signalGetListStatus = await infoService.getList({
                matchConditions: { status },
            });
            const listArticleIds = signalGetListStatus.data.map(item => item._id);
            if (listArticleIds.length > 0) {
                filter['en'] = { $in: listArticleIds }
            } else {
                filter['en'] = { $in: [null] }
            }
            delete filter['status']
        } else {
            delete filter['status']
        };

        let parent = null
        if (filter.parent && filter.parent != "") {
            parent = filter.parent.toString();
            filter['parent'] = mongoose.Types.ObjectId(parent)
        } else {
            delete filter['parent']
        }

        if (page < 0) return res.sendStatus(400);
        const limit = 15;
        const skip = (page - 1) * limit;

        const queryConditions = search ? { $text: { $search: search } } : {};
        const signalGetList = await this.service.getList({
            skip, limit,
            matchConditions: { ...queryConditions, ...filter },
            populates: ['vi', 'en', 'parent'],
            sort: { createdAt: -1 },
        });

        const signalGetCount = await this.service.count({ ...queryConditions, ...filter });

        const total = signalGetCount.data;
        const pagination = PAGINATION(total, limit, url, oldQuery);

        if (signalGetList.error) next();
        const categories = signalGetList.data;

        // list status
        const listStatus = LIST_STATUS_POST_CATE_EN;

        const listCategories = await this.service.getListCategory();

        res.render("admin", {
            inc: "inc/admin/post_categories",
            title: "Danh sách danh mục bài viết",
            categories,
            listCategories: listCategories,
            pagination,
            page,
            total,
            search,
            listStatus, status,
            parent,
            FORMAT_ID_FN,
        });
    };

    renderEdit = async (req, res, next) => {
        try {
            const { id } = req.params;
            const lang = req.query.lang || "en";
            const switchLangs = {
                vi: `/admin/post-categories/${id}?lang=vi`,
                en: `/admin/post-categories/${id}?lang=en`,
            };

            const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail"]);
            const result = signalGet?.data || { _id: id }

            const signalGetInfo = await infoService.get({ postCategory: id, lang });
            const info = signalGetInfo?.data || {}

            const signalGetCategories = await this.service.getList(
                {
                    matchConditions: {
                        // parent: null,
                        enStatus: { $gte: 1 },
                    }
                }
            );
            if (signalGetCategories.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalGetCategories);
            const categories = signalGetCategories.data;
            // const _listParentCate = categories?.map((e) => e._id) || []

            // const signalGetListChildCate = await this.service.getList(
            //     {
            //         matchConditions: {
            //             enStatus: { $gte: 1 },
            //             parent: { $in: _listParentCate },
            //         }
            //     }
            // );

            // const _categories = categories.reduce((arr, item) => {
            //     const _tmp = arr.filter((e) => e._id == item._id);
            //     if (_tmp.length == 0)
            //         arr.push({
            //             _id: item._id,
            //             name: item.description + `. ` + item.title
            //         })

            //     return arr;
            // }, [])

            return res.render("admin", {
                inc: "inc/admin/post_category",
                title: "Chỉnh sửa danh mục",
                category: result,
                categoryInfo: info,
                lang,
                switchLangs,
                categories: categories,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR).json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewPostCategoryController();

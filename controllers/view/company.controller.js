// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/company.services");
const { PAGINATION, STATUS_CODE, FORMAT_ID_FN } = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");

class ViewCompanyController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderEdit = async (req, res, next) => {
        try {
            const { pageName } = req.params;
            // console.log("ViewCompanyController	", pageName)
            const lang = req.query.lang || "en";
            const switchLangs = {
                vi: `/admin/company?lang=vi`,
                en: `/admin/company?lang=en`,
            };

            const signalGet = await this.service.get({ page: pageName });
            const result = signalGet?.data || {}

            const _result = {
                ...result,
                logoUrl: result?.logo?.url || "",
                logoWidth: result?.logo?.width || "",
                logoHeight: result?.logo?.height || "",

                imageUrl: result?.image?.url || "",
                imageWidth: result?.image?.width || "",
                imageHeight: result?.image?.height || "",

                locationName: result?.officeLocation?.name || "",
                locationStreetAddress: result?.officeLocation?.streetAddress || "",
                locationAddressLocality: result?.officeLocation?.addressLocality || "",
                locationAddressRegion: result?.officeLocation?.addressRegion || "",
                locationAddressCountry: result?.officeLocation?.addressCountry || "",
                locationPostalCode: result?.officeLocation?.postalCode || "",

                geoLatitude: result?.geo?.latitude || "",
                geoLongitude: result?.geo?.longitude || "",

                foundingLocationName: result?.foundingLocation?.name || "",
                foundingLocationUrl: result?.foundingLocation?.url || "",
                foundingLocationHasMap: result?.foundingLocation?.hasMap || "",

                contactPointTelephone: result?.contactPoint?.telephone || "",
                contactPointContactType: result?.contactPoint?.contactType || "",
                contactPointAreaServed: result?.contactPoint?.areaServed || "",
                contactPointEmail: result?.contactPoint?.email || "",
                contactPointUrl: result?.contactPoint?.url || "",

                // priceRangeCurrency: result?.priceRange?.currency || "",
                // priceRangeMinValue: result?.priceRange?.minValue || "",
                // priceRangeMaxValue: result?.priceRange?.maxValue || "",
            }

            return res.render("admin", {
                inc: "inc/admin/company",
                title: "Chỉnh sửa danh mục",
                result: _result,
                lang,
                switchLangs,
                DATE_FORMAT_FN,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR).json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewCompanyController();

const { baseSchema } = require("./base.schema");
const { buildOriginalSchema } = require("./buildOriginalSchema");
const ReviewModel = require('../../models/pageComponentInfo.model');
const PageComponentModel = require('../../models/pageComponent.model');
const { BREADCRUMB } = require("../../constants/base.constants");
const FaqCateModel = require('../../models/faqCategory.model');
const FaqModel = require('../../models/faq.model');

async function homeSchemaStructure(data) {
    try {
        const listRating = await ReviewModel.find({
            parent: null,
            page: { $in: ["home"] },
            name: { $in: ['customer_feedbacks'] },
        }).sort({ createdAt: -1 }).limit(10);

        // tính trung bình ratingValue  
        const _totalRating = listRating.reduce((total, item) => {
            return total + parseFloat(item.ratingValue);
        }, 0);
        const _ratingValue = _totalRating / listRating.length;

        let _review = []
        if (listRating.length > 0) {
            _review = listRating
                .filter(fiRating => fiRating.content && fiRating.title && fiRating.ratingValue && fiRating.major)
                .map(rating => {
                    return {
                        '@type': 'Review',
                        reviewBody: rating.content,
                        author: { '@type': 'Person', name: rating.title, jobTitle: rating.major },
                        datePublished: rating.createdAt,
                        reviewRating: {
                            '@type': 'Rating',
                            bestRating: 5,
                            ratingValue: parseFloat(rating.ratingValue),
                        },
                        audience: rating.major
                    };
                });
        };

        ///////////// FAQs
        const signalGetFaqs = await PageComponentModel
            .findOne({
                page: { $in: ["home"] },
                name: { $in: ["faqs"] },
                lang: "en",
                parent: null,
            })
            .populate({
                path: "faq",
                select: "name description content slug createdAt updatedDay publishedAt category",
                populate: {
                    path: "category",
                    select: "name description content slug createdAt updatedDay publishedAt category",
                }
            })
            .select("name description content slug faq createdAt updatedDay publishedAt category")
            .lean();

        const signalGetFaqCateHome = await FaqCateModel.find({
            lang: "en",
            status: { $gte: 1 },
            onlyVisibleOnPage: BREADCRUMB.HOME
        });

        const signalGetFaqService = await FaqModel.find({
            category: { $in: signalGetFaqCateHome.map((e) => e._id) },
            lang: "en",
            status: { $gte: 1 },
        })
            .sort({ name: 1 })
            .limit(5)
            .select("name description content publishedAt slug")

        const _dataListFaqs = signalGetFaqService || [];

        const listFaq = signalGetFaqs?.faq.length > 0 ? signalGetFaqs?.faq : _dataListFaqs;

        const _mainEntityFaqs = listFaq?.map((e) => {
            return {
                "@type": "Question",
                "name": e.name,
                "acceptedAnswer": {
                    "@type": "Answer",
                    // "text": e.description,
                    "text": e.content,
                }
            }
        }) || [];

        const webPage = {
            '@type': ['WebPage', 'ItemPage'],
            '@id': data.url + '#webpage',
            url: data.url,
            name: data.name,
            isPartOf: {
                '@type': ['WebSite'],
                '@id': data.url + '/#website',
            },
            datePublished: data.datePublished || '2016-03-28T09:48:05+00:00',
            dateModified: data.dateModified || '2022-06-06T03:42:58+00:00',
            description: data.description || '',
            inLanguage: "en",
            potentialAction: [
                {
                    '@type': 'ReadAction',
                    target: [data.url]
                },
                {
                    '@type': 'SearchAction',
                    target: [data.url + '/search?search={search_term_string}'],
                    'query-input': {
                        valueRequired: true,
                        type: 'PropertyValueSpecification',
                        valueName: 'search_term_string'
                    }
                }
            ],
        };

        const faqs = {
            "@type": "FAQPage",
            "mainEntity": _mainEntityFaqs,
        };

        let rest = [webPage, buildOriginalSchema(data), faqs];

        // if (_review.length > 0) {
        //     const review = {
        //         "aggregateRating": {
        //             "@type": "AggregateRating",
        //             "bestRating": 5,
        //             "ratingValue": _ratingValue || 5,
        //             "reviewCount": listRating.length || 1,
        //         },
        //     };
        //     rest = [webPage, { ...buildOriginalSchema(data), ...review }, faqs];
        // }

        return baseSchema(rest);
    } catch (error) {
        console.log(error);
        return "";
    }
}


module.exports = { homeSchemaStructure }

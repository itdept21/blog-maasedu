function buildOriginalSchemaTypeOrganization(data) {
    return {
        "@type": "Organization",
        "@id": "kg:/g/11bzvbqpnh",
        "name": data.name,
        "legalName": data.legalName,
        "additionalType": data.additionalType,
        "description": data.description,
        "url": data.url,
        "mainEntityOfPage": data.mainEntityOfPage,
        "sameAs": data.sameAs,
        "logo": {
            "@type": "ImageObject",
            "@id": `${data.url}#logo`,
            "url": data.logo.url,
            "width": data.logo.width,
            "height": data.logo.height,
        },
        "image": {
            "@type": "ImageObject",
            "@id": `${data.url}#logo`,
            "url": data.image.url,
            "width": data.image.width,
            "height": data.image.height,
        },
        "location": {
            "@type": "PostalAddress",
            "@id": `${data.url}#address`,
            "name": data.officeLocation.name,
            "streetAddress": data.officeLocation.streetAddress,
            "addressLocality": data.officeLocation.addressLocality,
            "addressRegion": data.officeLocation.addressRegion,
            "addressCountry": data.officeLocation.addressCountry,
            "postalCode": data.officeLocation.postalCode,
        },
        "address": {
            "@id": `${data.url}#address`
        },

        "telephone": data.telephone,

        "foundingDate": data.foundingDate,
        "foundingLocation": {
            "@type": "place",
            "@id": "kg:/m/0hn4h",
            "name": data.foundingLocation.name,
            "URL": data.foundingLocation.url,
            "hasmap": data.foundingLocation.hasMap,
        },
        "knowsLanguage": data.knowsLanguage,
        "numberOfEmployees": data.numberOfEmployees,
        "taxID": data.taxID,
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": data.contactPoint.telephone,
            "contactType": data.contactPoint.contactType,
            "areaServed": data.contactPoint.areaServed,
            "availableLanguage": data.knowsLanguage,
        },
        {
            "@type": "ContactPoint",
            "email": data.contactPoint.email,
            "contactType": "customer service",
            "availableLanguage": data.knowsLanguage,
        },
        {
            "@type": "ContactPoint",
            "url": data.contactPoint.url,
            "contactType": "support",
            "availableLanguage": data.knowsLanguage,
        }]
    }
}

module.exports = { buildOriginalSchemaTypeOrganization }
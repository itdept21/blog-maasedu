const PageComponentModel = require('../../models/pageComponent.model');
const PageComponentInfoModel = require('../../models/pageComponentInfo.model');

const PostCategoryModel = require('../../models/postCategory.model');
const ArticleModel = require('../../models/article.model');

const { baseSchema } = require('./base.schema');

async function blogSearchSchemaStructure(data, slug = "") {
    try {
        const lang = 'en';
        //////////////// fold1
        const _section1 = await PageComponentModel
            .findOne({
                page: { $in: ["blog_search"] },
                name: { $in: ["section1"] },
                lang: lang,
                parent: null,
            })
            .lean();

        //////////////// fold1
        const _seoBlogHome = await PageComponentModel
            .findOne({
                page: { $in: ["blogSearch"] },
                name: { $in: ["seo"] },
                lang: lang,
                parent: null,
            })
            .populate([{
                path: 'images'
            }])
            .lean();

        const _itemListElement = [
            {
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": {
                    "@type": "ListItem",
                    id: `${data.url}`,
                    url: `${data.url}`,
                    name: "Home",
                }
            },
            {
                "@type": "ListItem",
                "position": 3,
                "name": "Blog Search",
                "item": {
                    "@type": "ListItem",
                    id: `${data.url}` + 'search',
                    url: `${data.url}` + 'search',
                    name: "Blog Search",
                }
            },

        ];
        const _author = []
        _author.push({
            "@type": "LocalBusiness",
            name: data?.name || "",
            url: data?.url || "",
            "priceRange": data.priceRange,
            "image": {
                "@type": "ImageObject",
                "@id": `${data.url}#logo`,
                "url": data.image.url,
                "width": data.image.width,
                "height": data.image.height,
            },
            "telephone": data.telephone,
            "address": {
                "@id": `${data.url}#address`
            },
            "location": {
                "@type": "PostalAddress",
                "@id": `${data.url}#address`,
                "name": data.officeLocation.name,
                "streetAddress": data.officeLocation.streetAddress,
                "addressLocality": data.officeLocation.addressLocality,
                "addressRegion": data.officeLocation.addressRegion,
                "addressCountry": data.officeLocation.addressCountry,
                "postalCode": data.officeLocation.postalCode,
            },
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": data.geo.latitude,
                "longitude": data.geo.longitude,
            }

        });

        let webPage = {
            "@context": "https://schema.org",
            "@type": "WebSite",
            "url": `${data.url}` + 'search',
            "potentialAction": [{
                "@type": "SearchAction",
                "target": {
                    "@type": "EntryPoint",
                    "urlTemplate": `${data.url}` + 'search' + "?keyword={search_term_string}",
                },
                "query-input": "required name=search_term_string"
            }, {
                "@type": "SearchAction",
                "target": {
                    "@type": "EntryPoint",
                    "urlTemplate": "android-app://com.example/" + `${data.url}` + 'search' + "?keyword={search_term_string}"
                },
                "query-input": "required name=search_term_string"
            }]
        };

        const breadcrumbSchema = {
            "@type": "BreadcrumbList",
            "itemListElement": _itemListElement,
        }

        const rest = [webPage, breadcrumbSchema];
        return baseSchema(rest);
        // return webPage;
    } catch (error) {
        console.log(error);
        return "";
    }
}


module.exports = { blogSearchSchemaStructure }

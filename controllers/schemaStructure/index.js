const { STRUCTURE_DATA_TYPE } = require('../../constants/structureData.constants');
const { blogSchemaStructure } = require('./blog');
const { blogSearchSchemaStructure } = require('./blogSearch');

class IndexSchemaStructure {
    static #instance;

    constructor() { }

    static get instance() {
        if (!this.#instance) {
            this.#instance = new this();
        }
        return this.#instance;
    }

    async generateStructureDate(data) {
        const { type, company, slug } = data;
        const structureData = this.checkType(type, company, slug);
        return structureData;
    }

    checkType(type, company, slug) {
        let _structureData = null;
        switch (type) {
            case STRUCTURE_DATA_TYPE.BLOG:
                _structureData = blogSchemaStructure(company, slug);
                return _structureData;

            case STRUCTURE_DATA_TYPE.SEARCH:
                _structureData = blogSearchSchemaStructure(company, slug);
                return _structureData;

            default:
                break;
        }
    }
}

module.exports = IndexSchemaStructure.instance;

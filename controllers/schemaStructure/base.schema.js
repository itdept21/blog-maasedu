function baseSchema(rest) {
  return JSON.stringify({
    '@context': 'https://schema.org',
    '@graph': rest
  })
  // return {
  //   '@context': 'https://schema.org',
  //   '@graph': rest
  // }
}

module.exports = { baseSchema }

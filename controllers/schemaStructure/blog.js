const PageComponentModel = require('../../models/pageComponent.model');
const PostCategoryModel = require('../../models/postCategory.model');
const ArticleModel = require('../../models/article.model');
const { baseSchema } = require('./base.schema');

async function blogSchemaStructure(data, slug = "") {
    try {
        // chỗ này chỉnh lại cho clean
        const _slug = slug?.split("/") || [slug]
        slug = _slug[_slug.length - 1]

        const lang = 'en';
        //////////////// fold1
        const _section1 = await PageComponentModel
            .findOne({
                page: { $in: ["blog_home"] },
                name: { $in: ["section1"] },
                lang: lang,
                parent: null,
            })
            .lean();

        //////////////// fold1
        const _seoBlogHome = await PageComponentModel
            .findOne({
                page: { $in: ["blogHome"] },
                name: { $in: ["seo"] },
                lang: lang,
                parent: null,
            })
            .populate([{
                path: 'images'
            }])
            .lean();

        let _name = _seoBlogHome?.titles?.[0] || _section1?.titles?.[0] || data.name || "";
        let _description = _seoBlogHome?.descriptions?.[0] || _section1?.descriptions?.[0] || data.description || "";
        let _articleBody = _description;
        let _datePublished = _seoBlogHome?.createdAt || _section1?.createdAt || new Date();
        let _dateModified = _seoBlogHome?.modifiedAt || _section1?.modifiedAt || new Date();
        let _image = _seoBlogHome?.images?.src || data.image.url || "";
        let _type = 'BlogPosting' // || 'NewsArticle';
        let _url = data.url;
        let _keywords = _seoBlogHome?.seo_keywords?.[0]?.split(",") || [_name];

        const _itemListElement = [
            {
                "@type": "ListItem",
                "position": 1,
                "name": 'Home',
                "item": {
                    "@type": "ListItem",
                    id: `https://blog.maasedu.com/`,
                    url: `https://blog.maasedu.com/`,
                    name: 'Home',
                }
            },
        ];
        if (slug || slug !== '') {
            // kiểm tra slug là PostCategoryModel hay ArticleModel
            const _postCate = await PostCategoryModel
                .findOne({
                    enSlug: slug,
                    enStatus: { $gte: 1 },
                })
                .populate([{
                    path: 'en'
                }])
                .lean();

            // là category
            if (_postCate !== null) {
                _name = _postCate?.en?.name || _name;
                _description = _postCate?.en?.description || _description;
                _datePublished = _postCate?.en?.createdAt || _datePublished;
                _dateModified = _postCate?.en?.modifiedAt || _dateModified;
                _image = _postCate?.images?.src || _image;
                _type = 'BlogPosting';

                let __itemListElement = {
                    "@type": "ListItem",
                    "position": 2,
                    "name": _postCate?.en?.name,
                    "item": {
                        "@type": "ListItem",
                        id: _url + _postCate?.en?.slug,
                        url: _url + _postCate?.en?.slug,
                        name: _postCate?.en?.name,
                    },
                }
                // push breadcrumb category
                _itemListElement.push(__itemListElement);
            }
            // là ArticleModel
            else {
                const _post = await ArticleModel
                    .findOne({
                        slug: slug,
                        status: { $gte: 1 },
                    })
                    .populate([
                        {
                            path: 'category'
                        },
                        {
                            path: 'author'
                        },
                        {
                            path: 'thumbnail'
                        }
                    ])
                    .lean();
                if (_post) {
                    _name = _post?.name || _name;
                    _description = _post?.description || _description;
                    _datePublished = _post?.createdAt || _datePublished;
                    _dateModified = _post?.modifiedAt || _dateModified;
                    _image = _post?.thumbnail?.src || _image;
                    _type = 'NewsArticle';
                    _articleBody = _description;

                    console.log("_post", _post?.category)
                    _itemListElement.push({
                        "@type": "ListItem",
                        "position": 2,
                        "name": _post?.category?.enName,
                        "item": {
                            "@type": "ListItem",
                            id: `https://blog.maasedu.com/` + _post?.category?.enSlug,
                            url: `https://blog.maasedu.com/` + _post?.category?.enSlug,
                            name: _post?.category?.enName,
                        },
                    });

                    const _postCate = await PostCategoryModel
                        .findOne({
                            _id: _post?.category?.parent || "",
                            parent: null,
                        })
                        .populate([{
                            path: 'en'
                        }])
                        .lean();

                    _itemListElement.push({
                        "@type": "ListItem",
                        "position": 3,
                        "name": _postCate?.en?.name,
                        "item": {
                            "@type": "ListItem",
                            id: `https://blog.maasedu.com/` + _post?.category?.enSlug + "/" + _postCate?.en?.slug,
                            url: `https://blog.maasedu.com/` + _post?.category?.enSlug + "/" + _postCate?.en?.slug,
                            name: _postCate?.en?.name,
                        },
                    })
                }
            }
        }

        let webPage = {
            "@context": "https://schema.org",
            "@type": _type,
            "headline": _name,
            "name": _name,
            "inLanguage": lang,
            "image": [
                _image
            ],
            "datePublished": _datePublished,
            "dateModified": _dateModified,
            // author: _author,
            keywords: _keywords,
        };
        if (_type == "NewsArticle") {
            webPage = {
                ...webPage,
                articleBody: _articleBody,
            }
        } else {
            webPage = {
                ...webPage,
                description: _description,
            }
        }

        const breadcrumbSchema = {
            "@type": "BreadcrumbList",
            "itemListElement": _itemListElement,
        }

        const rest = [webPage, breadcrumbSchema];
        return baseSchema(rest);
        // return rest;
    } catch (error) {
        console.log(error);
        return "";
    }
}


module.exports = { blogSchemaStructure }

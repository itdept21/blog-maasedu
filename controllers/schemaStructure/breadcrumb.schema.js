export const buildSchemaBreadcrumb = (breadcrumbs) => {
	return {
		'@type': 'BreadcrumbList',
		itemListElement: breadcrumbs.map((_breadcrumb, index) => {
			return {
				'@type': 'ListItem',
				position: index + 1,
				item: {
					'@type': 'ListItem',
					id: _breadcrumb.url,
					url: _breadcrumb.url,
					name: _breadcrumb.name,
				}
			}
		})
	}
}
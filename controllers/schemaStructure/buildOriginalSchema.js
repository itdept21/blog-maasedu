function buildOriginalSchema(data) {
    return {
        "@type": "LocalBusiness",
        "@id": "kg:/g/11bzvbqpnh",
        "name": data.name,
        "legalName": data.legalName,
        "additionalType": data.additionalType,
        "description": data.description,
        "url": data.url,
        "mainEntityOfPage": data.mainEntityOfPage,
        "sameAs": data.sameAs,
        "currenciesAccepted": data.currenciesAccepted,
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": data.dayOfWeek,
            "opens": data.openTime,
            "closes": data.closeTime,
        },
        "paymentAccepted": data.paymentAccepted,
        "priceRange": data.priceRange,
        // "priceRange": {
        //     "@type": "MonetaryAmount",
        //     "currency": "VND",
        //     "minValue": 3000,
        //     "maxValue": 1000000
        // },
        "logo": {
            "@type": "ImageObject",
            "@id": `${data.url}#logo`,
            "url": data.logo.url,
            "width": data.logo.width,
            "height": data.logo.height,
        },
        "image": {
            "@type": "ImageObject",
            "@id": `${data.url}#logo`,
            "url": data.image.url,
            "width": data.image.width,
            "height": data.image.height,
        },
        "location": {
            "@type": "PostalAddress",
            "@id": `${data.url}#address`,
            "name": data.officeLocation.name,
            "streetAddress": data.officeLocation.streetAddress,
            "addressLocality": data.officeLocation.addressLocality,
            "addressRegion": data.officeLocation.addressRegion,
            "addressCountry": data.officeLocation.addressCountry,
            "postalCode": data.officeLocation.postalCode,
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": data.geo.latitude,
            "longitude": data.geo.longitude,
        },
        "address": {
            "@id": `${data.url}#address`
        },

        "hasMap": [data.hasMap],
        "telephone": data.telephone,

        "foundingDate": data.foundingDate,
        "foundingLocation": {
            "@type": "place",
            "@id": "kg:/m/0hn4h",
            "name": data.foundingLocation.name,
            "URL": data.foundingLocation.url,
            "hasmap": data.foundingLocation.hasMap,
        },
        "knowsLanguage": data.knowsLanguage,
        "numberOfEmployees": data.numberOfEmployees,
        "taxID": data.taxID,
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": data.contactPoint.telephone,
            "contactType": data.contactPoint.contactType,
            "areaServed": data.contactPoint.areaServed,
            "availableLanguage": data.knowsLanguage,
        },
        {
            "@type": "ContactPoint",
            "email": data.contactPoint.email,
            "contactType": "customer service",
            "availableLanguage": data.knowsLanguage,
        },
        {
            "@type": "ContactPoint",
            "url": data.contactPoint.url,
            "contactType": "support",
            "availableLanguage": data.knowsLanguage,
        }]
    }
}

module.exports = { buildOriginalSchema }
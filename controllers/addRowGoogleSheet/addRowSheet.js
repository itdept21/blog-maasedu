const { GoogleSpreadsheet } = require("google-spreadsheet");
const { JWT } = require("google-auth-library");
const { private_key, client_email, sheet_url } = require("../../constants/jwtGoogleApiSpreadSheets.constants");

async function Spreadsheets(data) {
    const serviceAccountAuth = new JWT({
        email: client_email,
        key: private_key,
        scopes: ['https://www.googleapis.com/auth/spreadsheets'],
    });
    const doc = new GoogleSpreadsheet(
        `${sheet_url}`,
        serviceAccountAuth,
    );
    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[0];
    console.log("sheet", sheet)
    console.log("data", data)
    sheet.addRow(data);
};

class AddRowSheetGoogle {
    #_contactOrder;
    constructor({
        contactOrder
    }) {
        this.#_contactOrder = contactOrder;
    }

    PassableEssayAddRowSheetGoogle() {
        Spreadsheets(this.#_contactOrder)
        console.log("PassableEssaySend");
    }

}

module.exports = {
    AddRowSheetGoogle
}

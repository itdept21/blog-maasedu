// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/article.services");
const PostCategoryService = require("../services/postCategory.services");
const PageComponentService = require("../services/pageComponent.services")

const { STATUS_CODE, RESPONSE_DATA } = require('../constants/base.constants');

class ArticleController extends BaseCrudController {
	constructor() {
		super(service);
	}

	getInfo = async (req, res, next) => {
		const { slugArticle } = req.params;

		const signalGetArticle = await this.service.get({ slug: slugArticle, status: { $gte: 1 } }, ["thumbnail post category"]);
		if (!signalGetArticle.data) return res.status(400).json(signalGetArticle)
		let article = signalGetArticle.data;

		const otherLangSlug = article.lang == 'en' ? article.post?.viSlug : article.post?.enSlug

		// get list bài viết liên quan
		const signalGetRelatedArticle = await this.service.getList({
			matchConditions: { category: article.category._id, _id: { $nin: [article._id] }, status: { $gte: 1 }, lang: article.lang },
			skip: 0, limit: 5, populates: ['thumbnail']
		})
		if (signalGetRelatedArticle.error || !signalGetRelatedArticle.data) return res.status(400).json(signalGetRelatedArticle)
		let relatedArticle = signalGetRelatedArticle.data

		// info cate
		const dataCate = await PostCategoryService.get({ _id: article.category }, [article.lang])
		if (dataCate.error) return res.status(400).json(dataCate)
		const category = dataCate.data?.[article.lang]

		const seo = {
			seo_title: article?.seo_title || article.name,
			seo_descriptions: article?.seo_description || article.description,
			seo_image: article?.thumbnail?.src || '/images/noimage.jpg',
			seo_keyword: article?.seo_keywords || article.name,
			seo_lang: article?.seo_lang || 'vi',
		}

		delete article.post
		delete article.seo_title
		delete article.seo_description
		delete article.seo_keywords

		const code = signalGetArticle.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK;
		return res.status(code).json({ error: false, data: { originalUrl: req?.originalUrl, otherLangSlug, article, relatedArticle, category, seo, typePage: 'detailArticle' } });
	};

	updateArticleStatus = async (req, res, next) => {
		let { articleID, status } = req.params;
		// validate status
		status = parseInt(status);
		if ([-2, -1, 0, 1, 2].indexOf(parseInt(status)) < 0)
			return res.status(STATUS_CODE.BAD_REQUEST).json(RESPONSE_DATA({ error: true, message: "invalid_status" }));

		const signalUpdate = await service.update({ _id: articleID }, { status });

		if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);

		return res.status(STATUS_CODE.OK).json(signalUpdate);
	}
}

module.exports = new ArticleController();

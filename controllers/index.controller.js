// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require('../services/pageComponent.services');
const pageInfoService = require('../services/pageComponentInfo.services');
const articleService = require('../services/article.services');
const companyService = require('../services/company.services');
const postCategoryService = require('../services/postCategory.services');

const { STATUS_CODE, RESPONSE_DATA, formatData, formatDataSEO, MESSAGES } = require("../constants/base.constants");
const { POPULATE_ARTICLE_DETAIL, POPULATE_FAQ } = require("../constants/populate.constants");
const { createEdgeNGramsRegex } = require("../helpers/common")
const { CacheRoute } = require("../redis/redis.module");
const { time_cache } = require("../constants/cache_redis.constants");
const { STRUCTURE_DATA_TYPE } = require("../constants/structureData.constants");
const dayjs = require('dayjs');
var utc = require('dayjs/plugin/utc');
var timezone = require('dayjs/plugin/timezone');
const { createImageLazyLoading, createRelNoFollowHref, convertStringHtml, convertTitleBrandStory, convertStringHtmlH4 } = require("../helpers/formatContent");
dayjs.extend(utc);
dayjs.extend(timezone);
const moment = require('moment');
moment.locale('vi')
const IndexSchemaStructure = require('./schemaStructure/index');

class IndexController extends BaseCrudController {
	constructor() {
		super(service);
	}

	getDataBlogHomeEn = async (req, res, next) => {
		const lang = 'en'
		const pageServices = "blog_home";

		///////////// Hero banner
		const section1 = await service.getDataByPageName(
			pageServices,
			"section1",
			lang,
		);
		const _section1 = {
			_id: section1._id,
			title: section1.title,
			content: section1.content,
			description: section1.description,
		}

		///////////// Category list
		const section2 = await service.getDataByPageName(
			pageServices,
			"section2",
			lang,
			[
				{
					path: 'blogParent',
					populate: [
						{
							path: 'en',
						}
					]
				}
			]
		);

		const listPostCate = section2?.blogParent?.map((e) => e._id) || []
		const signalGetListChild = await postCategoryService.getList(
			{
				matchConditions: {
					enStatus: { $gte: 1 },
					parent: { $in: listPostCate },
				},
				populates: [
					{
						path: 'en',
					}
				],
				sort: { enName: 1 }
			}
		);
		const listChildCate = signalGetListChild?.data || []

		const _section2 = {
			_id: section2?._id || "",
			title: section2?.title || "",
			listCategory: section2?.blogParent?.map((e) => {
				const _child = listChildCate?.filter((_) => _.parent.toString() == e._id.toString()) || []
				return {
					_id: e?.en?._id || "",
					name: e?.en?.name || "",
					slug: e?.en?.slug || "",
					status: e?.en?.status || "",
					child: _child
						.map((__child) => {
							return {
								_id: __child?.en?._id || "",
								name: __child?.en?.name || "",
								slug: __child?.en?.slug || "",
								status: __child?.en?.status || "",
							}
						})
				}
			}) || []
		};

		///////////// Popular article
		const section3 = await service.getDataByPageName(
			pageServices,
			"section3",
			lang,
			[
				{
					path: "blog",
					populate: [
						{
							path: 'category',
							select: '_id enName enSlug enStatus'
						},
					],
					select: "_id lang slug status name description slug shared views postAt hashTags author category"
				},
			],
		);

		let popularBlogServices = section3?.blog?.map((e) => {

			return {
				...e,
				hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
			}
		}) || [];

		if (popularBlogServices.length == 0) {
			const signalGetListBlog = await articleService.getList({
				matchConditions: {
					status: { $gte: 1 },
					lang: lang
				},
				populates: [
					{
						path: 'category',
						select: '_id enName enSlug enStatus'
					},
				],
				sort: { views: 1, displayOrder: 1, modifiedAt: -1, createdAt: -1 },
				select: '_id lang slug postAt name description createdAt hashTags shared status views category',
				limit: 10,
			});
			popularBlogServices = signalGetListBlog?.data?.map((e) => {

				return {
					...e,
					hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
					category: {
						_id: e.category._id,
						slug: e.category.enSlug,
						name: e.category.enName,
						status: e.category.enStatus,
					}
				}
			}) || [];
		}

		const _section3 = {
			_id: section3._id,
			lang: section3.lang,
			title: section3.title,
			content: section3.content,
			description: section3.description,
			buttonName: section3.buttonName,
			buttonLink: section3.buttonLink,
			blogs: popularBlogServices,
		};

		///////////// Are you ready to place order at MAAS
		const section4 = await service.getDataByPageName(
			pageServices,
			"section4",
			lang,
			[]
		);
		const _section4 = {
			_id: section4._id,
			lang: section4.lang,
			title: section4.title,
			content: section4.content,
			description: section4.description,
			buttonName: section4.buttonName,
			buttonLink: section4.buttonLink,
		};

		///////////// SEO 
		const signalGetSEO = await this.service.get(
			{
				page: { $in: ["blogHome"] },
				name: { $in: ["seo"] },
				lang: lang,
				parent: null,
			},
			['images']
		);
		const seo = formatDataSEO(signalGetSEO.data);
		// create breadcrumb on page
		const breadcrumb = [
			{
				id: 1,
				name: "Home",
				url: `/`,
			},
		]

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				breadcrumb,
				section1: _section1,
				section2: _section2,
				section3: _section3,
				section4: _section4,
				seo: {
					...seo,
					status: 1,
				},
				typePage: 'blog_home',
				page: STRUCTURE_DATA_TYPE.BLOG,
			},
			message: MESSAGES.GET_SUCCEED
		}
		CacheRoute.setExV2(req, dataResult, { ex: time_cache })
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getDataMenuAndFooterEn = async (req, res, next) => {
		///////////// 
		const matchConditionsGetMenuWebsite = {
			parent: null,
			page: { $in: ["menu_footer_website_blog"] },
			name: { $in: ['menu_website'] },
		};
		const signalGetMenuWebsite = await pageInfoService.getList(
			{
				matchConditions: matchConditionsGetMenuWebsite,
				sort: { description: 1, modifiedAt: -1, createdAt: -1 },
				populates: [
					{
						path: "child",
						select: "title url child",
						populate: {
							path: "child",
							select: "title url child description",
						},
					},
				],
				select: "title url child description description"
			}
		);
		const menu = signalGetMenuWebsite.data

		///////////// 
		const matchConditionsGetFooterWebsite = {
			parent: null,
			page: { $in: ["menu_footer_website_blog"] },
			name: { $in: ['footer_website'] },
		};
		const signalGetFooterWebsite = await pageInfoService.getList(
			{
				matchConditions: matchConditionsGetFooterWebsite,
				sort: { description: 1, modifiedAt: -1, createdAt: -1 },
				populates: [
					{
						path: "child",
						select: "title url child",
						options: { sort: 'description' },
						populate: {
							path: "child",
							select: "title url child description",
							options: { sort: 'description' },
						},
					},
				],
				select: "title url child description"
			}
		);
		const footer = signalGetFooterWebsite.data

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				menu,
				footer,
				typePage: 'menu_footer',
			},
			message: MESSAGES.GET_SUCCEED
		}

		CacheRoute.setExV2(req, dataResult, { ex: time_cache })

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	}

	getArticleDetail = async (req, res, next) => {
		const { slugArticle, slugPostCategory } = req.params;

		const signalGetPostCategoryBySlug = await postCategoryService.get(
			{
				enSlug: slugPostCategory,
				enStatus: { $gte: 1 }
			},
			[
				{
					path: 'en',
				},
				{
					path: 'vi',
				},
			]
		);
		if (signalGetPostCategoryBySlug.error) return next()
		const infoCategory = signalGetPostCategoryBySlug?.data || null;
		if (!infoCategory) return next();

		const _section1 = {
			_id: infoCategory?.en?._id || "",
			slug: infoCategory?.en?.slug || "",
			title: infoCategory?.en?.name || "",
			description: infoCategory?.en?.content || "",
			content: infoCategory?.en?.description || "",
		};

		const signalGetArticle = await articleService.get(
			{
				slug: slugArticle,
				status:
				{
					$gte: 1
				}
			},
			[
				{
					path: "thumbnail",
					select: "_id name src alt title caption",
				},
				{
					path: "related",
					select: "_id name slug description postAt modifiedAt hashTags views shared",
				},
				{
					path: "category",
					populate: [
						{
							path: "thumbnail",
							select: "_id name src alt title caption",
						}
					]
				},
				{
					path: "author",
					populate: [
						{
							path: "avatar",
							select: "_id name src alt title caption",
						},
					],
					select: "_id avatar content title"
				},
			],
		);

		if (signalGetArticle.error) return next()
		let article = signalGetArticle?.data || null;
		if (!article) return next();

		let relatedBlogServices = article?.related?.map((e) => {
			return {
				...e,
				hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
				category: {
					_id: e.category._id,
					slug: e.category.enSlug,
					name: e.category.enName,
					status: e.category.enStatus,
				}
			}
		}) || [];

		if (relatedBlogServices.length == 0) {
			const signalGetListBlog = await articleService.getList({
				matchConditions: {
					status: { $gte: 1 },
					_id: { $nin: [article._id] },
					category: article?.category?._id || null,
				},
				populates: [
					{
						path: 'category',
						select: '_id enName enSlug enStatus'
					},
				],
				select: '_id lang name description slug status views shared hashTags thumbnail postAt modifiedAt category',
				sort: { views: 1, displayOrder: 1, modifiedAt: -1, createdAt: -1 },
				limit: 10,
			});

			relatedBlogServices = signalGetListBlog?.data?.map((e) => {
				return {
					...e,
					hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
					category: {
						_id: e.category._id,
						slug: e.category.enSlug,
						name: e.category.enName,
						status: e.category.enStatus,
					}
				}
			}) || [];
		}

		///////////// Popular article
		const section3 = await service.getDataByPageName(
			"blog_home",
			"section3",
			'en',
			[],
		);
		let relatedArticle = {
			title: "Related Blogs",
			content: "About MSc Assignment research insight, ideas, news and guidance",
			buttonName: section3?.buttonName || "",
			buttonLink: section3?.buttonLink || "",
			child: relatedBlogServices,
		};


		// get category parent
		const signalGetCategoryParentByParent = await postCategoryService.get(
			{
				_id: article?.category?.parent || "",
				parent: null,
			},
			[
				{
					path: 'en',
				},
			]
		);
		let categoryParent = signalGetCategoryParentByParent.data || {};

		if (signalGetCategoryParentByParent.data == null) {
			const signalGetCategoryParent = await postCategoryService.get(
				{
					_id: article?.category?.parent || null
				},
				[
					{
						path: 'en',
					},
					{
						path: 'parent',
						populate: [
							{
								path: "en",
							}
						]
					},
				]
			);
			categoryParent = signalGetCategoryParent?.data?.parent || {};
		}

		// get category child 
		const signalListChildCate = await postCategoryService.getList(
			{
				matchConditions: {
					enStatus: { $gte: 1 },
					parent: { $in: [categoryParent._id] },
				},
				populates: [
					{
						path: 'en',
					}
				],
				sort: { enName: 1 }
			}
		);
		const listIdChildCategory = signalListChildCate?.data?.map((e) => e._id) || [];

		// get category child 3nd level
		const signalListChild2Cate = await postCategoryService.getList(
			{
				matchConditions: {
					enStatus: { $gte: 1 },
					parent: { $in: listIdChildCategory },
				},
				populates: [
					{
						path: 'en',
					}
				],
				sort: { enName: 1 }
			}
		);

		const category = {
			_id: categoryParent?.en?._id || "",
			name: categoryParent?.en?.name || "",
			listCategory: signalListChildCate?.data?.map((__child) => {
				const _child2 = signalListChild2Cate?.data?.filter((_) => _.parent.toString() == __child._id.toString()) || []
				return {
					_id: __child?.en?._id || "",
					name: __child?.en?.name || "",
					description: __child?.en?.description || "",
					content: __child?.en?.content || "",
					slug: __child?.en?.slug || "",
					status: __child?.en?.status || "",
					child: _child2.map((__child2) => {
						return {
							_id: __child2?.en?._id || "",
							name: __child2?.en?.name || "",
							description: __child2?.en?.description || "",
							content: __child2?.en?.content || "",
							slug: __child2?.en?.slug || "",
							status: __child2?.en?.status || "",
						}
					})
				}
			}) || [],
		};

		///////////// Category list
		// lấy từ trang home
		const section2 = await service.getDataByPageName(
			"blog_home",
			"section2",
			'en',
			[
				{
					path: 'blogParent',
					populate: [
						{
							path: 'en',
						}
					]
				}
			]
		);
		const _otherCategories = section2?.blogParent?.map((e) => {
			return {
				_id: e?.en?._id || "",
				name: e?.en?.name || "",
				description: e?.en?.description || "",
				content: e?.en?.content || "",
				slug: e?.en?.slug || "",
				status: e?.en?.status || "",
			}
		}) || [];

		const author = {
			_id: article?.author?._id || "",
			avatar: article?.author?.avatar || {},
			introduce: article?.author?.content || "",
			name: article?.author?.title || "",
		}

		const _seo_keywords = article?.seo_keywords || '';
		const _focus_keyphrase = article.focus_keyphrase || '';

		const seo = {
			seo_title: article?.seo_title || article.name,
			seo_descriptions: article?.seo_description || article.description,
			seo_image: {
				src: article?.thumbnail?.src || '',
				name: article?.thumbnail?.name || '',
				alt: article?.thumbnail?.alt || '',
			},
			seo_keyword: _seo_keywords == '' ?
				_focus_keyphrase == '' ? article.name : _focus_keyphrase
				: _seo_keywords,
			seo_canonical: article?.seo_canonical,
			seo_lang: article?.seo_lang || 'vi',
		}

		delete article.post
		delete article.seo_title
		delete article.seo_lang
		delete article.seo_description
		delete article.seo_keywords
		delete article.seo_canonical
		delete article.__v
		delete article.related
		delete article.toc
		delete article.author

		// lazy loading img on content
		let _content = createImageLazyLoading(article.content);
		// external link rel nofollow
		_content = createRelNoFollowHref(_content);
		_content = _content.replace("<html><head></head><body>", "").replace("</body></html>", "");

		// fix content
		const _article = {
			...article,
			hashTags: article?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
			content: _content,
		}

		const breadcrumb = [
			{
				id: 1,
				name: "Home",
				url: `/`,
			},
			{
				id: 2,
				name: article?.category?.enName || "",
				url: `/${article?.category?.enSlug || ""}`,
			},
			{
				id: 3,
				name: article?.name || "",
				url: `/${article?.category?.enSlug || ""}` + `/${article?.slug || ""}`,
			},
		];

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				breadcrumb,
				section1: _section1,
				article: {
					..._article,
					category: {
						_id: _article.category._id,
						slug: _article.category.enSlug,
						name: _article.category.enName,
						status: _article.category.enStatus,
					}
				},
				author: author,
				relatedArticle: relatedArticle,
				category: category,
				otherCategories: _otherCategories,
				seo: {
					...seo,
					status: _article?.status || 1,
				},
				typePage: 'blog_detail',
				page: STRUCTURE_DATA_TYPE.BLOG,
			},
			message: MESSAGES.GET_SUCCEED
		}
		CacheRoute.setExV2(req, dataResult, { ex: time_cache })
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getSchemaStructure = async (req, res, next) => {
		const { slugSchema } = req.params;
		let slug = req.query?.query || req.query?.slug || null;
		console.log("getSchemaStructure ->", slugSchema, slug)

		const signalGetCompany = await companyService.get(
			{
				page: "info",
			},
		);

		if (signalGetCompany.error) return res.status(400).json(signalGetCompany)
		let company = signalGetCompany?.data || null;

		const payloadStructureData = {
			slug,
			company,
			type: slugSchema
		}
		const structureData = await IndexSchemaStructure.generateStructureDate(payloadStructureData);

		return res.status(200).json({
			error: false,
			structureData,
			message: MESSAGES.GET_SUCCEED
		});
	};


	getDataBlogSearchEn = async (req, res, next) => {
		const lang = 'en';
		let { page = 1, limit = 10, keyword = '', hashTags = '', author = '' } = req.query;
		let query = "";

		// hashTags, keyword, author
		const _limit = parseInt(limit) || 10;
		const _page = parseInt(page) || 1;
		const _skip = (_page - 1) * _limit;


		const signalGetSectionSearch = await this.service.get(
			{
				page: { $in: ["blog_search"] },
				name: { $in: ['section1'] },
				lang,
			}
		);
		const section1 = {
			_id: signalGetSectionSearch?.data?._id || "",
			lang: signalGetSectionSearch?.data?.lang || "",
			title: signalGetSectionSearch?.data?.titles?.[0] || "",
			content: signalGetSectionSearch?.data?.contents?.[0] || "",
			description: signalGetSectionSearch?.data?.descriptions?.[0] || "",
		};

		///////////// SEO 
		const signalGetSEO = await this.service.get(
			{
				page: { $in: ["blogSearch"] },
				name: { $in: ["seo"] },
				lang: lang,
				parent: null,
			},
			POPULATE_FAQ,
		);
		const seo = formatDataSEO(signalGetSEO.data);

		if (keyword == '' || hashTags == '' || author == '') {
			// create breadcrumb on page
			const _breadcrumb = [
				{
					id: 1,
					name: "Home",
					url: `/`,
				},
				{
					id: 2,
					name: "",
					url: `/blog-search`,
				},
			];

			const dataResult = {
				error: false,
				data: {
					originalUrl: req?.originalUrl,
					_breadcrumb,
					section1,
					article: {
						list: [],
						paging: {
							page: _page,
							limit: _limit,
							total: 0,
							pages: 1,
						},
						query,
					},
					seo: {
						...seo,
						status: 1,
					},
					typePage: 'blog_search',
					page: STRUCTURE_DATA_TYPE.SEARCH,
				},
				message: MESSAGES.GET_SUCCEED
			}
			return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
		}

		// let postConditions = keyword ? { $text: { $search: keyword } } : {};
		let postConditions = {};
		if (keyword !== 'undefined' && keyword !== '') {
			postConditions['$or'] = [];
			postConditions['$or'].push({ name: { $regex: createEdgeNGramsRegex(keyword), $options: "gi" } });
			postConditions['$or'].push({ description: { $regex: createEdgeNGramsRegex(keyword), $options: "gi" } });
			query = keyword;
		};

		if (hashTags !== 'undefined' && hashTags !== '') {
			postConditions['hashTags'] = { $regex: hashTags, $options: "gi" };
			// postConditions['hashTags'] = hashTags
			query = hashTags;
		};

		if (author !== 'undefined' && author !== '') {
			const signalGetAuthor = await pageInfoService.get(
				{
					page: { $in: ["blog"] },
					name: { $in: ['author'] },
					lang,
					title: author
				}
			);
			postConditions['author'] = signalGetAuthor?.data?._id || "";
			query = author;
		};

		// get list by condition
		const signalGetListPost = await articleService.getList({
			matchConditions: {
				...postConditions,
				lang,
				status: {
					$gte: 1
				}
			},
			sort: { postAt: -1 },
			limit: _limit,
			skip: _skip,
			populates: [
				{
					path: "author",
					populate: [
						{
							path: "avatar",
							select: "_id name src alt title caption",
						},
					],
					select: "_id avatar content title"
				},
				{
					path: 'category',
					select: '_id enName enSlug enStatus'
				},
			],
			select: '_id lang name description slug status views shared hashTags thumbnail postAt modifiedAt category',
		});
		if (signalGetListPost.error) {
			return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(
				{
					error: false,
					data: {
						originalUrl: req?.originalUrl,
						breadcrumb,
						section1,
						article: {
							list: [],
							paging: {
								page: _page,
								limit: _limit,
								total: 0,
								pages: 1,
							},
							query,
						},
						seo: {
							...seo,
							status: 1,
						},
						typePage: 'blog_search',
						page: STRUCTURE_DATA_TYPE.SEARCH,
					},
					message: MESSAGES.GET_SUCCEED
				}
			));
		};

		const listArticle = signalGetListPost.data || [];
		const _listArticle = listArticle?.map((e) => {
			return {
				...e,
				hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
				author: {
					_id: e?.author?._id || "",
					avatar: e?.author?.avatar || {},
					introduce: e?.author?.content || "",
					name: e?.author?.title || "",
				},
				category: {
					_id: e.category._id,
					slug: e.category.enSlug,
					name: e.category.enName,
					status: e.category.enStatus,
				}
			}
		}) || [];

		const signalGetCountPost = await articleService.count({ ...postConditions, lang, status: { $gte: 1 } });
		const totalArticle = signalGetCountPost.data;
		const pages = Math.ceil(totalArticle / _limit) || 1;

		// create breadcrumb on page
		const breadcrumb = [
			{
				id: 1,
				name: "Home",
				url: `/`,
			},
			{
				id: 2,
				name: query || "",
				url: `/blog-search`,
			},
		];


		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				breadcrumb,
				section1,
				article: {
					list: _listArticle,
					paging: {
						page: _page,
						limit: _limit,
						total: totalArticle,
						pages: pages,
					},
					query,
				},
				seo: {
					...seo,
					status: 1,
				},
				typePage: 'blog_search',
				page: STRUCTURE_DATA_TYPE.SEARCH,
			},
			message: MESSAGES.GET_SUCCEED
		}
		CacheRoute.setExV2(req, dataResult, { ex: time_cache })
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	}

	getDataBlogCategoryEn = async (req, res, next) => {
		const { slugPostCategory } = req.params;
		let { page = 1, limit = 10, ...filter } = req.query;
		const _limit = parseInt(limit) || 10;
		const _page = parseInt(page) || 1;
		const _skip = (_page - 1) * _limit;

		const lang = 'en'
		const pageServices = "blog_category";

		const signalGetPostCategoryBySlug = await postCategoryService.get(
			{
				enSlug: slugPostCategory,
				enStatus: { $gte: 1 }
			},
			[
				{
					path: 'en',
				},
			]
		);
		if (signalGetPostCategoryBySlug.error) return next()
		const infoCategory = signalGetPostCategoryBySlug?.data || null;
		if (!infoCategory) return next();
		let categoryParent = infoCategory


		// get category parent
		if (infoCategory.parent !== null) {
			const signalGetCategoryParent = await postCategoryService.get(
				{ _id: infoCategory.parent },
				[
					{
						path: 'en',
					},
					{
						path: 'parent',
						populate: [
							{
								path: "en",
							}
						]
					},
				]
			);
			if (signalGetCategoryParent.data.parent != null) {
				categoryParent = signalGetCategoryParent?.data?.parent || {};
			} else {
				categoryParent = signalGetCategoryParent.data || {};
			}
		};

		const _section1 = {
			_id: categoryParent?.en?._id || "",
			title: categoryParent?.en?.name || "",
			description: categoryParent?.en?.content || "",
			content: categoryParent?.en?.description || "",

		};

		// get category child 
		const signalListChildCate = await postCategoryService.getList(
			{
				matchConditions: {
					enStatus: { $gte: 1 },
					parent: { $in: [categoryParent._id] },
				},
				populates: [
					{
						path: 'en',
					}
				],
				sort: { enName: 1 }
			}
		);
		const listIdChildCategory = signalListChildCate?.data?.map((e) => e._id) || [];

		// get category child 3nd level
		const signalListChild2Cate = await postCategoryService.getList(
			{
				matchConditions: {
					enStatus: { $gte: 1 },
					parent: { $in: listIdChildCategory },
				},
				populates: [
					{
						path: 'en',
					}
				],
				sort: { enName: 1 }
			}
		);
		const listIdChild2Category = signalListChild2Cate?.data?.map((e) => e._id) || [];

		const category = {
			_id: infoCategory?.en?._id || "",
			name: categoryParent?.en?.name || "",
			listCategory: signalListChildCate?.data?.map((__child) => {
				const _child2 = signalListChild2Cate?.data?.filter((_) => _.parent.toString() == __child._id.toString()) || []
				return {
					_id: __child?.en?._id || "",
					name: __child?.en?.name || "",
					description: __child?.en?.description || "",
					slug: __child?.en?.slug || "",
					status: __child?.en?.status || "",
					child: _child2.map((__child2) => {
						return {
							_id: __child2?.en?._id || "",
							name: __child2?.en?.name || "",
							description: __child2?.en?.description || "",
							slug: __child2?.en?.slug || "",
							status: __child2?.en?.status || "",
						}
					})
				}
			}) || [],
		}


		const seo = {
			seo_title: infoCategory?.en?.seo_title || infoCategory.name,
			seo_descriptions: infoCategory?.en?.seo_description == "" ? infoCategory?.en?.description : infoCategory?.en?.seo_description,
			seo_image: infoCategory?.en?.thumbnail ? {
				src: infoCategory?.en?.thumbnail?.src || '',
				name: infoCategory?.en?.thumbnail?.name || '',
				alt: infoCategory?.en?.thumbnail?.alt || '',
			} : {},
			seo_keyword: infoCategory?.en?.seo_keywords || infoCategory?.en?.name,
			seo_canonical: infoCategory?.en?.seo_canonical,
			seo_lang: infoCategory?.en?.seo_lang || 'vi',
		};

		let _condition = [];
		if (infoCategory.parent !== null) {
			_condition = [
				infoCategory._id,
			]
		} else {
			_condition = [
				categoryParent._id,
				...listIdChildCategory,
				...listIdChild2Category
			]
		}
		const signalGetArticleBySlugCategory = await articleService.getList({
			matchConditions: {
				category: {
					$in: _condition
				},
				status: { $gte: 1 },
				lang: lang,
			},
			populates: [
				{
					path: 'category',
					select: '_id enName enSlug enStatus'
				},
			],
			select: '_id lang name description slug status views shared hashTags thumbnail postAt modifiedAt category',
			skip: _skip,
			limit: _limit,
		});
		const listArticle = signalGetArticleBySlugCategory?.data || [];
		const _listArticle = listArticle?.map((e) => {

			return {
				...e,
				hashTags: e?.hashTags?.[0]?.split(",").map(e => e.trim()) || [],
				category: {
					_id: e.category._id,
					slug: e.category.enSlug,
					name: e.category.enName,
					status: e.category.enStatus,
				}
			}
		}) || [];

		const signalGetTotalArticle = await articleService.count({
			category: {
				$in: _condition,
			},
			_id: { $nin: [infoCategory._id] },
			status: { $gte: 1 },
			lang: lang,
		});
		const totalArticle = parseInt(signalGetTotalArticle?.data || 0) || 0;
		const pages = Math.ceil(totalArticle / _limit);

		///////////// Hero banner
		const discount = await service.getDataByPageName(
			pageServices,
			"section1",
			lang,
		);
		const _discount = {
			_id: discount._id,
			title: discount.title,
			content: discount.content,
			description: discount.description,
			buttonName: discount.buttonName,
			buttonLink: discount.buttonLink,
		};

		///////////// Category list
		// lấy từ trang home
		const section2 = await service.getDataByPageName(
			"blog_home",
			"section2",
			lang,
			[
				{
					path: 'blogParent',
					populate: [
						{
							path: 'en',
						}
					]
				}
			]
		);
		const _otherCategories = section2?.blogParent?.map((e) => {
			return {
				_id: e?.en?._id || "",
				name: e?.en?.name || "",
				description: e?.en?.description || "",
				slug: e?.en?.slug || "",
				status: e?.en?.status || "",
			}
		}) || [];

		// create breadcrumb on page
		const breadcrumb = [
			{
				id: 1,
				name: "Home",
				url: `/`,
			},
			{
				id: 2,
				name: infoCategory?.en?.name || "",
				url: `/${infoCategory?.en?.slug || ""}`,
			},
		]

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				breadcrumb,
				section1: _section1,
				info: {
					_id: infoCategory?.en?._id || "",
					name: infoCategory?.en?.name || "",
					description: infoCategory?.en?.description || "",
					content: infoCategory?.en?.content || "",
					slug: infoCategory?.en?.slug || "",
					status: infoCategory?.en?.status || "",
				},
				category: category,
				otherCategories: _otherCategories,
				article: {
					list: _listArticle,
					paging: {
						page: _page,
						limit: _limit,
						total: totalArticle,
						pages: pages,
					},
				},
				discount: _discount,
				seo: {
					...seo,
					status: infoCategory?.en?.status || 1,
				},
				filter,
				typePage: 'blog_category',
				page: STRUCTURE_DATA_TYPE.BLOG,
			},
			message: MESSAGES.GET_SUCCEED
		}
		CacheRoute.setExV2(req, dataResult, { ex: time_cache })
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

}

module.exports = new IndexController();

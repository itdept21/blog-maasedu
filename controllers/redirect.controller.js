const service = require("../services/redirect.services");
const docService = require("../services/doc.services");
const BaseCrudController = require("./baseCrud.controller");
const { STATUS_CODE } = require("../constants/base.constants");
const { readFileFromExcel } = require("../helpers/importExRedirect");

class RedirectController extends BaseCrudController {
  constructor(service) {
    super(service);
  }

  createOrUpdate = async (req, res, next) => {
    let { id = null } = req.params
    let matchingConditions = req.query;
    if (id) {
      matchingConditions['_id'] = id
    }

    const dataUpdate = req.body;
    if (dataUpdate.oldUrl) dataUpdate.oldUrl = decodeURI(dataUpdate.oldUrl);
    if (dataUpdate.url && dataUpdate.url?.lastIndexOf('/')) dataUpdate.url.slice(0, dataUpdate.url.lastIndexOf('/'));
    if (dataUpdate.oldUrl && dataUpdate.oldUrl?.lastIndexOf('/')) dataUpdate.oldUrl.slice(0, dataUpdate.oldUrl.lastIndexOf('/'));

    // @ts-ignore
    const signalResponse = await this.service.createOrUpdate(matchingConditions, dataUpdate);
    return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResponse);
  };

  importExcel = async (req, res, next) => {
    try {
      console.log(" -------- importExcel RedirectController -------- ")
      const { docs = '' } = req.body
      // @ts-ignore
      const signalResponse = await docService.get({ _id: docs });
      const docSrc = signalResponse.data ? signalResponse.data.src : ""

      const dataExcel = await readFileFromExcel("public" + docSrc);
      if (dataExcel?.length == 0) {
        return res.status(STATUS_CODE.NOT_FOUND).json({
          error: true,
          data: {
            signalResponse
          },
          message: "File bị rỗng, vui lòng kiểm tra lại"
        });
      };

      console.log("dataExcel", dataExcel)

      for (let i = 0; i < dataExcel.length; i++) {
        const data = dataExcel[i];
        let { oldUrl, url, statusCode, } = data;
        if (oldUrl?.includes('https://maasedu.com')) oldUrl = oldUrl.replace('https://maasedu.com', '');
        else if (oldUrl?.includes('http://maasedu.com')) oldUrl = oldUrl.replace('http://maasedu.com', '');

        if (url?.includes('https://')) url = url.replace('https://', '');
        else if (url?.includes('http://')) url = url.replace('http://', '');

        if (!statusCode) {
          statusCode = 301
        }
        if (url.charAt(0) === '/') url = url.slice(1, url.length)

        const matchingConditions = { oldUrl }
        const dataUpdate = { oldUrl, url, statusCode }
        // @ts-ignore
        await this.service.createOrUpdate(matchingConditions, dataUpdate);
      }

      return res.status(STATUS_CODE.OK).json({
        error: false,
        data: {
          signalResponse
        }
      });
    }
    catch (error) {
      console.log("error importExcel RedirectController", error)
      return res.status(STATUS_CODE.BAD_REQUEST).json({
        error: true,
        message: error.message
      });
    }

  }

  getRedirect = async (req, res, next) => {
    const { slug } = req.query;
    if (slug == '/' || slug == '/.env' || slug == '/env' || slug.startsWith('/assets/')) {
      return res.status(200).json({ error: true });
    };

    if (slug.match(/\/admin|\.php|\.htm/)) {
      return res.status(200).json({
        error: false,
        data: {
          redirect: {
            oldUrl: slug,
            statusCode: 301,
            url: "/"
          },
          typePage: 'redirect'
        }
      });
    };

    const translateNoneSlug = slug + '/'
    const firstChar = '/' + slug
    const firstCharTranslateNoneSlug = '/' + translateNoneSlug

    // @ts-ignore
    const signalData = await this.service.get(
      {
        $or: [
          { oldUrl: slug },
          { oldUrl: translateNoneSlug },
          { oldUrl: firstChar },
          { oldUrl: firstCharTranslateNoneSlug },
        ]
      },
      [],
      "-_id lang oldUrl url statusCode"
    )

    const redirect = signalData?.data || null
    console.log("CMS ---------> redirect", redirect)

    if (!redirect) return res.status(200).json({ error: true })
    console.log("CMS ---------> redirect", slug, redirect?.url || "")

    if (redirect.oldUrl == redirect.url) {
      return res.status(200).json({ error: true });
    };

    return res.status(200).json({
      error: false,
      data: {
        // redirect,
        redirect: {
          oldUrl: redirect.oldUrl,
          statusCode: redirect.statusCode,
          url: redirect.url == "" ? "/" : redirect.url,
        },
        typePage: 'redirect'
      }
    });
  };

}

//getRedirect
module.exports = new RedirectController(service);

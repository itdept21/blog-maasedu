const BaseService = require('./base.services');
const model = require('../models/image.model');

class ImageService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new ImageService();
const model = require("../models/redirect.model");
const BaseService = require("./base.services");

class redirectService extends BaseService {
	constructor() {
		super(model);
	}
}

module.exports = new redirectService();

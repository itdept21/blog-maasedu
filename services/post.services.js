const model = require("../models/post.model");
const BaseService = require("./base.services");
const ArticleService = require("./article.services");

const { RESPONSE_DATA, MESSAGES, LANGUAGES } = require("../constants/base.constants");

class PostService extends BaseService {
	constructor() {
		super(model);
	}

	createEmptyPost = async () => {
		try {
			const signalCreatePost = await this.create();
			if (signalCreatePost.error) return signalCreatePost;

			return RESPONSE_DATA({ data: signalCreatePost.data, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};
}

module.exports = new PostService();

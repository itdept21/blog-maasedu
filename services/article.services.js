// @ts-nocheck
const model = require("../models/article.model");
const BaseService = require("./base.services");

class ArticleService extends BaseService {
	constructor() {
		super(model);
	}

	dashboard = async () => {
		const signalCountArticle = await this.count({
			lang: 'en'
		});
		const countArticle = signalCountArticle.data || 0;

		const signalCountPublicIndex = await this.count({
			status: 1
		});
		const countPublicIndex = signalCountPublicIndex.data || 0;

		const signalCountPublicNoIndex = await this.count({
			status: 2
		});
		const countPublicNoIndex = signalCountPublicNoIndex.data || 0;

		return {
			publicIndex: {
				count: countPublicIndex,
				text: `public, index`
			},
			publicNoIndex: {
				count: countPublicNoIndex,
				text: `public, no index`
			},
		}
	}

}

module.exports = new ArticleService();

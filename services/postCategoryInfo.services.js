const model = require("../models/postCategoryInfo.model");
const BaseService = require("./base.services");
// const ArticleService = require("./article.services");

const { RESPONSE_DATA, MESSAGES, LANGUAGES } = require("../constants/base.constants");

class PostCategoryService extends BaseService {
	constructor() {
		super(model);
	}
}

module.exports = new PostCategoryService();

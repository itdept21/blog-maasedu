const BaseService = require('./base.services');
const model = require('../models/pageComponentInfo.model');

class PageComponentInfoService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new PageComponentInfoService();
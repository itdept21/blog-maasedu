const BaseService = require('./base.services');
const model = require('../models/company.model');

class CompanyService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new CompanyService();
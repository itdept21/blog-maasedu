// @ts-nocheck
const model = require("../models/file.model");
const BaseService = require("./base.services");
const fs = require("fs");

const { RESPONSE_DATA, MESSAGES, STATUS_CODE } = require("../constants/base.constants");

class FileService extends BaseService {
	constructor() {
		super(model);
	}

	/**
	 * @override
	 * @param {*} req
	 * @param {*} res
	 * @param {*} next
	 * @returns
	 */
	create = async (req, res, next) => {
		if (req.uploadError) {
			return res.status(400).json({ error: true, message: "upload_error" });
		}
		return res.json({ error: false, message: "created", data: req.files });
	};

	remove = async (id) => {
		const removeResult = await this.model.findOneAndDelete({ _id: id }).lean();
		const removeFileResult = await fs.unlinkSync("./public" + removeResult.src);
		console.log(removeFileResult);
		return RESPONSE_DATA({ data: removeResult, message: MESSAGES.DELETE_SUCCEED });
	};
}

module.exports = new FileService();

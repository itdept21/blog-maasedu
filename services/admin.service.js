const MODEL = require('../models/admin.model');
const { comparePassword, signingToken, parseToken, hashPassword } = require('../helpers/authentication.helper')
const STATUS_ACTIVE = 1;

class AuthService {

    /** LOGIN TO CURRENT ACCOUNT
     * @param {String} username     least 6 characters for raw username
     * @param {String} password     least 6 characters for raw password
     * @returns {SignalResponse} {error: Boolean, data?: {infoAdmin: adminModel, token: String}, message: String}
     */
    static async login({ username = '', password = '' }) {
        try {
            
            if (username.length < 5) return { error: true, message: 'invalid_username', data: null };
            if (password.length < 5) return { error: true, message: 'invalid_password', data: null };

            // find a same admin is active
            const exists = await MODEL
                .findOne({ username, status: STATUS_ACTIVE })
                .select({ createAt: 0, modifyAt: 0 })
                .lean()

            if (!exists) return { error: true, message: 'not_found_admin', data: null };

            // clear the password before return
            const { password: adminPassword, ...infoAdmin } = exists;

            const samePassword = await comparePassword({ raw: password, encrypted: adminPassword })

            if (!samePassword) return { error: true, message: 'wrong_password', data: null };

            // generate token
            const token = signingToken({ adminID: infoAdmin._id });

            return { error: false, data: { token, infoAdmin }, message: 'login_succeed' };
        } catch (error) {
            return { error: true, message: error.message, data: null }
        }
    }

    /** REGISTER NEW ACCOUNT
     * @param {String} password    Least 6 characters for raw password
     * @param {String} fullname    Least 3 characters for user's fullname
     * @param {String} username    Least 6 characters for user's username
     * @param {Number} role         0 editor - 1 admin - 2 root admin
     * @returns {SignalResponse}   {error: Boolean, data?: {infoAdmin: adminModel, token: String}, message: String}
     */
    static async create({ pass = '', fullname = '', username = '', role = 0 }) {
        try {
            if (username.length < 6) return { error: true, message: 'invalid_username', data: null };
            if (pass.length < 6) return { error: true, message: 'invalid_password', data: null };

            // find a same user has same phone and is active
            const exists = await MODEL
                .findOne({ username, status: STATUS_ACTIVE })
                .select({ createAt: 0, modifyAt: 0 })
                .lean()

            if (exists) return { error: true, message: 'admin_exists', data: null };

            const hashedPassword = await hashPassword(pass);

            // insert new user
            const doc = new MODEL({ password: hashedPassword, fullname, username, role })
            await doc.save();

            // clear the password before return
            const { password, ...infoAdmin } = doc._doc || doc; // prevent low performance mongoose saving


            // generate token
            const token = signingToken({ adminID: infoAdmin._id });

            return { error: false, data: { token, infoAdmin }, message: 'admin_registered_new' };
        } catch (error) {
            return { error: true, message: error.message, data: null }
        }
    }

    /** REFRESH NON EXPIRED TOKEN
     * @param {String} token      encrypted jwt string that we had sent to frontend before
     * @returns {SignalResponse} {error: Boolean, data: JWT_Token_String, message: String}
    */
    static async refreshToken(token) {
        try {
            const decodedToken = parseToken(token)
            if (!decodedToken) return { error: true, message: 'token_expired', data: null }

            const tokenRefreshed = signingToken({ adminID: decodedToken._id });
            return { error: false, data: tokenRefreshed, message: 'token_refreshed' };
        } catch (error) {
            return { error: true, message: error.message, data: null }
        }
    }
}

module.exports = AuthService
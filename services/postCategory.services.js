const model = require("../models/postCategory.model");
const BaseService = require("./base.services");

class PostCategoryService extends BaseService {
	constructor() {
		super(model);
	}

	getListCategory = async () => {
		const signalGetListCategoryParent = await this.getList({
			matchConditions: {
				parent: null,
			},
			sort: { createdAt: -1 },
		});
		if (signalGetListCategoryParent.error)
			return [];

		const categories = signalGetListCategoryParent.data;
		const _listCateParent = categories.map((e) => e._id);

		const signalGetListCategoryChild = await this.getList({
			matchConditions: {
				parent: { $in: _listCateParent },
			},
			sort: { createdAt: -1 },
			populates: [
				{
					path: "parent",
					populate: {
						path: "parent",
					},
				},
			],
		});
		const childCategory = signalGetListCategoryChild.data

		const _categories = categories.reduce((arr, item) => {
			const _tmp = arr.filter((e) => e._id == item._id);
			if (_tmp.length == 0)
				arr.push({
					_id: item._id,
					enName: item.enName,
					viName: item?.viName || item.enName,
				})

			let arr1 = [];
			const _child = childCategory.filter((_) => _.parent._id.toString() == item._id.toString());
			if (_child && _child.length > 0) {
				arr1 = _child?.reduce((arr1, item1) => {
					const _tmp1 = arr1.filter((e) => e._id == item1._id);
					if (_tmp1.length == 0)
						arr.push({
							_id: item1._id,
							enName: item1.enName,
							viName: item1?.viName || item1.enName,
						})

					let arr2 = [];
					if (item1.parent && item1.parent.length > 0) {
						arr2 = item1?.parent?.reduce((arr2, item2) => {
							const _tmp2 = arr2.filter((e) => e._id == item2._id);
							if (_tmp2.length == 0)
								arr2.push({
									_id: item2._id,
									enName: item2.enName,
									viName: item2?.viName || item2.enName,
								})

							return arr2;
						}, [])
					};

					arr1 = [...arr1, ...arr2]
					return arr1;
				}, [])
			};

			arr = [...arr, ...arr1]
			return arr;
		}, [])

		return _categories;
	}
}

module.exports = new PostCategoryService();

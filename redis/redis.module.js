const redis = require('redis');

class SingletonRedis {
    static getClient() {
        if (!this.client) this.client = redis.createClient({
            socket: {
                host: '127.0.0.1',
                port: '6379'
            }
        });
        return this.client;
    }
}

class CacheRoute {

    static getKeyCache(req) {
        const prefix = '__express__blog__maasedu__';
        const _query = req.query;
        const key = req.url.split("?")[0];
        const query = Object.keys(_query).sort().reduce(
            (obj, key) => {
                obj[key] = _query[key];
                return obj;
            },
            {}
        );
        const __key = prefix + key + "__" + JSON.stringify(query);
        return __key;
    };

    static async checkDataInCache(req, res, next) {
        const method = req.method;
        if (method !== "GET") return next()

        const _myClient = SingletonRedis.getClient();
        const key = CacheRoute.getKeyCache(req);

        const jsonValue = await _myClient.get(key);
        if (jsonValue) {
            return res.status(200).json(JSON.parse(jsonValue));
        }
        next();

    };

    static async checkDataInCacheUserProfile(req, res, next) {
        const _myClient = SingletonRedis.getClient();
        const bearerToken = req.headers.authorization || req.headers.Authorization
        const _key = bearerToken.split(" ")?.[1] || null;
        const jsonValue = await _myClient.get(_key);
        if (jsonValue) {
            return res.status(200).json(JSON.parse(jsonValue));
        }
        next();

    };

    static getKeyCacheV2(req) {
        const prefix = '__express__blog__maasedu__';
        const _query = req.query;
        const key = req.url.split("?")[0];
        const query = Object.keys(_query).sort().reduce(
            (obj, key) => {
                obj[key] = _query[key];
                return obj;
            },
            {}
        );
        const __key = prefix + key + "__" + JSON.stringify(query);
        return __key;
    }

    static async setEx(key, data, { ex = 20 }) {
        const _myClient = SingletonRedis.getClient();
        // const key = CacheRoute.getKeyCache(req);
        _myClient.setEx(key, ex, JSON.stringify(data));
    }

    static async setExV2(req, data, { ex = 20 }) {
        const _myClient = SingletonRedis.getClient();
        const key = CacheRoute.getKeyCache(req);
        _myClient.setEx(key, ex, JSON.stringify(data));
    }

    static async deleteEx(key) {
        const _myClient = SingletonRedis.getClient();
        _myClient.del(key);
    }


}

module.exports = {
    SingletonRedis,
    CacheRoute
};
require("dotenv").config();
const mongoose = require("mongoose");
const MODEL = require("../models/image.model");
mongoose.Promise = global.Promise;

const { SEO_ALL_IMAGES_IN_DIRECTORY } = require("./toolImage");

const {
    IMAGES_STORAGE_LARGE = "./public/uploads/images/large_images",
    IMAGES_STORAGE_MEDIUM = "./public/uploads/images/medium_images",
    IMAGES_STORAGE_SMALL = "./public/uploads/images/small_images",
} = process.env;

// Connect MongoDB at default port 27017.
mongoose.connect(
    "mongodb://localhost:27017/blog",
    {
        useNewUrlParser: true,
        useCreateIndex: true,
    },
    async (err) => {
        if (!err) {
            const documents = await MODEL.find({}).sort({ createdAt: -1 }).lean();
            for (let i = 0; i < documents.length; i++) {
                await Promise.all([
                    SEO_ALL_IMAGES_IN_DIRECTORY("./public" + documents[i].src, IMAGES_STORAGE_LARGE, "desktop", 90),
                    SEO_ALL_IMAGES_IN_DIRECTORY("./public" + documents[i].src, IMAGES_STORAGE_MEDIUM, "tablet", 86),
                    SEO_ALL_IMAGES_IN_DIRECTORY("./public" + documents[i].src, IMAGES_STORAGE_SMALL, "mobile", 70),
                ]);
            }
            await mongoose.connection.close();
        } else {
            console.log("Error in DB connection: " + err);
        }
    }
);

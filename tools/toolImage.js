// @ts-nocheck
const sharp = require('sharp');
const dotenv = require('dotenv');
const fs = require('fs')
dotenv.config();


class IMAGE {
    constructor(fileDir) {
        this.file = sharp(fileDir)
        //this.ToJPEG = () => { this.file.jpeg(); return this };
        this.ResizeForDesktop = (width = 700, height = 500) => { this.file.resize(width); return this; }
        this.ResizeForMobile = (width = 300, height = 300) => { this.file.resize(width); return this; }
        this.writeFile = (fileWriteDir) => { this.file.toFile(fileWriteDir, writeFileErrorFunction); return this; }
        this.SEOFor = async (device = 'desktop', quality = 90, outDir) => {
            device = `${device}`.toLowerCase();
            const RegexRemoveExtension = /(\.\w+)$/i;
            const RegexRemoveFileOriginalPath = /((.*\\)|(.*\/))/gi
            const imageWidth = parseInt(process.env[`${device.toUpperCase()}_IMAGE_WIDTH`]) || 500;
            //let newFileName = (fileDir.replace(RegexRemoveExtension, '') + `.jpg` || '.jpg').replace(RegexRemoveFileOriginalPath, '');
            let newFileName = fileDir.replace(RegexRemoveFileOriginalPath, '');

            newFileName = ((outDir) ? outDir + '/' : '') + newFileName;

            const info = await this.file.metadata().then(res => res)
            try {

                await this.file.resize({
                    width: parseInt(info.width >= imageWidth ? imageWidth : info.width),
                    /* height: 100, */
                    /* position: 'left top', */
                }).toFile(`${newFileName}`, (err) => {
                    if (err) console.error(' xxx CANNOT CREATE FILE : ' + newFileName + ' ! xxx\n', err);
                });

                if (info.width >= imageWidth)
                    console.warn(`xxxxxxx Image ${fileDir.replace(RegexRemoveFileOriginalPath)} is too small ! xxxxxx`)

            } catch (err) {
                console.log(err)
            }

            return this;
        }

        this.SEO = (quality, outDir) => {
            let promiseArr = ['mobile', 'tablet', 'desktop'].map(e => this.SEOFor(e, quality, outDir));
            Promise.all(promiseArr);
        }

        let writeFileErrorFunction = (err) => {
            if (err) console.warn(' ===== THERE WAS AN ERROR WHILE TRYING TO CREATE FILE =====');
        }
    }

}

SEO_ALL_IMAGES_IN_DIRECTORY = (Dirname, outDir = './SEO IMAGES', options = 'desktop', quality = 90) => {
    if (!fs.existsSync(outDir)) {
        fs.mkdirSync(outDir)
    }
    let fileDir = Dirname

    if (fileDir.match(/(\.(png|jpg|webp|svg))$/i)) {
        let image = new IMAGE(fileDir)
        options = options.toLowerCase()
        if (options !== 'all')
            image.SEOFor(options, quality, outDir);
        else
            image.SEO(quality, outDir)
    }

}

module.exports = { SEO_ALL_IMAGES_IN_DIRECTORY }

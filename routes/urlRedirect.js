const router = require("express").Router();
const controller = require("../controllers/redirect.controller");

router.get('/redirect', controller.getRedirect);

module.exports = router;
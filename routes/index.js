var express = require("express");
var router = express.Router();
const { IndexController } = require("../controllers");
/* GET home page. */

router.use((req, res, next) => {
	console.log(req?.originalUrl);

	if (req?.originalUrl.match(/\/admin|\.php|\.html/)) {
		return res.status(404).json({ error: true, message: 'notfound', data: null })
	}
	return next();
})

router.use(require("./urlRedirect"));
router.use(require("./seo"));
// ==================================== API NEW =======================================

// home
router.get("/", IndexController.getDataBlogHomeEn);
router.get("/menu-footer", IndexController.getDataMenuAndFooterEn);

router.get("/blog-search", IndexController.getDataBlogSearchEn);

router.get("/:slugPostCategory", IndexController.getDataBlogCategoryEn);

router.get("/:slugPostCategory/:slugArticle", IndexController.getArticleDetail);

// ==================================== API SCHEMA STRUCTURE =======================================
router.get("/schema-structure/:slugSchema", IndexController.getSchemaStructure);

module.exports = router;

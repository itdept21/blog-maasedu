const router = require("express").Router();
const SeoController = require("../controllers/seo.controller");

router.get("/sitemap.xml", SeoController.renderMainSitemap)

module.exports = router;
var express = require("express");
var router = express.Router();

const controller = require("../controllers/postCategory.controller");

const { parseFormData } = require("../helpers");
const { GuardController } = require("../controllers");
const viewController = require("../controllers/view/postCategory.controller.js");


// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/post-categories/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/post-categories", GuardController.needLogin, viewController.renderList);


// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/post-categories", GuardController.needLogin, parseFormData, controller.createID);
router.put('/post-categories/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);

router.put('/post-categories/:id/status/:status', GuardController.needLogin, parseFormData, controller.updatePostCategoryStatus);

// // -------------------- DELETE ----------------------------
router.delete("/post-categories/:id", GuardController.needLogin, controller.remove);

module.exports = router;


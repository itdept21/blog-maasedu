// @ts-nocheck
const nodemailer = require('nodemailer');
const cf_mailer = require('../config/cf_mailer');

module.exports = function (to, subject, content) {
    // var transporter = nodemailer.createTransport({
    //     host: cf_mailer.host,
    //     secureConnection: false,
    //     tls: {
    //         ciphers: 'SSLv3'
    //     },
    //     auth: {
    //         user: cf_mailer.email,
    //         pass: cf_mailer.pass,
    //     }
    // });
    var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: cf_mailer.email,
            pass: cf_mailer.pass,
        }
    })

    var mailOptions = {
        from: `Submission Order  <${cf_mailer.email}>`,
        to,
        subject,
        html: content.html,
        text: content.text,
        attachments: content.attachments
    };
    console.log("mailOptions", mailOptions)

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}


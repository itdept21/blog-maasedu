const cheerio = require("cheerio");
const jsdom = require("jsdom");
const { FE_HOST } = require("../constants/host.constants");
const { JSDOM } = jsdom;

function test() {
    // HTML mẫu
    const html = '<div class="parent"><span>Hello, World!</span></div>';

    // Load HTML bằng Cheerio
    const $ = cheerio.load(html);

    // Lấy phần tử con
    const childElement = $('span');

    // Xóa phần tử con từ tag parent cũ
    childElement.unwrap();

    // Thay đổi tag parent của phần tử con thành div
    childElement.wrap(`<div class="wrapImage">`);

    // In ra HTML mới
    console.log($.html());
    return $.html();
}

function createImageLazyLoadingV2(content_article) {
    const IMAGE = ["img"];
    const $ = cheerio.load(content_article);
    let imgs = $(IMAGE.join(","));
    console.log("header", imgs);
    imgs.each(function (i, elem) {
        $(this).attr("loading", "lazy");
    });

    // Lấy phần tử con
    const childElement = $('img');
    // Xóa phần tử con từ tag parent cũ
    childElement.unwrap();
    // Thay đổi tag parent của phần tử con thành div
    childElement.wrap(`<div class="wrapImage">`);
    // In ra HTML mới
    console.log($.html());

    return $.html();
}

function createImageLazyLoadingV3(content_article) {
    const IMAGE = ["img"];
    const $ = cheerio.load(content_article);
    let imgs = $(IMAGE.join(","));
    console.log("header", imgs);
    imgs.each(function (i, elem) {
        $(this).attr("loading", "lazy");
    });
    return $.html();
}

function createImageLazyLoading(content_article) {
    const IMAGE = ["img"];
    const $ = cheerio.load(content_article);

    // Lấy phần tử con
    const childElement = $('img');
    // Xóa phần tử con từ tag parent cũ
    childElement.unwrap();

    // Thêm thuộc tính loading="lazy" và xử lý longdesc cho mỗi thẻ img
    $(IMAGE.join(",")).each(function (i, elem) {
        const $img = $(this);

        // Lấy giá trị của thuộc tính longdesc từ thẻ img
        const longdescValue = $img.attr('longdesc');
        console.log('Value of longdesc attribute:', longdescValue);

        // Thêm thuộc tính loading="lazy"
        $img.attr("loading", "lazy");

        // Tạo thẻ div mới và thêm vào mỗi thẻ img
        const $div = $('<div class="wrapImage"></div>');

        // Thay thế mỗi thẻ img bằng thẻ div mới
        $img.replaceWith(function () {
            const $clone = $img.clone();

            // Kiểm tra và thêm giá trị longdesc vào thẻ div nếu tồn tại
            if (longdescValue !== undefined) {
                const $p = $('<p></p>').text(longdescValue);
                $div.append($clone, $p);
            } else {
                $div.append($clone);
            }

            return $div;
        });
    });

    return $.html();
}

function createRelNoFollowHref(content_article) {
    const AHREF = ["a"];
    const $ = cheerio.load(content_article);

    // Thêm thuộc tính rel="nofollow"
    $(AHREF.join(",")).each(function (i, elem) {
        const $href = $(this);
        const href = $href.attr('href');
        if (href && !href.startsWith(`${FE_HOST}`)) {
            // Thêm thuộc tính rel="nofollow"
            $href.attr("rel", "nofollow");
        }
    });

    return $.html();
}

function removeListItemsByStrongValue(content, value, valueOld, valueNew) {
    if (valueOld !== valueNew) {
        return content.replace(value, "")
    } else if (valueOld == valueNew) {
        const doc = new JSDOM(content).window.document

        // Tìm tất cả các thẻ strong trong thẻ li
        var liElements = doc.querySelectorAll('li strong');

        // Duyệt qua từng thẻ strong
        liElements.forEach(function (strongElement) {
            // Lấy giá trị trong thẻ strong
            var strongValue = strongElement.textContent.trim();

            // Kiểm tra xem giá trị có bằng targetValue không
            if (strongValue === value) {
                // Nếu có, lấy thẻ li chứa strong và xóa nó
                var liElement = strongElement.closest('li');
                liElement.parentNode.removeChild(liElement);
            }
        });

        // Trả về chuỗi HTML sau khi xóa
        return doc.body.innerHTML;
    }
}

function convertStringHtml(content_article) {
    let content = `<p>` + content_article + `</p>`;
    content = content.replaceAll("<p><p>", "<p>");
    content = content.replaceAll("</p></p>", "</p>");
    content = content.replaceAll("<br />\r\n", "</p><p>");
    content = content.replaceAll("\r\n", "</p><p>");
    return content;
}

function convertStringHtmlH4(content_article) {
    let content = `<p>` + content_article + `</p>`;
    content = content.replaceAll("<p><p>", "<p>");
    content = content.replaceAll("</p></p>", "</p>");
    content = content.replaceAll("<br />\r\n", "</p><p>");
    content = content.replaceAll("\r\n", "</p><p>");

    content = content.replaceAll("p>", `h4>`);
    return content;
}

function convertTitleBrandStory(content_article) {
    const lengthContent = content_article.length;
    let indexOf1 = content_article?.indexOf("[") || 0;
    let indexOf2 = content_article?.indexOf("]") || 0;

    const title1 = content_article?.slice(0, indexOf1).trim() || "";
    const title2 = content_article?.slice(indexOf1, indexOf2 + 1).trim() || "";
    const title3 = content_article?.slice(indexOf2 + 1, lengthContent).trim() || "";

    return {
        title1: title1,
        title2: title2.replace("[", "<strong>").replace("]", "</strong>"),
        title3: title3,
    };
}

module.exports = {
    createImageLazyLoading,
    removeListItemsByStrongValue,
    createRelNoFollowHref,
    convertStringHtml,
    convertTitleBrandStory,
    convertStringHtmlH4,
}


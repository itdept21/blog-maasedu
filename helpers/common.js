const fs = require('fs');

function createSlugFromTitle(alias) {
    let str = alias;
    if (str && str.length > 0) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        str = str.replace(/\s+/g, "-");
    }
    return str;
}

function createSlugFromTitleV2(alias) {
    let str = alias;
    if (str && str.length > 0) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        str = str.replace(/\s+/g, "+");
    }
    return str;
}

function formatDate(date) {
    var year = date.getFullYear().toString();
    var month = (date.getMonth() + 101).toString().substring(1);
    var day = (date.getDate() + 100).toString().substring(1);

    var HH = addZero(date.getHours()).toString();
    var mm = addZero(date.getMinutes()).toString();

    return day + "_" + month + "_" + year + "_" + HH + "_" + mm;
    // return day + "_" + month + "_" + HH + "_" + mm;
}

function addZero(i) {
    if (i < 10) { i = "0" + i }
    return i;
}

function ERPcreateSlugFromTitle(alias) {
    let str = alias;
    if (str && str.length > 0) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        str = str.replace(/\s+/g, "-");
    }
    return str;
}

function createEdgeNGramsRegex(str) {
    if (str && str.length > 3) {
        const minGram = 3
        const maxGram = str.length

        return str.split(" ").reduce((ngrams, token) => {
            if (token.length > minGram) {
                for (let i = minGram; i <= maxGram && i <= token.length; ++i) {
                    ngrams = [...ngrams, token.substr(0, i)]
                }
            } else {
                ngrams = [...ngrams, token]
            }
            return ngrams
        }, []).join("|")
    }

    return str
}

function createEdgeNGramsRegexV2(str) {
    if (str && str.length > 5) {
        const minGram = 5
        const maxGram = str.length
        str = str.replace(/\s/g, '');

        return str.split(" ").reduce((ngrams, token) => {
            if (token.length > minGram) {
                for (let i = minGram; i <= maxGram && i <= token.length; ++i) {
                    ngrams = [...ngrams, token.substr(0, i)]
                }
            } else {
                ngrams = [...ngrams, token]
            }
            return ngrams
        }, []).join("|")
    }

    return str
}

const splitWords = (text) => {
    text = text.replace(/(^\s*)|(\s*$)/gi, '') //exclude  start and end white-space
    text = text.replace(/\n/g, ' ')
    text = text.replace(/[ ]{2,}/gi, ' ') //2 or more space to 1
    return text.split(' ').filter((s) => !!s.trim())
}

const countWords = (text) => {
    return splitWords(text).length
}

const wordExists = (
    text,
    keyword,
    firstNumChar
) => {
    text = text.toLowerCase()
    const arrKeyword = splitWords(keyword.toLowerCase())
    if (firstNumChar) {
        let i = firstNumChar - 1
        while (text[i] && text[i]?.trim()) {
            i++
        }
        return (
            arrKeyword.length > 0 &&
            arrKeyword.every((k) => splitWords(text.slice(0, i)).includes(k))
        )
    }
    return (
        arrKeyword.length > 0 &&
        arrKeyword.every((k) => splitWords(text).includes(k))
    )
}

module.exports = {
    createEdgeNGramsRegex,
    createEdgeNGramsRegexV2,
    createSlugFromTitle,
    createSlugFromTitleV2,
    formatDate,
    ERPcreateSlugFromTitle,
    splitWords,
    countWords,
    wordExists,

}

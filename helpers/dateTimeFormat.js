const moment = require('moment')
moment.locale('vi')

const DATE_FROM_NOW = (date = new Date()) => {
    const newDate = new Date(date) || new Date()
    return moment(newDate).fromNow()
}

const DATE_FORMAT_FN = (date, format = 'DD/MM/YYYY HH:mm') => {
    let temp = new Date(date);
    temp.setHours(temp.getHours() + 7);
    return moment(temp).format(format);
}

// const DATE_ORDER_FN = (date) => {
//     let temp = new Date(date);
//     temp.setHours(temp.getHours() + 7);
//     return moment(temp).format('YYMMDDHH');
// }

const monthly = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const formatDate = (d) => {
    const date = d.getDate();
    const month = monthly[d.getMonth()];
    const hour = d.getHours();
    const min = d.getMinutes();
    const year = d.getFullYear();
    const dateFormatted = `${month} ${date},${year} at ${hour}:${min} (GMT +7)`;
    return dateFormatted;
};

module.exports = { DATE_FROM_NOW, DATE_FORMAT_FN, formatDate }

const passportConfig = require('./authentication');
const fileUploader = require('./uploadFile');
const dateTimeFormat = require('./dateTimeFormat');
const roleChecker = require('./roleChecker');
const session = require('./session');

const parseFormData = fileUploader.uploadImage.none();
const parseFormDataDocument = fileUploader.uploadDocument.none()

module.exports = { passportConfig, fileUploader, dateTimeFormat, roleChecker, session, parseFormData, parseFormDataDocument }
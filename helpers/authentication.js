// @ts-nocheck
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const adminModel = require("../models/admin.model");

function initialize() {
	passport.serializeUser(function (user, done) {
		const { _id, username, name, role } = user;
		done(null, { _id, username, name, role });
	});

	passport.deserializeUser(function (obj, done) {
		done(null, obj);
	});

	passport.use(
		"local.login",
		new LocalStrategy(
			{
				usernameField: "username",
				passwordField: "password",
			},
			function (username, password, done) {
				// Find User
				adminModel.findOne({ username }, function (err, user) {
					if (err) return done(err);
					if (!user) return done(null, false, { error: true, message: "user_not_exists" });
					if (!user.comparePassword(password) && user.password)
						return done(null, false, { error: true, message: "password_not_match" });
					return done(null, user);
				});
			}
		)
	);
}

const PASSPORT_LOGIN_OPTIONS = {
	successRedirect: "/admin/dashboard",
	failureRedirect: "/admin?wrong=true",
}

module.exports = { initialize, PASSPORT_LOGIN_OPTIONS }
// @ts-nocheck
require("dotenv").config();
const multer = require("multer");
const uuid = require("uuid");

const { SEO_ALL_IMAGES_IN_DIRECTORY } = require("../tools/toolImage");


const {
	DOCS_STORAGE_DIR = "./public/uploads/documents",
	PUBLIC_STORAGE_DIR = "./public",
	IMAGES_STORAGE_DIR_SRC = "/uploads/images/original_images/",
	DOCS_STORAGE_DIR_SRC = "/uploads/documents/",
	IMAGES_STORAGE_DIR = "./public/uploads/images/original_images",
	IMAGES_STORAGE_LARGE = "./public/uploads/images/large_images",
	IMAGES_STORAGE_MEDIUM = "./public/uploads/images/medium_images",
	IMAGES_STORAGE_SMALL = "./public/uploads/images/small_images",
	MAX_IMAGE_SIZE_MB = 5,

	FILES_STORAGE_DIR = "./public/uploads/files",
	FILES_STORAGE_DIR_SRC = "/uploads/files/",
	MAX_FILE_SIZE_MB = 5,

	IMAGES_STORAGE_USER_SRC = "/user_images/",
	IMAGES_STORAGE_USER = "./public/user_images",
} = process.env;

const IMAGE_MODEL = require("../models/image.model");
const DOC_MODEL = require("../models/doc.model");
const FILE_MODEL = require("../models/file.model");

function get_file_extension(fileOriginalName = "") {
	return fileOriginalName.split(".").pop();
}

function cleanString(input) {
	var output = "";
	for (var i = 0; i < input.length; i++) {
		if (input.charCodeAt(i) <= 127 || (input.charCodeAt(i) >= 160 && input.charCodeAt(i) <= 255)) {
			output += input.charAt(i);
		}
	}
	return output;
}

function removeVietnameseTones(str) {
	str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
	str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
	str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
	str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
	str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
	str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
	str = str.replace(/đ/g, "d");
	str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
	str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
	str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
	str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
	str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
	str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
	str = str.replace(/Đ/g, "D");
	// Some system encode vietnamese combining accent as individual utf-8 characters
	// Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
	str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
	str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
	// Remove extra spaces
	// Bỏ các khoảng trắng liền nhau
	str = str.replace(/ + /g, " ");

	str = str.trim();
	// Remove punctuations
	// Bỏ dấu câu, kí tự đặc biệt
	str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
	str = cleanString(str.replace(/\x00/g, ""));
	str = str.replace(/\s+/g, "-");
	return str.toLocaleLowerCase();
}

// <MEDIA FILTER FUNCTIONS> --------------------------
function image_file_filter(req, file, cb) {
	if (!file.mimetype.match("image")) {
		req.imageError = true;
		return cb(false);
	}
	cb(null, true);
}
function document_file_filter(req, file, cb) {
	if (!file.mimetype.match(/msword|officedocument|pdf/)) {
		req.docError = true;
		return cb(false);
	}
	cb(null, true);
}

function txt_file_filter(req, file, cb) {
	if (!file.mimetype.match(/text.plain/)) {
		req.docError = true;
		return cb(false);
	}
	cb(null, true);
}

function xml_file_filter(req, file, cb) {
	if (!file.mimetype.match(/XML/)) {
		req.docError = true;
		return cb(false);
	}
	cb(null, true);
}

function randomString(len) {
	const p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	return [...Array(len)].reduce(a => a + p[~~(Math.random() * p.length)], '');
};

// <FILE FILTER FUNCTIONS/> --------------------------

// <Storages > ------------------------
const imageStorage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, IMAGES_STORAGE_DIR);
	},

	filename: function (req, file, cb) {
		const fileExtensions = get_file_extension(file.originalname);
		const fileRawName = file.originalname.split("." + fileExtensions)[0];

		const cleanFileName = decodeURIComponent(removeVietnameseTones(fileRawName));
		const filename = `${cleanFileName}-${Math.round(Math.random() * 10000)}.${fileExtensions}`;
		cb(null, filename);
	},
});

const imageStorageUser = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, IMAGES_STORAGE_USER);
	},

	filename: function (req, file, cb) {
		const fileExtensions = get_file_extension(file.originalname);
		const fileRawName = file.originalname.split("." + fileExtensions)[0];

		const cleanFileName = decodeURIComponent(removeVietnameseTones(fileRawName));
		const filename = `${cleanFileName}-${randomString(16)}.${fileExtensions}`;
		cb(null, filename);
	},
});

const documentStore = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, DOCS_STORAGE_DIR);
	},

	filename: function (req, file, cb) {
		const fileExtensions = get_file_extension(file.originalname);
		const fileRawName = file.originalname.split("." + fileExtensions)[0];
		const filename =
			removeVietnameseTones(fileRawName) + "-" + Math.round(Math.random() * 10000) + "." + fileExtensions;

		cb(null, `${filename}`);
	},
});

const fileStore = multer.diskStorage({
	destination: function (req, files, cb) {
		cb(null, FILES_STORAGE_DIR);
	},

	filename: function (req, files, cb) {
		const fileExtensions = get_file_extension(files.originalname);
		const fileRawName = files.originalname.split("." + fileExtensions)[0];
		const filename =
			removeVietnameseTones(fileRawName) + "-" + Math.round(Math.random() * 10000) + "." + fileExtensions;
		console.log("fileStore filename ", fileExtensions, fileRawName, filename)
		cb(null, `${filename}`);
	},
});

const sitemapStore = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, PUBLIC_STORAGE_DIR);
	},

	filename: function (req, file, cb) {
		const filename = 'sitemap.xml'
		cb(null, `${filename}`);
	},
});

const robotTxtStore = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, PUBLIC_STORAGE_DIR);
	},

	filename: function (req, file, cb) {
		const filename = 'robots.txt'
		cb(null, `${filename}`);
	},
});


// <Storages /> ------------------------

async function saveSingleImagesToDB(req, res, next) {
	console.log("saveSingleImagesToDB", req.file)
	if (req.file) {
		const image = await new IMAGE_MODEL({
			name: req.file.filename,
			alt: req.file.filename,
			src: IMAGES_STORAGE_DIR_SRC + req.file.filename,
			createdBy: req.user.username,
		}).save();
		req.file._id = image._id;
		// generate 3 images with difference size for thumbnail images
		await Promise.all([
			SEO_ALL_IMAGES_IN_DIRECTORY("./public" + image.src, IMAGES_STORAGE_LARGE, "desktop", 90),
			SEO_ALL_IMAGES_IN_DIRECTORY("./public" + image.src, IMAGES_STORAGE_MEDIUM, "tablet", 90),
			SEO_ALL_IMAGES_IN_DIRECTORY("./public" + image.src, IMAGES_STORAGE_SMALL, "mobile", 80),
		]);
	}

	if (req.files) {
		const fieldNames = Object.keys(req.files);
		for (let i = 0; i < fieldNames.length; i++) {
			const images = req.files[fieldNames[i]]
				? await Promise.all(
					req.files[fieldNames[i]].map((e) =>
						new IMAGE_MODEL({
							name: e.filename,
							src: IMAGES_STORAGE_DIR_SRC + e.filename,
							alt: e.filename,
							createdAt: new Date(),
							createdBy: req?.user?.username || 'ufo',
						}).save()
					)
				)
				: [];
			req.files[fieldNames[i]] = images;

			for (let index = 0; index < images.length; index++) {
				await Promise.all([
					SEO_ALL_IMAGES_IN_DIRECTORY("./public" + images[index].src, IMAGES_STORAGE_LARGE, "desktop", 90),
					SEO_ALL_IMAGES_IN_DIRECTORY("./public" + images[index].src, IMAGES_STORAGE_MEDIUM, "tablet", 90),
					SEO_ALL_IMAGES_IN_DIRECTORY("./public" + images[index].src, IMAGES_STORAGE_SMALL, "mobile", 80),
				]);
			}
		}
	}

	if (!req.files && !req.file) {
		req.uploadError = true;
	}
	next();
}

async function saveSingleDocsToDB(req, res, next) {
	if (req.file) {
		const doc = await new DOC_MODEL({
			name: req.file.filename,
			src: DOCS_STORAGE_DIR_SRC + req.file.filename,
			type: get_file_extension(req.file.filename) == "pdf" ? "pdf" : get_file_extension(req.file.filename) == "txt" ? "txt" : "doc",
		}).save();
		req.file = doc;
	}

	if (req.files) {
		const fieldNames = Object.keys(req.files);
		for (let i = 0; i < fieldNames.length; i++) {
			const docs = req.files[fieldNames[i]]
				? await Promise.all(
					req.files[fieldNames[i]].map((e) =>
						new DOC_MODEL({
							name: e.filename,
							src: DOCS_STORAGE_DIR_SRC + e.filename,
							createdAt: new Date(),
							type: get_file_extension(e.filename) == "pdf" ? "pdf" : get_file_extension(e.filename) == "txt" ? "txt" : "doc",
						}).save()
					)
				)
				: [];
			req.files[fieldNames[i]] = docs;
		}
	}

	if (!req.files && !req.file) {
		req.uploadError = true;
	}
	next();
}


///////////// file
async function saveSingleFilesToDB(req, res, next) {
	if (req.file) {
		const doc = await new FILE_MODEL({
			name: req.file.filename,
			src: FILES_STORAGE_DIR_SRC + req.file.filename,
			type: get_file_extension(req.file.filename),
		}).save();
		req.file = doc;
	}

	if (req.files) {
		const fieldNames = Object.keys(req.files);
		for (let i = 0; i < fieldNames.length; i++) {
			const docs = req.files[fieldNames[i]]
				? await Promise.all(
					req.files[fieldNames[i]].map((e) =>
						new FILE_MODEL({
							name: e.filename,
							src: FILES_STORAGE_DIR_SRC + e.filename,
							createdAt: new Date(),
							type: get_file_extension(req.files.filename),
						}).save()
					)
				)
				: [];
			req.files[fieldNames[i]] = docs;
		}
	}

	if (!req.files && !req.file) {
		req.uploadError = true;
	}
	next();
}

const uploadImage = multer({
	storage: imageStorage,
	fileFilter: image_file_filter,
	limits: { fileSize: parseFloat(MAX_IMAGE_SIZE_MB) * 1000 * 1024 },
});

const uploadImageUser = multer({
	storage: imageStorageUser,
	fileFilter: image_file_filter,
	limits: { fileSize: parseFloat(MAX_IMAGE_SIZE_MB) * 1000 * 1024 },
});

const uploadDocument = multer({
	storage: documentStore,
	fileFilter: document_file_filter,
	limits: { fileSize: parseFloat(MAX_IMAGE_SIZE_MB) * 1000 * 1024 },
});

const uploadFile = multer({
	storage: fileStore,
	limits: { fileSize: parseFloat(MAX_FILE_SIZE_MB) * 1000 * 1024 },
});

const uploadSitemap = multer({
	storage: sitemapStore,
	/* fileFilter: txt_file_filter, */
	limits: { fileSize: parseFloat(MAX_IMAGE_SIZE_MB) * 1000 * 1024 },
});


const uploadRobotTxt = multer({
	storage: robotTxtStore,
	fileFilter: txt_file_filter,
	limits: { fileSize: parseFloat(MAX_IMAGE_SIZE_MB) * 1000 * 1024 },
});

// <User /> ------------------------

async function saveSingleImagesToDBUser(req, res, next) {
	if (req.file) {
		const image = await new IMAGE_MODEL({
			name: req.file.filename,
			src: IMAGES_STORAGE_USER_SRC + req.file.filename,
		}).save();
		req.file._id = image._id;
	}
	if (req.files) {
		console.log("req.files", req.files)

		const fieldNames = Object.keys(req.files);
		for (let i = 0; i < fieldNames.length; i++) {
			const images = req.files[fieldNames[i]]
				? await Promise.all(
					req.files[fieldNames[i]].map((e) =>
						new IMAGE_MODEL({
							name: e.filename,
							src: IMAGES_STORAGE_USER_SRC + e.filename,
							createdAt: new Date(),
						}).save()
					)
				)
				: [];
			req.files[fieldNames[i]] = images;
		}
	}

	if (!req.files && !req.file) {
		req.uploadError = true;
	}
	next();
}

module.exports = {
	uploadImage,
	uploadImageUser,
	uploadDocument,
	uploadFile,
	saveSingleImagesToDB,
	saveSingleDocsToDB,
	uploadSitemap,
	uploadRobotTxt,
	saveSingleImagesToDBUser,
	saveSingleFilesToDB,
};

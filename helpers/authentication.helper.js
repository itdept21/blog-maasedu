// @ts-nocheck
const brcypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const HASH_SALT = 7;
const SECRET_JWT = 'jsonwebtoken';
const DATE_EXPIRED = process.env.DATE_EXPIRED || 30;
const JWT_SIGNING_ALGORITHM = 'HS256';


/**
 * Description: hash a string password with brcypt's salt
 * @param {String} password
 * @returns {String?} hashed password
 */
function hashPassword(password = '') {
    return brcypt.hashSync(password, HASH_SALT);
}

/**
 * Description: comparison between raw password & encrypted password
 * @param {String} raw        raw password
 * @param {String} encrypted  hashed password
 * @returns {Boolean} /=> true if equal, false if not
 */
function comparePassword({ raw = '', encrypted = '' }) {
    return brcypt.compare(raw, encrypted);
}

/**
 * Description: return a jwt string encrypted with jwt secret with the payload {_id: UserID}
 * @param {ObjectId} userID  user identify
 * @returns {String?} encrypted information
 */
function signingToken({ authID = '', userID = null, expertID = null, adminID = null }) {
    return jwt.sign({ authID, userID, expertID, adminID }, SECRET_JWT, {
        algorithm: JWT_SIGNING_ALGORITHM,
        expiresIn: DATE_EXPIRED * 24 * 60 * 60 + 's'
    });
}

/**
 * Description: this function will return null if the input is invalid token or token has been expired
 * @param {String} token JWT Token string
 * @returns {TokenResponse?} {_id: String, iat: JWT_Date, exp: JWT_Date}
 *
 */
function parseToken(token = '') {
    try {
        const decoded = jwt.verify(token, SECRET_JWT);
        return decoded;
    } catch (error) {
        return null;
    }
}

module.exports = { hashPassword, comparePassword, parseToken, signingToken }

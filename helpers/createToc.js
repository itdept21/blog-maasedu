const cheerio = require("cheerio");

function createSlugFromTitle(alias) {
	let str = alias;
	if (str && str.length > 0) {
		str = str.toLowerCase();
		str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
		str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
		str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
		str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
		str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
		str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
		str = str.replace(/đ/g, "d");
		str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
		str = str.replace(/ + /g, " ");
		str = str.trim();
		str = str.replace(/\s+/g, "_");
	}
	return str;
}

const createContentTOC = (listLevelHeading) => {
	const list = listLevelHeading;
	let pre = 0;
	let html = list
		.map((e, i) => {
			// pre = e.level;
			if (e.level < pre) {
				let ols = "";
				let olsCount = e.level;
				while (olsCount < pre) {
					olsCount++;
					ols += "</ol>";
				}
				pre = e.level;
				return `${ols}<li><a href="#${e.id}">${e.text}</a></li>`;
			} else if (e.level > pre) {
				//3>2
				pre = e.level;
				return `<ol> <li><a href="#${e.id}">${e.text}</a></li>`;
			}
			return `<li><a href="#${e.id}">${e.text}</a></li>`;
		})
		.join("");

	if (html) {
		html = "<ol>" + html + "</ol>";
	} else {
		html = "";
	}
	return html;
};

/**
 * CREATE OL list Headings for article
 * @param {string} content_article
 */
function createToc(content_article) {
	let data = {};

	const HEADING_LEVEL_TAGS = ["h1", "h2", "h3", "h4", "h5"];

	const $ = cheerio.load(content_article);

	let headings = $(HEADING_LEVEL_TAGS.join(","));
	$(headings).each(function (i, elem) {
		// console.log("headings ------", createSlugFromTitle($(this).text()).charAt(0).toUpperCase() + createSlugFromTitle($(this).text()).slice(1))
		$(this).attr(
			"id",
			createSlugFromTitle($(this).text())
		);
	});
	let level = 0;
	let _minLevel = 5

	const buildListFromHeadings = (headings = [], from = 0, to = 0, level = 0) => {
		var result = [];
		for (let i = from; i < to; i++) {
			if (_minLevel > HEADING_LEVEL_TAGS.indexOf(headings[i].tagName.toLocaleLowerCase())) {
				_minLevel = HEADING_LEVEL_TAGS.indexOf(headings[i].tagName.toLocaleLowerCase())
			};
			result.push({
				level: HEADING_LEVEL_TAGS.indexOf(headings[i].tagName.toLocaleLowerCase()),
				text: $(headings[i]).text(),
				id: headings[i].attribs.id,
				_id: i,
			});
		}
		return result;
	};

	const listLevelHeading = buildListFromHeadings(headings, 0, headings.length, level);
	data.html = createContentTOC(listLevelHeading);
	data.content = $("body").html();
	data.menuList = listLevelHeading.map((e) => {
		return {
			title: e.text,
			_id: e._id,
			id: e.id,
			style: HEADING_LEVEL_TAGS[e.level],
			level: e.level,
		}
	});
	data.minLevel = _minLevel
	return data;
}


module.exports = createToc;
// addRestoreEvent();

function openNewTab({ url = "/admin/gallery", windowName = "", win = window, w = 1080, h = 800, callback = () => { } }) {
	// @ts-ignore
	const y = win.top.outerHeight / 2 + win.top.screenY - h / 2;
	// @ts-ignore
	const x = win.top.outerWidth / 2 + win.top.screenX - w / 2;

	const config = `toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${w}, height=${h}, top=${y}, left=${x}`;
	const newWindow = win.open(url, windowName, config);
	// @ts-ignore
	return callback(newWindow);
}

function openCreateOrUpdateChildItem(image, inputID = "", url = "/admin/gallery", inputData = '') {
	let _targetData = image.parentElement.querySelector('input');
	let params = _targetData?.value || "";

	if (params == "") {
		// Sử dụng hàm và Promise
		createId(url)
			.then(docID => {
				params = docID;
				console.log("Doc ID:", docID);
				proceedWithParams(image, inputID, url + '/' + params, inputData);
			})
			.catch(error => {
				console.error(error);
			});
	} else {
		proceedWithParams(image, inputID, url + '/' + params, inputData);
	}
}

function proceedWithParams(image, inputID = "", url = "/admin/gallery", inputData = '') {
	console.log("proceedWithParams", inputID, url, inputData,)
	let _targetData = image.parentElement.querySelector('input');
	console.log("proceedWithParams _targetData", _targetData)
	const _url = url
	// @ts-ignore
	const windowGalleryHandler = (windowGallery = window.open("/")) => {
		window.addEventListener('message', function (event) {
			if (event.origin !== window.origin) {
				// Kiểm tra nguồn gốc của thông điệp
				return;
			}
			const data = event.data;
			console.log("event.data", data);
			if (data.title && data.id) {
				// Tạm ngưng sự kiện
				$(image.parentElement.querySelector('select')).off("change.select2");

				// Phá hủy select2
				$(image.parentElement.querySelector('select')).select2("destroy");

				// Lấy đối tượng Select2 từ phần tử <select> có id là 'mySelect'
				const mySelect = $(image.parentElement.querySelector('select')).select2();
				/// Kiểm tra xem option có tồn tại hay không
				const existingOption = mySelect.find(`option[value='${data.id}']`);

				if (existingOption.length) {
					// // Nếu option đã tồn tại, cập nhật giá trị
					existingOption.text(data.title);
					$(image.parentElement.querySelector('select')).val(data.id).trigger('change');
					_targetData.value = data.id;
				} else {
					// Nếu option chưa tồn tại, thêm mới
					$(image.parentElement.querySelector('select')).append(new Option(data.title, data.id)).trigger('change');
					$(image.parentElement.querySelector('select')).val(data.id).trigger('change');
					_targetData.value = data.id || _targetData.value;
				}

				$(image.parentElement.querySelector('select')).select2({ width: "100%" });
			}
		});
	};
	openNewTab({ url: _url, callback: windowGalleryHandler });
}

function removeDuplicateOptions(selectId) {
	const selectElement = document.querySelector(selectId);
	// Tạo một mảng để lưu trữ giá trị đã xuất hiện
	const seenValues = [];
	// Lặp qua từng option trong select
	for (let i = 0; i < selectElement.options.length; i++) {
		const option = selectElement.options[i];

		// Kiểm tra xem giá trị đã xuất hiện chưa
		if (seenValues.includes(option.value)) {
			// Nếu giá trị đã xuất hiện, xóa option này
			selectElement.remove(i);
			i--; // Giảm giá trị của i để tránh bỏ qua option mới xuất hiện sau khi xóa
		} else {
			// Nếu giá trị chưa xuất hiện, thêm vào mảng seenValues
			seenValues.push(option.value);
		}
	}
}

function createId(url) {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				const result = JSON.parse(xhr.responseText);
				let data = result.data;
				if (!result.error && data) {
					const docID = data._id;
					resolve(docID);
				} else {
					// reject("Khởi tạo thất bại");
					reject();
				}
			}
		};
		xhr.open("POST", url);
		xhr.send();
	});
}
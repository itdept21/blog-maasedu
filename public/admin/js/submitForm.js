// @ts-ignore
document.addEventListener("DOMContentLoaded", async (ev) => {
	const selectInput = document.querySelector(".select-search-input");

	if (selectInput)
		// @ts-ignore
		$(".select-search-input").select2();

	// ============= FORM CREATE - UPLOAD HANDLERS
	// variables
	let formArr = document.querySelectorAll(".form-horizontal");

	let contentEditors = document.querySelectorAll(".content-editor");
	let openPopupButtonArr = document.querySelectorAll(".open-modal");
	let titleContentInput = document.querySelector('[name="title"].title-content');
	let slugContentInput = document.querySelector('[name="slug"].slug-content');

	let editor = [];
	if (contentEditors)
		contentEditors.forEach((item, index) => {
			// console.log(item.id)
			// @ts-ignore
			editor[index] = CKEDITOR.replace(`${item.id}`, {
				height: 500,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})

	let contentEditorsComponentF1 = document.querySelectorAll(".content-editor-component-f1");
	let editorComponentF1 = [];
	if (contentEditorsComponentF1)
		contentEditorsComponentF1.forEach((item, index) => {
			console.log(item.id)
			// @ts-ignore
			editorComponentF1[index] = CKEDITOR.replace(`${item.id}`, {
				height: 200,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})

	let contentEditorsComponentF2 = document.querySelectorAll(".content-editor-component-f2");
	let editorComponentF2 = [];
	if (contentEditorsComponentF2)
		contentEditorsComponentF2.forEach((item, index) => {
			console.log(item.id)
			// @ts-ignore
			editorComponentF2[index] = CKEDITOR.replace(`${item.id}`, {
				height: 200,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})

	let contentEditorsComponentF3 = document.querySelectorAll(".content-editor-component-f3");
	let editorComponentF3 = [];
	if (contentEditorsComponentF3)
		contentEditorsComponentF3.forEach((item, index) => {
			console.log(item.id)
			// @ts-ignore
			editorComponentF3[index] = CKEDITOR.replace(`${item.id}`, {
				height: 200,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})

	let contentEditorsComponentF4 = document.querySelectorAll(".content-editor-component-f4");
	let editorComponentF4 = [];
	if (contentEditorsComponentF4)
		contentEditorsComponentF4.forEach((item, index) => {
			editorComponentF4[index] = CKEDITOR.replace(`${item.id}`, {
				height: 200,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})

	const copyButtonsDiv3 = document.querySelectorAll(".copy-button-div-3");
	copyButtonsDiv3.forEach((button) => {
		button.onclick = copyTargetAndPasteDiv3;
	});

	function setDeleteEvent() {
		const removeCategoriesButtons = document.querySelectorAll(".post-remove-btn");
		removeCategoriesButtons.forEach((button) => {
			button.onclick = removeConfirmedCategory;
		});
	}

	let editor_v2 = [];
	function copyTargetAndPasteDiv3() {
		console.log(this.dataset.copy)
		const copy = document.querySelector(this.dataset.copy);
		const paste = document.querySelector(this.dataset.paste);

		const newItem = copy.cloneNode(true);
		const newID = Date.now();
		newItem.setAttribute("id", `new-${newID}`);
		newItem.querySelectorAll("input").forEach((_) => (_.value = ""));
		newItem.querySelectorAll("img").forEach((_) => (_.src = "/wp-content/noimage.jpg"));
		newItem.querySelector("#content-editor-1234567890").innerHTML = createTextareaCKEditor(newID);
		paste.appendChild(newItem);
		let contentEditors_v2 = document.querySelectorAll(".content-editor-v2")
		console.log("contentEditors_v2.length", contentEditors_v2.length)
		const _lengthContentEditors_v2 = contentEditors_v2.length - 1
		editor_v2[_lengthContentEditors_v2] = CKEDITOR.replace(`content-editor-${newID}`, {
			height: 500,
			entities: false,
			allowedContent: true,
			entities_additional: "",
			entities_greek: false,
			entities_latin: false,
			filebrowserUploadUrl: "/admin/upload-ckeditor",
			filebrowserBrowseUrl: "/admin/upload-ckeditor",
		});

		setDeleteEvent();
	}

	function removeConfirmedCategory(e) {
		const confirmed = window.confirm("Bạn có chắc chắn muốn xoá ?");
		if (confirmed) {
			this.parentElement.parentElement.remove();
		}
	}

	function createTextareaCKEditor(index) {
		return `
			<label>Answer to the question</label>
			<textarea name="childContents" class="content-editor-v2" id="content-editor-${index}"></textarea>
			<br />`;
	}

	// functions
	function createSlugFromTitle(alias) {
		let str = alias;
		if (str && str.length > 0) {
			str = str.toLowerCase();
			str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
			str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
			str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
			str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
			str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
			str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
			str = str.replace(/đ/g, "d");
			str = str.replace(
				/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
				" "
			);
			str = str.replace(/ + /g, " ");
			str = str.trim();
			str = str.replace(/\s+/g, "-");
		}
		return str;
	}

	function getAllCheckboxes() {
		// Lấy tất cả các checkbox có tên là 'isCreated[]'
		const checkboxes = document.querySelectorAll('input[name="isCreated[]"]');

		// Duyệt qua từng checkbox và hiển thị thông tin
		checkboxes.forEach((checkbox, index) => {
			console.log(`Checkbox ${index + 1}: ${checkbox.id}, Checked: ${checkbox.checked}`);
		});
	}

	function formSubmitHandler(event) {
		event.preventDefault();

		// @ts-ignore
		Swal.fire(" Đang tải ... ");

		let fd = new FormData(this);
		let that = this;

		let contentEditors = this.querySelectorAll(".content-editor");
		const dataCKEditor = []
		if (contentEditors) {
			contentEditors.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editor[index].getData()
				})
			})
		}

		let contentEditorsComponentF1 = this.querySelectorAll(".content-editor-component-f1");
		if (contentEditorsComponentF1) {
			contentEditorsComponentF1.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editorComponentF1[index].getData()
				})
			})
		};

		let contentEditorsComponentF2 = this.querySelectorAll(".content-editor-component-f2");
		if (contentEditorsComponentF2) {
			contentEditorsComponentF2.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editorComponentF2[index].getData()
				})
			})
		};


		let contentEditorsComponentF3 = this.querySelectorAll(".content-editor-component-f3");
		if (contentEditorsComponentF3) {
			contentEditorsComponentF3.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editorComponentF3[index].getData()
				})
			})
		};

		let contentEditorsComponentF4 = this.querySelectorAll(".content-editor-component-f4");
		if (contentEditorsComponentF4) {
			contentEditorsComponentF4.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editorComponentF4[index].getData()
				})
			})
		};

		let contentEditorsV2 = this.querySelectorAll(".content-editor-v2");
		if (contentEditorsV2) {
			contentEditorsV2.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editor_v2[index].getData()
				})
			})
		};
		console.log("fd.set --------", JSON.stringify({ ...fd }));


		console.log("dataCKEditor --------", JSON.stringify(dataCKEditor));

		// get name in dataCKEditor not duplicate
		let nameCKEditor = dataCKEditor.map((item) => item.name)
		const uniqueNameCKEditor = Array.from(new Set(nameCKEditor));
		console.log("nameCKEditor", JSON.stringify(uniqueNameCKEditor));

		// check trùng name -> fd.set array name
		uniqueNameCKEditor.forEach((item, index) => {
			const _value = dataCKEditor.filter((e) => e.name.toString() == item.toString());
			console.log("_value", _value)
			fd.delete(item)
			if (_value.length > 1) {
				for (var i = 0; i < _value.length; i++) {
					fd.append(`${item}[]`, _value[i].value);
				}
			} else {
				fd.append(item, _value?.[0]?.value || "")
			}
		});
		console.log("fd.set --------", fd.get("childContents[]"));
		// Lấy mảng giá trị của tất cả các checkbox có tên là 'isCreated[]'

		const checkboxesIsCreated = document.querySelectorAll('input[name="isCreated[]"]');
		if (checkboxesIsCreated.length > 0) fd.delete('isCreated[]')
		checkboxesIsCreated.forEach((checkbox, index) => {
			console.log(`Checkbox ${index + 1}: ${checkbox.id}, Checked: ${checkbox.checked}`);
			fd.append(`isCreated[]`, checkbox.checked ? '1' : '0');
		});
		console.log("fd.isCreated --------", fd.getAll("isCreated[]"));

		const checkboxesIsUpdated = document.querySelectorAll('input[name="isUpdated[]"]');
		if (checkboxesIsUpdated.length > 0) fd.delete('isUpdated[]')
		checkboxesIsUpdated.forEach((checkbox, index) => {
			fd.append(`isUpdated[]`, checkbox.checked ? '1' : '0');
		});

		const checkboxesIsDelete = document.querySelectorAll('input[name="isDelete[]"]');
		if (checkboxesIsDelete.length > 0) fd.delete('isDelete[]')
		checkboxesIsDelete.forEach((checkbox, index) => {
			fd.append(`isDelete[]`, checkbox.checked ? '1' : '0');
		});

		const checkboxesIsGet = document.querySelectorAll('input[name="isGet[]"]');
		if (checkboxesIsGet.length > 0) fd.delete('isGet[]')
		checkboxesIsGet.forEach((checkbox, index) => {
			fd.append(`isGet[]`, checkbox.checked ? '1' : '0');
		});

		const checkboxesMenu = document.querySelectorAll('input[name="menu[]"]');
		if (checkboxesMenu.length > 0) fd.delete('menu[]')
		checkboxesMenu.forEach((checkbox, index) => {
			fd.append(`menu[]`, checkbox.id);
		});

		// parse all the input, textarea tags value to an object, name is form data (fd)
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			// console.log(xhr.responseText)
			// if server has been responded and status is OK
			if (xhr.readyState == 4) {
				const result = JSON.parse(xhr.responseText);
				let data = result.data;

				if (!result.error && data) {
					// @ts-ignore
					Swal.fire({
						title: "Đăng thành công",
						icon: "success"
						// @ts-ignore
					}).then((value) => {
						/* window.location.reload() */
					});
					// variables
					let categoriesList = document.querySelector('select[name="category"]');
					// functions
					let createCategoryOption = ({ _id, name }) => {
						return `<option value="${_id}">${name}</option>`;
					};

					//events
					if (data._id && data.name && categoriesList) {
						categoriesList.innerHTML += createCategoryOption(data);
					}

					setTimeout(() => {
						if (that.dataset.callbackurl && that.dataset.callbackurl !== "false") {
							console.log(that.dataset.callbackurl, typeof that.dataset.callbackurl);
							return (window.location.href = that.dataset.callbackurl);
						}
						return window.location.reload();
					}, 1000);
				} else {
					let messError = result?.message || "Hãy điền đầy đủ thông tin bắt buộc hoặc bạn không có quyền làm việc này  !"
					// @ts-ignore
					Swal.fire(
						"Đăng thất bại",
						messError,
						"warning"
					);
				}
			}
		};

		xhr.open(this.getAttribute("method"), this.getAttribute("action"), true); // current URL
		xhr.send(fd); // send as req.body to the router
	}

	//events
	if (formArr) formArr.forEach((form) => form.addEventListener("submit", formSubmitHandler));

	if (openPopupButtonArr)
		openPopupButtonArr.forEach((button) => {
			console.log(button);
			// @ts-ignore
			button.addEventListener("click", openModalPopup);
		});

	if (titleContentInput && slugContentInput) {
		titleContentInput.addEventListener("change", () => {
			// @ts-ignore
			let slug = titleContentInput.value;
			slug.toLowerCase();
			// @ts-ignore
			slugContentInput.value = createSlugFromTitle(slug).replace(/ú|ù|ũ|ủ|ụ/g, "u");
		});
	};

	document.querySelectorAll(".count-word-content-preview").forEach((button) => {
		// console.log("count-word-content-preview")
		button.addEventListener("click", countWordStringInput);
	});

	function countWordStringInput() {
		let contentEditors_test = document.querySelectorAll(".content-editor");
		const dataCKEditor_test = []
		if (contentEditors_test) {
			contentEditors_test.forEach((item, index) => {
				dataCKEditor_test.push({
					name: item.getAttribute("name"),
					value: editor[index].getData()
				})
			})
		};
		const _content = dataCKEditor_test.filter((e) => e.name.toString() == "content")?.[0].value

		const _words = currencyFormat(countWords(_content) || 0);
		const _characters = currencyFormat(countCharacters(_content) || 0);
		// console.log("count-word-content-preview", _words, _characters)

		document.querySelector(".count-word-content-preview").innerHTML = _words + ' words - ' + _characters;
	}

	function countWords(s) {
		s = s.toString().replaceAll(/\n/g, ' '); // newlines to space
		s = s.toString().replaceAll(/(^\s*)|(\s*$)/gi, ''); // remove spaces from start + end
		s = s.toString().replaceAll(/[ ]{2,}/gi, ' '); // 2 or more spaces to 1
		return s.split(' ').length;
	};

	function countCharacters(s) {
		return s.length;
	};

	function currencyFormat(num) {
		return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};

	const checkFocusKeyPhrase = document.querySelector('#check-focus-key-phrase-btn')
	if (checkFocusKeyPhrase) {
		checkFocusKeyPhrase.onclick = (evt) => {
			evt.preventDefault();

			const seoTitleInput = document.querySelector(".count-word-seo-title-input-string");
			const _seoTitle = seoTitleInput.value;

			const seoDescriptionInput = document.querySelector(".count-word-seo-description-string");
			const _seoDescription = seoDescriptionInput.value;

			const focusKeyPhraseInput = document.querySelector(".count-word-focus-keyphrase-input-string");
			const _focusKeyPhrase = focusKeyPhraseInput.value;

			// const contentInput = document.querySelector(".analyze-content-string");
			// console.log("contentInput", contentInput)
			// const _content = contentInput.value;

			let contentEditors_test = document.querySelectorAll(".content-editor");
			const dataCKEditor_test = []
			if (contentEditors_test) {
				contentEditors_test.forEach((item, index) => {
					dataCKEditor_test.push({
						name: item.getAttribute("name"),
						value: editor[index].getData()
					})
				})
			};
			const _content = dataCKEditor_test.filter((e) => e.name.toString() == "content")?.[0].value

			const slugInput = document.querySelector(".slug-input-string");
			const _slug = slugInput.value;


			var data = new FormData();
			data.append('keyword', _focusKeyPhrase);
			data.append('title', _seoTitle);
			data.append('descriptionMeta', _seoDescription);
			data.append('content', _content);
			data.append('slug', _slug);
			console.log("data", data)

			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.data, result.message);
					const newHTMLOrderWine = result.data.seoAnalyzer.map(_ => createNodeCheckKeyPhrasePreview(_)).join();

					const checkKeyPhrasePreview = document.querySelector('#check-key-phrase-preview');

					const _seoAnalyzerScore = result?.data?.seoAnalyzerScore?.text || "";
					const _keyword = result?.data?.keyword?.split(",")?.[0] || result?.data?.keyword || "";
					const _scoreHtml = `
				<label class="label-2 pl-3 pr-3 br-7">
				  KeyPhrase: ${_keyword} <strong>( ${_seoAnalyzerScore} )</strong>
				</label>
				`
					checkKeyPhrasePreview.innerHTML = _scoreHtml + newHTMLOrderWine.replaceAll(",", "");

					//  render additional keyphrase
					const newHTMLAdditionalKeyPhrases = result.data.seoAnalyzerAdditions.map(_ => createNodeCheckKeyPhrasePreviewAdditions(_)).join();
					const checkKeyPhrasePreviewAdditionalKeyPhrases = document.querySelector('#check-key-phrase-preview-additional');
					checkKeyPhrasePreviewAdditionalKeyPhrases.innerHTML = newHTMLAdditionalKeyPhrases.replaceAll(",", "");

				} else {
					alert("Thất bại");
				}
			};
			xhr.open("POST", '/admin/check-post');
			xhr.send(data);
		}
	}

	function createNodeCheckKeyPhrasePreview(_seoAnalyzer) {
		let _icon = `<i class="fa fa-times-circle mr-2 mb-2" style="font-size:24px;color: red;"aria-hidden="true"></i>`
		if (_seoAnalyzer.status == 'perfect') {
			_icon = `<i class="fa fa-check-circle-o mr-2 mb-2" style="font-size:24px;color: green;" aria-hidden="true"> </i>`
		}
		const _key = _seoAnalyzer.key
		const _score = _seoAnalyzer.score
		const _title = _seoAnalyzer.title
		const _text = _seoAnalyzer.text
		return `
		  
		  <div class="mt-2">
						  <a style="font-size:16px;color: black;" href="#${_key}" data-toggle="collapse">
							${_icon}${_title}
						  </a>
						  <div id="${_key}" class="collapse">
							- ${_text} ( ${_score} )
						  </div>
		  </div>
		  `

	}

	function createNodeCheckKeyPhrasePreviewAdditions(_seoAnalyzerAdditions) {
		const _title = _seoAnalyzerAdditions.title
		const _key = _seoAnalyzerAdditions.key
		const _score = _seoAnalyzerAdditions.score
		const _seoAnalyzer = _seoAnalyzerAdditions?.seoAnalyzer || []
		const newHTMLAdditionChild = _seoAnalyzer.map(_ => createNodeCheckKeyPhrasePreview(_)).join();
		const innerHTML = newHTMLAdditionChild.replaceAll(",", "");
		console.log("createNodeCheckKeyPhrasePreview innerHTML ------", innerHTML)
		return `
		  <div class=" mt-2">
			<a style="font-size:16px;color: black;" href="#${_key}" data-toggle="collapse">
			- ${_title} <strong>( ${_score.text} )</strong>
			<div id="${_key}" class="collapse">
			  ${innerHTML}
			</div>
		  </a>
		  </div>
		  `
	}


	const checkPageAnalysis = document.querySelector('#check-page-analysis-btn')
	if (checkPageAnalysis) {
		checkPageAnalysis.onclick = (evt) => {
			evt.preventDefault();

			const seoTitleInput = document.querySelector(".count-word-seo-title-input-string");
			const _seoTitle = seoTitleInput.value;

			const seoDescriptionInput = document.querySelector(".count-word-seo-description-string");
			const _seoDescription = seoDescriptionInput.value;

			const focusKeyPhraseInput = document.querySelector(".count-word-focus-keyphrase-input-string");
			const _focusKeyPhrase = focusKeyPhraseInput.value;

			// const contentInput = document.querySelector(".analyze-content-string");
			// const _content = contentInput.value;
			let contentEditors_test = document.querySelectorAll(".content-editor");
			const dataCKEditor_test = []
			if (contentEditors_test) {
				contentEditors_test.forEach((item, index) => {
					dataCKEditor_test.push({
						name: item.getAttribute("name"),
						value: editor[index].getData()
					})
				})
			};
			const _content = dataCKEditor_test.filter((e) => e.name.toString() == "content")?.[0].value

			const slugInput = document.querySelector(".slug-input-string");
			const _slug = slugInput.value;

			var data = new FormData();
			data.append('keyword', _focusKeyPhrase);
			data.append('title', _seoTitle);
			data.append('descriptionMeta', _seoDescription);
			data.append('content', _content);
			data.append('slug', _slug);

			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.data, result.message);

					const _seoAnalyzerScore = result?.data?.pageAnalysisScore?.text || "0/100";
					const _scoreHtml = `
					${_seoAnalyzerScore}
					`
					const pageAnalysisScore = document.querySelector('.page-analysis-score');
					pageAnalysisScore.innerHTML = _scoreHtml;

					const newHTMLBasicSEO = result.data.pageAnalysis.basicSEO.map(_ => createNodeCheckPageAnalysis(_)).join();
					const pageAnalysisBasicSEO = document.querySelector('#pageAnalysisBasicSEO');
					pageAnalysisBasicSEO.innerHTML = newHTMLBasicSEO.replaceAll(",", "");

					const newHTMLBasicSEOCountError = result.data.pageAnalysis.basicSEOCountError || 0
					const pageAnalysisBasicSEOCountError = document.querySelector('#basic-seo-error-count');
					pageAnalysisBasicSEOCountError.innerHTML = parseInt(newHTMLBasicSEOCountError) > 0 ?
						parseInt(newHTMLBasicSEOCountError) > 1 ?
							`Basic SEO <label style="color: #f18200;">( ${parseInt(newHTMLBasicSEOCountError)} Errors )</label>` :
							`Basic SEO <label style="color: #f18200;">( ${parseInt(newHTMLBasicSEOCountError)} Error )</label>` :
						`Basic SEO <label style="color: #00aa63;">( All Good )</label>`

					const newHTMLTitle = result.data.pageAnalysis.title.map(_ => createNodeCheckPageAnalysis(_)).join();
					const pageAnalysisTitle = document.querySelector('#pageAnalysisTitle');
					pageAnalysisTitle.innerHTML = newHTMLTitle.replaceAll(",", "");

					const newHTMLTitleCountError = result.data.pageAnalysis.titleCountError || 0
					const pageAnalysisTitleCountError = document.querySelector('#title-error-count');
					pageAnalysisTitleCountError.innerHTML = newHTMLTitleCountError > 0 ?
						newHTMLTitleCountError > 1 ?
							`Title <label style="color: #f18200;">( ${newHTMLTitleCountError} Errors )</label>` :
							`Title <label style="color: #f18200;">( ${newHTMLTitleCountError} Error )</label>` :
						`Title <label style="color: #00aa63;">( All Good )</label>`

					const newHTMLReadability = result.data.pageAnalysis.readability.map(_ => createNodeCheckPageAnalysis(_)).join();
					const pageAnalysisReadability = document.querySelector('#pageAnalysisReadability');
					pageAnalysisReadability.innerHTML = newHTMLReadability.replaceAll(",", "");

					const newHTMLReadabilityCountError = result.data.pageAnalysis.readabilityCountError || 0
					const pageAnalysisReadabilityCountError = document.querySelector('#readability-error-count');
					pageAnalysisReadabilityCountError.innerHTML = newHTMLReadabilityCountError > 0 ?
						newHTMLReadabilityCountError > 1 ?
							`Readability <label style="color: #f18200;">( ${newHTMLReadabilityCountError} Errors )</label>` :
							`Readability <label style="color: #f18200;">( ${newHTMLReadabilityCountError} Error )</label>` :
						`Readability <label style="color: #00aa63;">( All Good )</label>`


				} else {
					alert("Thất bại");
				}
			};
			xhr.open("POST", '/admin/check-post');
			xhr.send(data);
		}
	};

	function createNodeCheckPageAnalysis(data) {
		let _icon = `<i class="fa fa-times-circle mr-2 mb-2" style="font-size:24px;color: red;"aria-hidden="true"></i>`
		if (data.status == 'perfect') {
			_icon = `<i class="fa fa-check-circle-o mr-2 mb-2" style="font-size:24px;color: green;" aria-hidden="true"> </i>`
		}
		const _key = data.key
		const _title = data.title
		const _text = data.text
		return `
	
		<div class="mt-2" >
						  <a style="font-size:16px;color: black;" href="#${_key}" data-toggle="collapse">
							${_icon}${_title}
						  </a>
						  <div id="${_key}" class="collapse">
							- ${_text}
						  </div>
		  </div>
		  `

	}


	const getListAdditionalKeyPhrase = document.querySelector('#check-get-additional-key-phrases-btn')
	if (getListAdditionalKeyPhrase) {
		getListAdditionalKeyPhrase.onclick = (evt) => {
			evt.preventDefault();

			const seoTitleInput = document.querySelector(".count-word-seo-title-input-string");
			const _seoTitle = seoTitleInput.value;

			const seoDescriptionInput = document.querySelector(".count-word-seo-description-string");
			const _seoDescription = seoDescriptionInput.value;

			const focusKeyPhraseInput = document.querySelector(".count-word-focus-keyphrase-input-string");
			const _focusKeyPhrase = focusKeyPhraseInput.value;

			// const contentInput = document.querySelector(".analyze-content-string");
			// const _content = contentInput.value;
			let contentEditors_test = document.querySelectorAll(".content-editor");
			const dataCKEditor_test = []
			if (contentEditors_test) {
				contentEditors_test.forEach((item, index) => {
					dataCKEditor_test.push({
						name: item.getAttribute("name"),
						value: editor[index].getData()
					})
				})
			};
			const _content = dataCKEditor_test.filter((e) => e.name.toString() == "content")?.[0].value

			const slugInput = document.querySelector(".slug-input-string");
			const _slug = slugInput.value;

			var data = new FormData();
			data.append('keyword', _focusKeyPhrase);
			data.append('title', _seoTitle);
			data.append('descriptionMeta', _seoDescription);
			data.append('content', _content);
			data.append('slug', _slug);

			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.data, result.message);

					const _countListKeyPhrase = result?.data?.listKeyPhrases.length || 0;

					const newHTMLGetAdditionalKeyPhrase = result.data.listKeyPhrases.map(_ => createNodeGetListAdditionalKeyPhrase(_)).join();
					const getAdditionalKeyPhrase = document.querySelector('#check-get-additional-key-phrase-preview');
					getAdditionalKeyPhrase.innerHTML = `
				<div class="mt-3">
				  <a style="font-size:16px;color: black;" href="#get-additional-key-phrase-preview" data-toggle="collapse">List Additional KeyPhrases ( ${_countListKeyPhrase} recommended.)
				  </a>
				</div>
				<div id="get-additional-key-phrase-preview" class="collapse">
				  ${newHTMLGetAdditionalKeyPhrase.replaceAll(",", "")}
				</div>
				`;

				} else {
					alert("Thất bại");
				}
			};
			xhr.open("POST", '/admin/get-list-additional-key-phrase');
			xhr.send(data);
		}
	};

	function createNodeGetListAdditionalKeyPhrase(data) {
		const _title = data.title
		return `
		  <div class=" mt-2">
			- ${_title}
		  </div>
	
		  `
	}


	const getListInternalLink = document.querySelector('#check-get-internal-links-btn')
	if (getListInternalLink) {
		getListInternalLink.onclick = (evt) => {
			evt.preventDefault();

			const seoTitleInput = document.querySelector(".count-word-seo-title-input-string");
			const _seoTitle = seoTitleInput.value;

			const seoDescriptionInput = document.querySelector(".count-word-seo-description-string");
			const _seoDescription = seoDescriptionInput.value;

			const focusKeyPhraseInput = document.querySelector(".count-word-focus-keyphrase-input-string");
			const _focusKeyPhrase = focusKeyPhraseInput.value;

			// const contentInput = document.querySelector(".analyze-content-string");
			// const _content = contentInput.value;
			let contentEditors_test = document.querySelectorAll(".content-editor");
			const dataCKEditor_test = []
			if (contentEditors_test) {
				contentEditors_test.forEach((item, index) => {
					dataCKEditor_test.push({
						name: item.getAttribute("name"),
						value: editor[index].getData()
					})
				})
			};
			const _content = dataCKEditor_test.filter((e) => e.name.toString() == "content")?.[0].value

			const slugInput = document.querySelector(".slug-input-string");
			const _slug = slugInput.value;

			var data = new FormData();
			data.append('keyword', _focusKeyPhrase);
			data.append('title', _seoTitle);
			data.append('descriptionMeta', _seoDescription);
			data.append('content', _content);
			data.append('slug', _slug);

			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.data, result.message);

					const _countListKeyPhrase = result?.data?.listInternalLink.length || 0;

					const newHTMLGetAdditionalKeyPhrase = result.data.listInternalLink.map(_ => createNodeGetListInternalLink(_)).join();
					const getAdditionalKeyPhrase = document.querySelector('#check-get-internal-link-preview');
					getAdditionalKeyPhrase.innerHTML = `
            <div class="mt-3">
              <a style="font-size:16px;color: black;" href="#get-internal-link-preview" data-toggle="collapse">List Internal Link ( ${_countListKeyPhrase} recommended.)
              </a>
            </div>
            <div id="get-internal-link-preview" class="collapse">
              ${newHTMLGetAdditionalKeyPhrase.replaceAll(",", "")}
            </div>
            `;

				} else {
					alert("Thất bại");
				}
			};
			xhr.open("POST", '/admin/get-list-internal-link');
			xhr.send(data);

		}
	};

	function createNodeGetListInternalLink(data) {
		const _title = data.name
		const _slug = data.slug
		return `
      <div class=" mt-2">
        - <a href="${_slug}" target="_blank" style="color: brown;">
          ${_title}
        </a>
      </div>
      `
	}
});

document.querySelectorAll(".button-create-empty").forEach((button) => {
	// @ts-ignore
	button.onclick = createPost;
});

document.querySelectorAll(".button-remove").forEach((button) => {
	// @ts-ignore
	button.onclick = removeApi;
});

document.querySelectorAll(".button-post").forEach((button) => {
	// @ts-ignore
	button.onclick = postApi;
});

document.querySelectorAll(".button-update").forEach((button) => {
	// @ts-ignore
	button.onclick = updateApi;
});


function createPost(e) {
	e.preventDefault();
	const xhr = new XMLHttpRequest();
	xhr.onload = (e) => {
		// @ts-ignore
		console.log(e.responseText)
		if (xhr.status < 300) {
			const docID = JSON.parse(xhr.responseText).data._id;
			const variantID = JSON.parse(xhr.responseText).data.variantID;

			if (variantID)
				window.location.href = `/admin/${this.dataset.type?.trim()}/${docID}?lang=en&variantID=${variantID}`;
			else
				window.location.href = `/admin/${this.dataset.type?.trim()}/${docID}?lang=en`;
		} else {
			alert("Khởi tạo thất bại");
		}
	};
	xhr.open("POST", this.dataset.action);
	xhr.send();
}

// @ts-ignore
function removeApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const ok = window.confirm("Bạn có chắc chắn muốn xoá ?");
		if (ok) {
			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.message);
					window.location.reload();
				} else {
					alert("Xoá bài viết thất bại");
				}
			};

			xhr.open("DELETE", url);
			xhr.send();
		}
	}
}

// @ts-ignore
function postApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const ok = window.confirm("Bạn có chắc chắn?");
		if (ok) {
			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.message);
					window.location.reload();
				} else {
					alert("Thất bại");
				}
			};

			xhr.open("POST", url);
			xhr.send();
		}
	}
}

// @ts-ignore
function updateApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const xhr = new XMLHttpRequest();
		// @ts-ignore
		xhr.onload = (e) => {
			if (xhr.status < 300) {
				const result = JSON.parse(xhr.responseText)
				console.log(result.error, result.message);
				// const postID = JSON.parse(xhr.responseText).data._id;
				window.location.reload();
			} else {
				alert("Cập nhật thất bại");
			}
		};

		xhr.open("PUT", url);
		xhr.send();

	}
}
